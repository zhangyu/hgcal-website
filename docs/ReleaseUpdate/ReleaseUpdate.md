# Notes on release update

## CMSSW_12_1_0_pre2
 * TICL v3 for HLT TDR
 * Moving the Pattern Recognization step to be plugin based
 * Seeding from L1 object developed by EGamma

