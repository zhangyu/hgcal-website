# Scintillator Sector

Since at the moment the details concerning type and size of the SiPMs and the
energy-to-charge conversion factors are not fully defined yet, the energy unit
for scintillators is MIPs. The conversion `keV2MIP` is defined in the
  [`hgcalDigitizer_cfi.py`](https://github.com/cms-sw/cmssw/blob/master/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py)
  and based on the measurement of the  energy deposited by muons traversing a
  scintillator tile perpendicularly which is approximately 650 keV. The number
  of photoelectrons (pe) generated per MIP is assumed to be 21 according to
  inputs from the CALICE group and measurements at test beams. This number takes
  into account the light lost in reflections within the tile before reaching the
  SiPM and the photo detection efficiency (PDE) of the SiPM itself.  The
  combined use of `keV2MIP` and `nPEperMIP` allows to estimate the total number
  of pe associated to each energy deposit which is a necessary step when adding
  the noise contribution.  Once the number of pe associated to each energy
  deposit is computed and updated to take into account noise, geometry, SiPM
  size, etc, the energy is converted to MIPs and sent to the shaper that creates
  the digi data frame.

!!! warning 
    
    One detail to keep in mind is the interplay between the amount of pe
    generated cell by cell and the final calibrated response. As an example
    let's consider the evolution of the response for a scintillating tile in
    time. For a MIP-like signal the new tile is expected to generate 21 pe
    while, after a given amount of integrated luminosity, the signal is expected
      to decrease due to the radiation damage of the scintillating material. The
      degradation of the response is parametrized as a function of dose which
      allows to compute a *degradation factor* (smaller than one) for the signal
      at any moment in time. When computing the `recHitEnergy` however, the
      final response should be calibrated to compensate for the energy
      degradation.  Practically the *degradation factor* should be re-applied in
      reverse.

The take away message is that the `digi` step and the `recHit` calibration are
linked tightly. The simulation of the response and noise evolution in time at
`digi` level as well as the dependence of the signal from the tile size, should be
take into account when computing the final `recHit` energy.


## Trivial shaper

At first order the main purpose of the shaper is to *bin* the signal according
to the ADC parameters (charge range and number of channels). As mentioned above,
at the moment a proper conversion to charge isn't available and the parameters
of the ADC for scintillators are fake and chosen to give approximately 15 ADC
for a signal of 1 MIP.

## DIGIs

The DIGI collection is stored in the event by the `HGCDigiProducer` class that
takes as an input the raw data frame computed by the shaper. For each channel
five time slices are stored and the in time signal is expected to fall in the
central (third) one.

## Uncalibrated recHits

This step simply un-do the ADC binning explained above. The amplitude in ADC of
the central `DIGI` sample is multiplied times `adcLSB`. This step happens in the
[HGCalUncalibRecHitRecWeightsAlgo](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecAlgos/interface/HGCalUncalibRecHitRecWeightsAlgo.h#L85)
class. To be noticed that for scintillators both the thickness and the
`fCPerMIP` factors are set to 1
[here](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalUncalibRecHit_cfi.py#L51).

## RecHits

This is the step that takes `UncalibRecHits` as an input and applies the energy calibration on top in order to restore the energy response in GeV. The calibration potentially takes into account several factors:

* charge to MIP factor to convert from `fC` to `MIPs`. This step is only needed for
  Silicons given that the response of scintillators is already stored in `MIPs` in
  the `DIGIs`. The conversion happens in the
  [HGCalRecHitWorkerSimple::run](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalRecHitWorkerSimple.cc#L190)
  method.

* `dEdx` weights to compensate for the energy lost in the absorber material. The
  weights are such that convert the `MIPs` to GeV. This step happens in the
  [HGCalRecHitSimpleAlgo::makeRecHit](https://github.com/cms-sw/cmssw/blob/CMSSW_11_0_X/RecoLocalCalo/HGCalRecAlgos/interface/HGCalRecHitSimpleAlgo.h#L54)
  method

* other factors are needed to compensate for the response degradation as
  mentioned above.

!!! warning 

    To be noticed that for scintillator the same variables defined for the
    silicon sector are recycled. While this technically works well, the variable
    naming might sound misleading.



## Summary of the parameters used for a typical tile size:

* Typical size: $30 \times 30\times 3~mm^3$

* $\mathrm{keV2MIP} = 1/650~\mathrm{keV}^{-1}$ -  energy released by a MIP in a typical cell
* $\mathrm{nPEperMIP}: 21$ - number of photoelectrons per MIP
  * $\mathrm{overvoltage} = 2V$
  * $\mathrm{PDE} = 20.5\%$
* $\mathrm{nTotalPE} = 7500$ -  number of SiPMs cells
* $\mathrm{ZSthr} = 0.5~\mathrm{MIPs}$


## References and pointers
* [HGCAL DPG, Apr 3rd](https://indico.cern.ch/event/810242/#2-simulation-of-scintsipms)
* [PR to CMSSW](https://github.com/cms-sw/cmssw/pull/26380)
* [Notes from Chris (early 2019)](https://deguio.web.cern.ch/deguio/hgcal/docs/ElectronicsNotes.pdf)
* [Notes from Philippe (mid 2018)](https://deguio.web.cern.ch/deguio/hgcal/docs/Note_area.pdf)
* [DN from Jeremy/Pawel (early 2017)](https://github.com/cms-sw/cmssw/pull/cms.cern.ch/iCMS/jsp/openfile.jsp?tp=draft&files=DN2017_010_v1.pdf)



<!--This section describes the details of all the steps that happen between the PCaloHits and the DIGIs.
The focus is on the scintillating section of the HGCAL.

## Signal and noise models
The energy of `PCaloHits` is distributed according to a landau which describes the energy deposition of muon MIPs traveling through matter. Since neither the scintillation processes nor the light collection is described by Geant4, corrections to the deposited amount of energy are needed to model effects related to the geometry and to the aging of the detector.

 The steps which are performed in the `HGCHEbackDigitizer` are:
* Scale the signal energy according to the tile geometry (smaller tile $$\rightarrow$$ more efficient light collection)
* Scale the signal energy to take into account the scintillator darkening (radiation damage, depends on dose)
* Smear the signal according to poissonian distribution
* Estimate the dark current contribution associated to the SiPM readout (depends on fluence)
* Make the resulting pulse go through the SiPM simulation (non-linearity of the sensor + cross talk)
* Digitize the final pulse

or in one formula:
$$
\text{Digitization~of~}\Bigg[ \frac{\text{SiPM}\Bigg(  \sum_{hits} \Bigg( \text{Poiss} \Big( \text{PCaloHit} \times \text{SF} \times \text{npePerGeV} \Big) \Bigg) + \text{noise} \Bigg)} {\text{npePerGeV} \times \text{ADC2GeV}} \Bigg] = \text{uncalib DIGIs}
$$

In addition to the energy scaling and smearing, the simulation of the time response is also important. The digitized pulse will result from the convolution of the scintillation pulse shape and the SiPM response. In this case the needed steps would be:
* Use the caloHit time to calculate "tzero"
* Add an extra time contribution according to the scintillation shape
* Convolve the time distribution with the time response of the SiPM

## The SiPM readout

## Dependence of the response on the Tile and SiPM size

## Evolution of the response with time-->
