# HGCAL Digitization

## CMSSW General Overview

The main actor in the _digitization_ process in `CMSSW` is the `MixingModule`.
It lives in the package
[`SimGeneral/MixingModule`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimGeneral/MixingModule/plugins/MixingModule.h).
The most important ingredient is the `Accumulators` data member, that is a
vector of all the `Accumulators` module that have been configured. The
configuration of the accumulators is done via the `theDigitizers` python object,
that is configured
[here](https://cmssdt.cern.ch/dxr/CMSSW/source/SimGeneral/MixingModule/python/digitizers_cfi.py).
Every detector will have its own configuration section, that will be forwarded
to the correct `DIGI_ACCUMULATOR`.
The accumulators are created following, more or less, the same logic as the
`plugin` mechanism in `CMSSW`, i.e. created via a `factory`: a string will
identify the correct accumulator to instantiate.

The accumulators/digitizers for the HGCAL detector are configured in the
[`SimCalorimetry/HGCalSimProducers`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py)
package. The python labels for the different digitizers are:

* [`hgceeDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py#74)
* [`hgchefrontDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py#104)
* [`hgchebackDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py#133)

The `C++` type of the accumulator/digitizer that is created, in all cases, is
[`HGCDigiProducer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/plugins/HGCDigiProducer.h).

This class is registered as a `DIGI_ACCUMULATOR`
[here](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/plugins/HGCDigiProducer.cc#66),
via a macro. The product produced by this class is `HGCalDigiCollection`,
defined
[here](https://cmssdt.cern.ch/dxr/CMSSW/source/DataFormats/HGCDigi/interface/HGCDigiCollections.h#21),
**and is the same for all cases**.
This class will internally have the `theDigitizer_` member data, of `C++` type
`HGCDigitizer` that will do all the heavy work of creating the digis.

The
[`HGCDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/interface/HGCDigitizer.h#30)
is defined in the very same package. This, internally, will have 3 pointers
(`unique_ptr`), each of which will take care of a dedicated region in the HGCAL
detector, following the same 3-way partitioning that is present in the python
configuration files, as seen above.

The `C++` type of each digitizer will be different:

* [`HGCEEDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/interface/HGCEEDigitizer.h#7)
* [`HGCHEfrontDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/interface/HGCHEfrontDigitizer.h#7)
* [`HGCHEbackDigitizer`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/interface/HGCHEbackDigitizer.h#9)

In the very end, all of them will be derived from a common `base class`:
`HGCDigitizerBase<HGCalDataFrame>`. The main template class is defined
[here](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/interface/HGCDigitizerBase.h#61),
while the template argument, `HGCalDataFrame`, is defined [here](https://cmssdt.cern.ch/dxr/CMSSW/source/DataFormats/HGCDigi/interface/HGCDigiCollections.h#20)

!!! warning
    Other, additional, collections and types are defined:
    ```
    typedef HGCDataFrame<HGCalDetId, HGCSample> HGCEEDataFrame;
    typedef edm::SortedCollection<HGCEEDataFrame> HGCEEDigiCollection;

    typedef HGCDataFrame<HGCalDetId, HGCSample> HGCHEDataFrame;
    typedef edm::SortedCollection<HGCHEDataFrame> HGCHEDigiCollection;

    typedef HGCDataFrame<HcalDetId, HGCSample> HGCBHDataFrame;
    typedef edm::SortedCollection<HGCBHDataFrame> HGCBHDigiCollection;
    ```

    It is totally unclear if they are relics from the very past, or if they are
    used somewhere in the code. The **rather bad thing** is that, even if they
    are not used, specific template code compilation is forced at compile-time
    also for them, e.g.,
    [here](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/src/HGCDigitizerBase.cc#216)
    and
    [here](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/src/HGCFEElectronics.cc#473).
    If those are relics from the past, **they have to be cleaned up as soon as
    possible**.


## Introduction and General Overview

The chain of events that goes from the _Simulation_ (`PCaloHits`) up to the
_digis_ (`HGCalDigiCollection`) goes along the following schema:

![Sequence for Digitization](DigiSequence.png)
[Link to sequence code.](digis.md#sequence-for-digis)


A more detailed explanation follow.

1. An instance of `HGCDigitizer` will hold internally all the needed `C++`
   object to transform the `PCaloHits` into _digis_.

1. `HGCDigitizer` will call the `accumulate(...)` method that will receive a
   collection of all the `PCaloHits`, both from the hard-scatterer (**BX=0**)
   and from the sibling ones.

1. An instance of `HGCSimHitDataAccumulator` internal to the `HGCDigitizer` will
   hold, for every single `DetId` of `HGCAL`, an`HGCCellInfo` which is an
   `array` of made of 2 `array`s. The first `array` of size 15 will contain the
   **integrated charge for that particular time-slot(TS)**. There are 15 time
   slots: the slot 9 correspond to BX=0, the others on the left and on the right
   are the sibling bunch-crossings, before and after, respectively. The second
   `array` will contain the information about the timing (in the current
   implementation, if the interpretation is correct, that information is
   available only for the slot number 9, which correspond to the **in-time**
   signal). This data structure belongs to the `HGCDigitizer` and is filled by
   the different `accumulate(...)` methods.

1. The method `finalizeEvent(...)` of `HGCDigitizer` will then use this data
   structure (i.e. the `HGSimHitDataAccumulator`), pass it down to the
   specialized **HGCAL digitizers** and put into the event the final
   `HGCalDigiCollection`.

1.  The specialized **digitizers** (HGCEEDigitizer in the picture) will use this
    data-structure, will **add noise according to the input parameters for all
    the detids of HGCAL and will add it to the signal in each time-slot**. 
  
1. Next, a new `HGCDataFrame` is created, one for each `DetId` of the **full
   HGCAL**. 

1. Finally, the proper specialization of the **shaper** (_emulating the
   Front-End chip_) is applied on the input `HGCSimHitData` samples.

1. Inside the frontend, the 15 samples will be **digitized** simulating the
   behaviour of the HGCROC. 

1. Each slot is analysed as a single slot or in more general terms if the ToT
   readout is triggered. Later in the same digitizer, for each of the 15 samples
   an `HGCSample` will be created according to the different kind of reading
   triggered by the deposited charge: either a single read via `ADC` or a longer
   `ToT` read that will be active in the first time-sample that trigger it and
   that will mask the next sibling time-slots until all the charge has been
   integrated/drained. 

!!! info
    Not all the `HGCDataFrame` created are put into the `Event`, since that would be
    useless and a waste of resources. Only the `HGCDataFrame` in which the time-slot
    **9 (i.e. BX=0)** are above threshold (i.e. _fired_) are saved into the event.

!!! warning
    Also, the timing of the `HGCDataFrame`, when put into the `Event`, is such that
    the time-slot **9** is set back to **2**, and only the sibling 2 time-slots,
    before and after, are saved. The final `HGCDataFrame` will have 5 slots as
    follows:
    
    
    
    | Idx | Original TimeSlot | BX |
    | --- | ----------------- | -- |
    | 0   | 7                 | -2 |
    | 1   | 8                 | -1 |
    | 2   | 9                 |  0 |
    | 3   | 10                | +1 |
    | 4   | 11                | +2 |
   

!!! danger
    The Uncalibrated RecHit will only access information in Idx=2, i.e. the
    **in-time BX** only.


## C++ Actors

### [`HGCSample`](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCDigi/interface/HGCSample.h)

This is mainly a wrapper around a `word`, which is an `uint32_t` type:

```cpp
class HGCSample {
...

  enum HGCSampleMasks {
    kThreshMask = 0x1,
    kModeMask = 0x1,
    kToAValidMask = 0x1,
    kGainMask = 0xf,
    kToAMask = 0x3ff,
    kDataMask = 0xfff
  };
  enum HGCSampleShifts {
    kThreshShift = 31,
    kModeShift = 30,
    kToAValidShift = 29,
    kToGainShift = 22,
    kToAShift = 12,
    kDataShift = 0
  };
...
void setWord(uint16_t word, HGCSampleMasks mask, HGCSampleShifts shift) {
  // mask and shift bits
  const uint32_t masked_word = (word & mask) << shift;

  //clear to 0  bits which will be set by word
  value_ &= ~(mask << shift);

  //now set bits
  value_ |= (masked_word);
}

uint32_t getWord(HGCSampleMasks mask, HGCSampleShifts shift) const { return ((value_ >> shift) & mask); }

// a 32-bit word
// V10 Format: tmt---ggggttttttttttdddddddddddd
uint32_t value_;
...
};
```

The bitmap can be visually explained as follow:

![HGCSample bits meaning](HGCSample.png)

[Link to Graphivz code.](digis.md#hgcsample-in-graphviz)

### [`HGCDataFrame<DetId,HGCSample>`](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCDigi/interface/HGCDataFrame.h)

This class will store a `vector` of `HGCSample` for every specific `DetId`.

```cpp
template <class D, class S>
class HGCDataFrame {
public:

  typedef D key_type;

  ...

private:
  //collection of samples
  std::vector<S> data_;

  // det id for this data frame
  D id_;

  //number of samples and maximum available
  size_t maxSampleSize_;
};

```

### [`HGCSimHitDataAccumulator`](https://github.com/cms-sw/cmssw/blob/master/SimCalorimetry/HGCalSimProducers/interface/HGCDigitizerTypes.h)


```cpp
namespace hgc_digi {

  //15 time samples: 9 pre-samples, 1 in-time, 5 post-samples
  constexpr size_t nSamples = 15;

  typedef float HGCSimData_t;

  typedef std::array<HGCSimData_t, nSamples> HGCSimHitData;

  struct HGCCellInfo {
    //1st array=energy, 2nd array=energy weighted time-of-flight
    std::array<HGCSimHitData, 2> hit_info;
    int thickness;
    double size;
  };

  typedef std::unordered_map<uint32_t, HGCCellInfo> HGCSimHitDataAccumulator;

}  // namespace hgc_digi
```

### HGCAL FrontEnd Simulator: [HGCFEElectronics](https://github.com/cms-sw/cmssw/blob/master/SimCalorimetry/HGCalSimProducers/interface/HGCFEElectronics.h)

```cpp
template <class DFr>
class HGCFEElectronics {
public:
  enum HGCFEElectronicsFirmwareVersion { TRIVIAL, SIMPLE, WITHTOT };
  enum HGCFEElectronicsTOTMode { WEIGHTEDBYE, SIMPLETHRESHOLD };
  inline void runShaper(DFr& dataFrame,
                        hgc::HGCSimHitData& chargeColl,
                        hgc::HGCSimHitData& toa,
                        CLHEP::HepRandomEngine* engine,
                        uint32_t thrADC = 0,
                        float lsbADC = -1,
                        uint32_t gainIdx = 0,
                        float maxADC = -1,
                        int thickness = 1) {
    switch (fwVersion_) {
      case SIMPLE: {
        runSimpleShaper(dataFrame, chargeColl, thrADC, lsbADC, gainIdx, maxADC);
        break;
      }
      case WITHTOT: {
        runShaperWithToT(dataFrame, chargeColl, toa, engine, thrADC, lsbADC, gainIdx, maxADC, thickness);
        break;
      }
      default: {
        runTrivialShaper(dataFrame, chargeColl, thrADC, lsbADC, gainIdx, maxADC);
        break;
      }
    }
  }

  /**
     @short converts charge to digis without pulse shape
   */
  void runTrivialShaper(
      DFr& dataFrame, hgc::HGCSimHitData& chargeColl, uint32_t thrADC, float lsbADC, uint32_t gainIdx, float maxADC);

  /**
     @short applies a shape to each time sample and propagates the tails to the subsequent time samples
   */
  void runSimpleShaper(
      DFr& dataFrame, hgc::HGCSimHitData& chargeColl, uint32_t thrADC, float lsbADC, uint32_t gainIdx, float maxADC);

  /**
     @short implements pulse shape and switch to time over threshold including deadtime
   */
  void runShaperWithToT(DFr& dataFrame,
                        hgc::HGCSimHitData& chargeColl,
                        hgc::HGCSimHitData& toa,
                        CLHEP::HepRandomEngine* engine,
                        uint32_t thrADC,
                        float lsbADC,
                        uint32_t gainIdx,
                        float maxADC,
                        int thickness);

  ...

private:
  //private members
  uint32_t fwVersion_;
  std::array<float, 6> adcPulse_, pulseAvgT_;
  std::array<float, 3> tdcForToAOnset_fC_;
  std::vector<float> tdcChargeDrainParameterisation_;
  float adcSaturation_fC_, adcLSB_fC_, tdcLSB_fC_, tdcSaturation_fC_, adcThreshold_fC_, tdcOnset_fC_, toaLSB_ns_,
      tdcResolutionInNs_;
  uint32_t targetMIPvalue_ADC_;
  std::array<float, 3> jitterNoise2_ns_, jitterConstant2_ns_;
  std::vector<float> noise_fC_;
  uint32_t toaMode_;
  bool thresholdFollowsMIP_;
  //caches
  std::array<bool, hgc::nSamples> busyFlags, totFlags, toaFlags;
  hgc::HGCSimHitData newCharge, toaFromToT;
};
```



### HGCAL Digitizer base class: [HGCDigitizerBase](https://github.com/cms-sw/cmssw/blob/master/SimCalorimetry/HGCalSimProducers/interface/HGCDigitizerBase.h)

```cpp
namespace hgc_digi_utils {
  using hgc::HGCCellInfo;
  ...
}  // namespace hgc_digi_utils

template <class DFr>
class HGCDigitizerBase {
public:
  typedef DFr DigiType;
  typedef edm::SortedCollection<DFr> DColl;

  void run(std::unique_ptr<DColl>& digiColl,
           hgc::HGCSimHitDataAccumulator& simData,
           const CaloSubdetectorGeometry* theGeom,
           const std::unordered_set<DetId>& validIds,
           uint32_t digitizationType,
           CLHEP::HepRandomEngine* engine);

  void runSimple(std::unique_ptr<DColl>& coll,
                 hgc::HGCSimHitDataAccumulator& simData,
                 const CaloSubdetectorGeometry* theGeom,
                 const std::unordered_set<DetId>& validIds,
                 CLHEP::HepRandomEngine* engine);
  }


protected:
  //baseline configuration
  edm::ParameterSet myCfg_;

  //1keV in fC
  float keV2fC_;

  //noise level (used if scaleByDose=False)
  std::vector<float> noise_fC_;

  //charge collection efficiency (used if scaleByDose=False)
  std::vector<double> cce_;

  //determines if the dose map should be used instead
  bool scaleByDose_;

  //path to dose map
  std::string doseMapFile_;

  //noise maps (used if scaleByDose=True)
  HGCalSiNoiseMap scal_;

  //front-end electronics model
  std::unique_ptr<HGCFEElectronics<DFr> > myFEelectronics_;

  //bunch time
  double bxTime_;

  //if true will put both in time and out-of-time samples in the event
  bool doTimeSamples_;

  //if set to true, threshold will be computed based on the expected meap peak/2
  bool thresholdFollowsMIP_;

  // New NoiseArray Parameters

  const double NoiseMean_, NoiseStd_;
  static const size_t NoiseArrayLength_ = 200000;
  static const size_t samplesize_ = 15;
  std::array<std::array<double, samplesize_>, NoiseArrayLength_> GaussianNoiseArray_;
  bool RandNoiseGenerationFlag_;
  // A parameter configurable from python configuration to decide which noise generation model to use
  bool NoiseGeneration_Method_;
};

```

### HGCAL Digitizer specializations

```cpp
template class HGCDigitizerBase<HGCEEDataFrame>;
template class HGCDigitizerBase<HGCBHDataFrame>;
template class HGCDigitizerBase<HGCalDataFrame>;
```

### Digi Collections

The digi collections are defined as follows:

```cpp
typedef HGCDataFrame<DetId, HGCSample> HGCalDataFrame;
typedef edm::SortedCollection<HGCalDataFrame> HGCalDigiCollection;

// Legacy data below
typedef HGCDataFrame<HGCalDetId, HGCSample> HGCEEDataFrame;
typedef HGCDataFrame<HGCalDetId, HGCSample> HGCHEDataFrame;
typedef HGCDataFrame<HcalDetId, HGCSample> HGCBHDataFrame;
typedef edm::SortedCollection<HGCEEDataFrame> HGCEEDigiCollection;
typedef edm::SortedCollection<HGCHEDataFrame> HGCHEDigiCollection;
typedef edm::SortedCollection<HGCBHDataFrame> HGCBHDigiCollection;

```

## Source code for the images

### HGCSample in Graphviz

```cpp
digraph hgcsample {
size ="10,10";
graph [pad="0", nodesep="2", ranksep="3"];
node [shape=record];
bits [shape=record,label="BITS: |<b31> 31|<b30> 30|<b29> 29|<b28> 28|<b27> 27|<b26> 26|<b25> 25|<b24> 24|<b23> 23|<b22> 22|<b21> 21|<b20> 20|<b19> 19|<b18> 18|<b17> 17|<b16> 16|<b15> 15|<b14> 14|<b13> 13|<b12> 12|<b11> 11|<b10> 10|<b9> 9|<b8> 8|<b7> 7|<b6> 6|<b5> 5|<b4> 4|<b3> 3|<b2> 2|<b1> 1|<b0> 0"];
value [shape=record,label="<th> Threshold[1] |<mode> Mode[1] | <toavalid> ToA Valid[1] | <gain> Gain[4] | <toadigi> ToA Value[10] | <adc> adc[12]"];
bits:b31 -> value:th [color="#11ff00"];

bits:b30 -> value:mode [color="#ee11ff"];

bits:b29 -> value:toavalid [color="#00ffaa"];

bits:b22 -> value:gain [color="#454545"];
bits:b23 -> value:gain [color="#454545"];
bits:b24 -> value:gain [color="#454545"];
bits:b25 -> value:gain [color="#454545"];

bits:b12 -> value:toadigi [color="#000fff"];
bits:b13 -> value:toadigi [color="#000fff"];
bits:b14 -> value:toadigi [color="#000fff"];
bits:b15 -> value:toadigi [color="#000fff"];
bits:b16 -> value:toadigi [color="#000fff"];
bits:b17 -> value:toadigi [color="#000fff"];
bits:b18 -> value:toadigi [color="#000fff"];
bits:b19 -> value:toadigi [color="#000fff"];
bits:b20 -> value:toadigi [color="#000fff"];
bits:b21 -> value:toadigi [color="#000fff"];

bits:b0 -> value:adc [color=red];
bits:b1 -> value:adc [color=red];
bits:b2 -> value:adc [color=red];
bits:b3 -> value:adc [color=red];
bits:b4 -> value:adc [color=red];
bits:b5 -> value:adc [color=red];
bits:b6 -> value:adc [color=red];
bits:b7 -> value:adc [color=red];
bits:b8 -> value:adc [color=red];
bits:b9 -> value:adc [color=red];
bits:b10 -> value:adc [color=red];
bits:b11 -> value:adc [color=red];
}
```

### Sequence for Digis

```sequence
participant Geant4
participant HGCDigitizer
note right of HGCDigitizer: holds instance of HGCSimHitDataAccumulator \nand every specialization of HGCAL Digitizers
participant HGCSimHitDataAccumulator
participant HGCEEDigitizer
participant HGCFEElectronics
note right of HGCEEDigitizer: holds a pointer to HGCFEElectronics
participant Event

# Actions
Geant4-->HGCDigitizer: PCaloHits


HGCDigitizer->HGCSimHitDataAccumulator: accumulate(...)
note right of HGCSimHitDataAccumulator: Loop on PCaloHits\nFill data-structure
HGCSimHitDataAccumulator->HGCDigitizer: HGCSimHitDataAccumulator filled\nwith all information from all PCaloHits


HGCDigitizer->HGCEEDigitizer: finalizeEvent(...)
note right of HGCEEDigitizer: Loop on HGCSimHitDataAccumulator
note right of HGCEEDigitizer: Create HGCDataFrame\none foreach DetId
HGCEEDigitizer->HGCFEElectronics: Pass HGCDataFrame
HGCFEElectronics->HGCFEElectronics: runShaper(...)
HGCFEElectronics->HGCFEElectronics: updateOutput(...)
note right of HGCFEElectronics: Compress to 5 TS\nDiscard empty HGCDataFrame
HGCFEElectronics-->HGCDigitizer: HGCalDigiCollection
```





