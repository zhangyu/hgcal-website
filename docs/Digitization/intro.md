# Introduction

The digitization step takes care of everything that happens between the
`PCaloHits` and the `DIGIs`. It includes the cumulation of the energy for all the
`SimHits` within each detector active cell, the description of the readout
electronics including the noise component and the readout logic, the shaping and
the digitization of the pulse. The evolution of the response in time according
to dose and fluence (*FLUKA simulation is needed as an input*) is also described
in this step.

* **Silicon Sector**: When biased by a voltage difference, the response of a Si
  sensor to the passage of ionizing particles is characterized by its charge
  collection efficiency (CCE) and its leakage current (Ileak). As the Si sensors
  will be operated well above its full depletion voltage, the CCE is expected to
  be maximized. Exposure to radiation may induce displacements in the lattice,
  and ionization damage liberating charge carriers, among others. These effects
  contribute to both reduce the CCE and increase the Ileak.

* **Scintillator Sector**: Scintillators cover approximately half of the HGCAL
  sensitive area with approximately 389k channels. The tile size is typically
  3cm x 3cm x 3mm, but the size changes with the radius introducing a
  geometrical dependence of the light yield. The region where scintillators are
  used is defined by requiring MIP S/N>5, after full HGCAL lifetime exposure to
  irradiations. Two dominant effects are expected at the end of life from the
  radiation: 
    
    * decrease in photo-statistics (i.e. light yield from the passage of
      ionizing particles)
    
    * increase in dark current generating random firing of the Si
      photomultipliers (SiPMs).


A simplified list of all the steps which are expected to happen is:

* Take `PCaloHits` from primary or secondary particles and sum their energy in
  each cell accordingly to the detector geometry

* Use the hit time to calculate `tzero`

* Add an extra time contribution according to the signal process considered
  (i.e. typical scintillation decay time in case of the `CEH_Sci` tiles)

* Add dark current contribution (noise) according to the sensors characteristics

* If needed simulate additional effects such as the `SiPM` non-linearity and
  cross talk

* Compute and add the readout noise

* Digitize the final pulse

or in one formula:

![Digi](digitization.png)

The numerical procedure to emulate the performance of the HGCAL sensors and
front end electronics at the end-of-life, after collecting >3 $\textrm{ab}^{−1}$
of integrated luminosity are described in the detector note DN-19-045. The
implementation is centrally available in `CMSSW` after release `11_0_pre7`.
