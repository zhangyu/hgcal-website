# TICL

## Overview and Components
The HGCAL detector is novel in its design. It requires novel approaches to
reconstruction in order to make optimal usage of its potential. 
The **TICL** (**T**he **I**terative **CL**ustering) Framework has been designed
with these goals in mind.

The building blocks used by TICL are the 2-dimensional `LayerClusters` that are
built by the CLUE algorithm, described [here](clueAlgorithm.md). These 2d
objects are then connected together to form 3-d objects/showers. The problem
(especially the combinatorics one) is simplified if easier-to-reconstruct
objects, like **electromagnetic showers**, or externally seeded ones get removed
before running the **hadronic** reconstruction. These concepts are realized in
the iterative clustering framework TICL. The framework is fully modular,
allowing the user to test and validate different components, like clustering,
pattern recognition or energy regression, in an easy and transparent way. A
corresponding **validation** package has been developed in order to quantify the
goodness of the reconstructed objects (see
[here](../Validation/HGCalValidator.md),
[here](../Validation/TracksterValidation.md) and
[here](../Validation/LayerClusterValidation.md) for its documentation).

## Skeleton of an Iteration

A typical iteration inside TICL will be composed by 5 steps:

   * `LayerCluster` **masking and filering** 
   * **Seeding Region** definition
   * **Pattern Recognition**
   * **Linking**
   * **Cleaning** and **Classification**

![Skeleton of a  **TICL** Iteration](TICL_iteration.png)

### Navigation Tile

In order to speed up the process of searching for nearby 2-d clusters to be
linked together, a `TICLLayerTile` data structure has been created.

A `TICLLayerTile` acts like a bi-dimensional histogram. It is an
`array<vector<unsigned int>, nEtaBins*nPhiBins>`, properly wrapped into a class
to ease the handling of the bin numbering. The binning is done in $\eta,
\phi$, but the data structure is generic enough to accomodate other space
partitions like, e.g., $x, y$.

Another data structure, `TICLLayerTiles` is built on top of that. This is an
`std::array<TICLLayerTile, ticl::constants::nLayers>` and is used to add,
as an additional dimension to the histogram concept, the Layer index. Again, the
correct handling of the bins and access operators `[]` are implemented in the
wrapper class to the underlying data structure.

The concept of a `Tile` is illustrated in the image below:

![Navigation Tile used by **TICL**](TileNavigationTICL.png)

In the current (2019-07-26) implementation of **TICL**, a single
`TICLLayerTiles` is created at the beginning of each event and is stored inside
the `Event`. That tile contains all the 2-d `LayerClusters` that have been
reconstructed by `CLUE`. All iterations will fetch this tile from the `Event`
and, eventually, mask it using the proper mask filters.

### Seeding Region Producer

This step produces seeding regions in the detector by tracks and ... .

#### SeedingRegionByTracks

   * Cut on tracks : $1.48<|\eta|<3.0$, quality(HighPurity), MISSING_OUTER_HITS<5
   * Propogate the track to the first disk
   * Save the track position, momentum and Id in SeedingRegion
   * Sort by track momentum

<font color=blue>Yu : add global seeding</font>
#### SeedingRegionGlobal
  The clusters are pointing back to the origin (0,0,0).

### Cellular Automaton Pattern Recognition

The Pattern Recognition algorithm that has been implemented inside **TICL** is
the **Cellular Automaton**. The basic principle of working is illustrated in the
figure below.

![Doublets connection using CA in **TICL**](CADoubletsInTICL.png)

The algorithm works as follow:

   * Start from a $\mathrm{Layer_N}$ and consider a specific 2-d cluster
   * Open a window, in the $[\eta,\phi]$ space, around it and project it onto the
   next $\mathrm{Layer_{N+1}}$
   * Consider all the 2-d clusters that fall within this region and try to establish a
   `Doublet` connection between the 2-d clusters on the 2 layers.
   * Apply some compatibility criteria to decide is the 2-d clusters should be
   linked or not (like geometry constrains, energy, timing compatibility, etc
   $\ldots$)
   * Repeat this same procedure for all the 2-d clusters on $\mathrm{Layer_N}$
   * Repeat this same procedure for all pairs of contiguous Layers,
   $(\mathrm{Layer_K}, \mathrm{Layer_{K+1}})$.

!!! note
    If configured properly, the CA can also establish links between
    **non-adjacent layers**, allowing for (consecutive) missing 2-d clusters.
    The number of missing `Layers` is a configuration parameter.

At the end of this process, the 2-d clusters will be linked by `Doublets` if they
satisfy some configurable requirements. The set of all connected `Doublets` form
a _graph_ and is the building block of a `Trackster`. The root cell of the graph
is the 2-d cluster closer to the interaction point that has no upstream
connection/`Doublets`.

### CLUE3D
<font color=blue>Yu : add description of CLUE3D</font>
CLUE3D is a novel pattern recognization algorithm. See [here](https://indico.cern.ch/event/1049055/contributions/4410257/attachments/2265568/3846599/20210616_HGCAL_DPG_CLUE3D_MR.pdf) for more details.

![gener procedure of CLUE3D](CLUE3D.png)
![CLUE3D : calculateLocalDensity()](CLUE3D_calculateLocalDensity.png)
![CLUE3D : calculateDistanceToHigher()](CLUE3D_calculateDistanceToHigher.png)
![CLUE3D : findAndAssignTracksters()](CLUE3D_findAndAssignTracksters.png)
![CLUE3D : assignPCAToTracksters()](CLUE3D_assignPCAToTracksters.png)


The `Tracksters` are conveniently transformed into the legacy `MultiClusters`
data formats, to easily interface with the rest of the existing reconstruction
software.

## Using **TICL**

If you like to try TICL, look at the [Quick recipe section](../../GettingStarted/).

