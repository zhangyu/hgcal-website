# Imaging Algorithm

The so called *Imaging Algorithm* is, in fact, derived from a published
paper ([link](http://science.sciencemag.org/content/344/6191/1492.full.pdf)).

## Algorithm's Logic

The main logic of the algorithm can be split into three logical sections, that
are illustrated below via some pseudo-code.

![General algorithm logic](HGCALImagingAlgorithm.png)

### Calculate the local density
![Calculate the local density](HGCALImagingAlgorithm_CalculateDensity.png)
### Calculate the closest distance to a cell with higher density
![Calculate the closest distance to a cell with higher density](HGCALImagingAlgorithm_CalculateDistanceToHigher.png)
### Find Seeds and make clusters
![Find Seeds and make clusters](HGCALImagingAlgorithm_FindAndAssignClusters.png)


