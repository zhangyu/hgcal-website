# Reconstruction Tutorial

## Introduction

The goal of this tutorial is to review in some details (i.e from the _technical
point of view_, with special emphasis on _running jobs in CMSSW_) the main
components of the **TICL** reconstruction. We will analyse all the building
blocks of TICL, inspect where they _live in CMSSW_, how we could inspect their
configurations, how to customise them and, finally, how to put everything
together and visualize the results. This is in no way meant to be an exhaustive
overview but simply an help to get you familiar with TICL and start contributing
to it the way you think it would be the most appropriate.

!!! warning

    **The TICL reconstruction should be considered as a work in progress**.
    Exposing few ideas that we are still in the process of being developed can
    open up for new inputs and suggestions from you that could be promptly
    tested and, in case, integrated upstream.

??? info

    This is a **live document**, in the sense that it could become outdated
    quite often. The nice thing about it, though, is that you can very easily
    contribute to it, either correcting errors and mistakes, or simply adding
    documentation, procedures and suggestions that you think are missing. Simply
    go to [this web-site](https://gitlab.cern.ch/websites/hgcal-website), fork it,
    create a new branch with your suggestions and fixes and open a _merge
    request_. We will integrate it, after reviewing it.

## Overview and Components

The HGCAL detector is novel in its design. It requires novel approaches to
reconstruction in order to make optimal usage of its potential.  The **TICL**
(**T**he **I**terative **CL**ustering) Framework has been designed with these
goals in mind.

The building blocks used by TICL are illustrated below:

![TICL Components](TICLComponents.png)

We will review all the components that are used inside TICL in the following
sections.


### Data Structures

#### TICL Navigation Tiles

##### Motivations

Several challenges in the TICL reconstruction require the knowledge of _spatial
proximity_ between objects. The objects could be, e.g., rechits laying on a
single layer, 2D Layer clusters on different layers or even Tracksters between
themselves.

Usually, from the computer science point of view, this problem is solved using
an ad-hoc data structures called [_K-d
Tree_](https://en.wikipedia.org/wiki/K-d_tree). The building block of this data
structure is a tree that will recursively partition your search _d-dimensional_
space into 2 along each dimension (recursively) using the median to partition it
each time. In this way, the search for specific objects in a certain region of
the phase-space can be translated into a binary search problem, bringing down
the complexity search for nearest neighbour to a relatively affordable $ln(n)$
complexity, where $n$ is the overall number of points among which the search is
run. The cost of _building the k-d tree_ is, in the best case scenario, of
$n\times ln(n)$ complexity. This data structure is extremely important in
computer science and found several applications in various fields, including
CMSSW.

On the other hand, its graph-based nature makes it a rather complex structure to
be brought into modern massively parallel architectures, like GPUs.  Moreover,
the dimensionality of the problems we have to solve in the HGCAL reconstruction
is rather low (typically 2D or 3D, like $\mathbb{R}^2\{\eta\times\phi\}$ or
$\mathbb{R}^3\{x \times y \times z\}$). For these reasons, we decided to explore
different solutions.

##### Description of a `Tile`

Among the ones we explored, we decided to implement and use a **Tile** data
structure. **A Tile is a histogram-like data structure**. It partitions the
space into bins and keeps track of all the points that physically fall into that
space. Since the number of points that will fall into each bin is not known _a
priori_, **a vector is associated to each bin**, so that it can be dynamically
expanded, as needed.  The core structure, the `Tile`, is defined in the package
[DataFormats/HGCalReco](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCalReco/interface/TICLLayerTile.h):

```c++
std::array<std::vector<unsigned int>, T::nBins> tile_;
```

where `T` is the class template parameter whose purpose is to define the number
of bins to use to partition the space, together with the boundary conditions on
$\eta$ and $\phi$.

The `tile_` is a `C++ array` of `vector<unsigned int>`. It is a linear structure
whose dimension is `T::nBins`. In practice it is equivalent to a *2D histogram*
in which all the bins have been **flattened and linearised**. In our specific
case the $\eta$ range is divided into `34` bins and the $\phi$ space into 126
bins. This, if we take into account the covered regions of $\eta\in[1.5,3.2]$
and $\phi\in[0,2\pi)$, will translate into a bin dimension in the
$\eta\times\phi$ space of approximately $0.05 \times 0.05$. The Tile's size has
been chosen to be big enough to host all the *relevant points* for the specific
searches we need to perform and reasonably small to perform that search on a
small number of elements (the ones contained in each *bin-vector* or in a small,
confined region of $3\times 3$ or $5 \times 5$ bins).

##### `TICLLayerTile` and `TICLLayerTiles`

In order to be generic and support `Tiles` of different dimension (both in terms
of $\eta, \phi$ coverage and in terms of number of bins), a common templated
class has been created: `TICLLayerTileT`, defined in [DataFormats/HGCalReco](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCalReco/interface/TICLLayerTile.h)

```c++
template <typename T>
class TICLLayerTileT {
public:
  void fill(double eta, double phi, unsigned int layerClusterId) {
    tile_[globalBin(eta, phi)].push_back(layerClusterId);
  }

  int etaBin(float eta) const {
    constexpr float etaRange = T::maxEta - T::minEta;
    static_assert(etaRange >= 0.f);
    float r = T::nEtaBins / etaRange;
    int etaBin = (std::abs(eta) - T::minEta) * r;
    etaBin = std::clamp(etaBin, 0, T::nEtaBins - 1);
    return etaBin;
  }

  int phiBin(float phi) const {
    auto normPhi = normalizedPhi(phi);
    float r = T::nPhiBins * M_1_PI * 0.5f;
    int phiBin = (normPhi + M_PI) * r;

    return phiBin;
  }

  int globalBin(int etaBin, int phiBin) const { return phiBin + etaBin * T::nPhiBins; }

  int globalBin(double eta, double phi) const { return phiBin(phi) + etaBin(eta) * T::nPhiBins; }

  void clear() {
    auto nBins = T::nEtaBins * T::nPhiBins;
    for (int j = 0; j < nBins; ++j)
      tile_[j].clear();
  }

  const std::vector<unsigned int>& operator[](int globalBinId) const { return tile_[globalBinId]; }

private:
  std::array<std::vector<unsigned int>, T::nBins> tile_;
};
```

Few template specializations are defined in the very same file:

```c++
namespace ticl {
  using TICLLayerTile = TICLLayerTileT<TileConstants>;
  using Tiles = std::array<TICLLayerTile, TileConstants::nLayers>;
  using TracksterTiles = std::array<TICLLayerTile, TileConstants::iterations>;
}  // namespace ticl
```

The main `TICLLayerTile` is used to partition each and every layer in HGCAL.

The `struct TileConstants` is defined in [DataFormats/HGCalReco](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCalReco/interface/Common.h) as:

```c++
namespace ticl {
  struct TileConstants {
    static constexpr float minEta = 1.5f;
    static constexpr float maxEta = 3.2f;
    static constexpr int nEtaBins = 34;
    static constexpr int nPhiBins = 126;
    static constexpr int nLayers = 104;
    static constexpr int iterations = 4;
    static constexpr int nBins = nEtaBins * nPhiBins;
  };
}  // namespace ticl
```

The `TICLLayerTile` is a wrapper around the `Tile` data structure that properly
handles *bin arithmetic*. The interface exposed to the outside is similar to an
ordinary `C++ array` (i.e. it has the `const operator[int]`, provided the correct
**global bin** is used) and also to a regular `Histogram` (i.e. it has the
`fill(x, y, value)`). Each bin in a `TICLLayerTile` will contain **the indices
of all the objects that fall into that bin**.

A `TICLLayerTiles` is built on top of `TICLLayerTile` and adds an additional
dimension to the histogram concept. The class has been templated to make it more
generic. The additional dimension could be, e.g., the **HGCAL Layer
index(0-based)** or event the **iteration number**. Again, the correct handling
of the bins and access operators `[]` are implemented in the wrapper class to
the underlying data structure. The skeleton of the `TICLLayerTiles` is like the
following:

```c++
template <typename T>
class TICLGenericTile {
public:
  // This class represents a generic collection of Tiles. The additional index
  // numbering is not handled internally. It is the user's responsibility to
  // properly use and consistently access it here.
  const TICLLayerTile& operator[](int index) const { return tiles_[index]; }
  void fill(int index, double eta, double phi, unsigned int objectId) {
    tiles_[index].fill(eta, phi, objectId);
  }

private:
  T tiles_;
};

using TICLLayerTiles = TICLGenericTile<ticl::Tiles>;
using TICLTracksterTiles = TICLGenericTile<ticl::TracksterTiles>;
```

The concept of a `TICLLayerTiles` class is illustrated in the image below:

![Navigation Tile used by **TICL**](TileNavigationTICL.png)

##### CMSSW `Tile` Producer

In the current implementation of **TICL**, a single `TICLLayerTiles` is created
by the
[`TICLLayerTileProducer`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/TICLLayerTileProducer.cc)
producer (which is an `edm::stream::EDProducer<>`) at the beginning of each
event and is stored inside the `Event`. The `TICLLayerTileProducer` is defined
in the package `RecoHGCal/TICL`.  The tile contains all the 2D `LayerClusters`
that have been reconstructed by the `CLUE` algorithm. All iterations will fetch
this tile from the `Event` and, eventually, mask it using the proper mask
filters.

???+ tip

    The configuration of `TICLLayerTileProducer` is not contained in a regular
    `python` configuration file or snippet, but it is actually automatically
    generated using the `fillDescriptions` static method. For further details,
    please [look at the
    code](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/TICLLayerTileProducer.cc#L59).

???+ info

    In order to run the TICL reconstruction, you have to be sure that the
    `process` configuration object in `python` defines the
    `ticlLayerTileProducer` producer.

##### How does it look like?

It would be nice, besides all the definitions and the technical details, to have
a visual representation of how such a `Tile` structure looks like in real life,
when superimposed to the real detector.

Since the `Tile` is arranged in constant $\eta\times\phi$ bins, the bins
themselves will not be of identical shape when projected on every single layer,
but their size will grow as we move from inner to outer radii (here radius is
meant in the transverse plane, i.e. $r=\sqrt{x^2+y^2}$ in global coordinates).

In order to have a rough understanding of how the size of the bins changes,
e.g., along the $\eta$ direction, you can have a look at the plot below, that
shows the variation along $r$ for a bin size of $0.05$ in the $\eta$ direction.

![Bin size variation as a function of $\eta$](R_vs_DeltaEta.png "Bin size variation as a function of $\eta$")

Another important aspects is to compare the size of every single bin to the size
of the Silicon cells, both in the *high density* and *low density* regions.

To guide the eye and have such an idea, you can look at the following two
images, which are relative to the first layer in HGCAL (inner and outer regions,
respectively), where the $0.05\times0.05$ bins in $\eta\times\phi$ are shown.

???+ note
     The small and grey numbers that appear at the centre of each square
     represent its global bin number in the overall `Tile` structure and the
     $\eta$ and $\phi$ bins, respectively, inside round parenthesis.

![Tile on the first layer, inner region](TICLGrid_Layer1_Inside.png "Tile on
First Layer, inner region")

![Tile on the first layer, outer region](TICLGrid_Layer1_Outside.png "Tile on
First Layer, outer region")

If you want to explore how the `Tile` will look like in the Silicon section of
the full HGCAL detector, you can browse [this
directory](https://rovere.web.cern.ch/rovere/HGCAL/LayersFromPhilippeFile/).

#### TICL Tracksters

A TICL `Trackster` is meant to collect the 2D `LayerClusters` that have been
reconstructed by each single iteration. It is defined in the package
[DataFormats/HGCalReco](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCalReco/interface/Trackster.h).
It is a Direct Acyclic Graph of connected 2D `LayerClusters` and is created by
the pattern recognition algorithms by linking them to form 3D objects. Despite
the intent, for the time being only the vertex information is stored **without
the edges between them**.  Its definition is the following:

```c++
#include <array>
#include <vector>
#include "DataFormats/Provenance/interface/ProductID.h"
#include "DataFormats/Math/interface/Vector3D.h"

#include <Eigen/Core>

// A Trackster is a Direct Acyclic Graph created when
// pattern recognition algorithms connect hits or
// layer clusters together in a 3D object.

namespace ticl {
  class Trackster {
  public:
    typedef math::XYZVector Vector;

    // types considered by the particle identification
    enum class ParticleType {
      photon = 0,
      electron,
      muon,
      neutral_pion,
      charged_hadron,
      neutral_hadron,
      ambiguous,
      unknown,
    };

    enum class PCAOrdering { ascending = 0, descending };

  ...
  ...

  private:
    // The vertices of the DAG are the indices of the
    // 2d objects in the global collection
    std::vector<unsigned int> vertices_;
    std::vector<uint8_t> vertex_multiplicity_;

    // The edges connect two vertices together in a directed doublet
    // ATTENTION: order matters!
    // A doublet generator should create edges in which:
    // the first element is on the inner layer and
    // the outer element is on the outer layer.
    std::vector<std::array<unsigned int, 2> > edges_;

    // Product ID of the seeding collection used to create the Trackster.
    // For GlobalSeeding the ProductID is set to 0. For track-based seeding
    // this is the ProductID of the track-collection used to create the
    // seeding-regions.
    edm::ProductID seedID_;

    // For Global Seeding the index is fixed to one. For track-based seeding,
    // the index is the index of the track originating the seeding region that
    // created the trackster. For track-based seeding the pointer to the track
    // can be cooked using the previous ProductID and this index.
    int seedIndex_;

    // We also need the pointer to the original seeding region ??
    // something like:
    // int seedingRegionIdx;

    // -99, -1 if not available. ns units otherwise
    float time_;
    float timeError_;

    // regressed energy
    float regressed_energy_;
    float raw_energy_;
    float raw_em_energy_;
    float raw_pt_;
    float raw_em_pt_;

    // PCA Variables
    Vector barycenter_;
    std::array<float, 3> eigenvalues_;
    std::array<Vector, 3> eigenvectors_;
    std::array<float, 3> sigmas_;
    std::array<float, 3> sigmasPCA_;

    // trackster ID probabilities
    std::array<float, 8> id_probabilities_;
  };
}  // namespace ticl
#endif
```

#### TICL Candidates

A `TICLCandidate` is a lightweight physics object made from one or multiple
`Tracksters`. It is defined in the package
[DataFormats/HGCalReco](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCalReco/interface/TICLCandidate.h).

This class is an intermediate representation of the objects created by the TICL
reconstruction before they are injected into *Particle-Flow* by the
`PFTICLProducer`.

It is derived from a `reco:LeafCandidate` and, as such, it has particleId and
energy and momentum information. In addition it has `time` and `timeError`
information that, for the time being, are not filled.

If a `TICLCandidate` is made starting from a `Trackster` produced by the
*track-seeded iteration* it will also store the pointer to the original `Track`
that produced the `Trackster`.

Its class layout is like the following:

```c++
class TICLCandidate : public reco::LeafCandidate {
public:
  typedef ticl::Trackster::ParticleType ParticleType;

  TICLCandidate(Charge q, const LorentzVector& p4)
      : LeafCandidate(q, p4), time_(0.f), timeError_(-1.f), rawEnergy_(0.f) {}

  TICLCandidate() : LeafCandidate(), time_(0.f), timeError_(-1.f), rawEnergy_(0.f) {}

  TICLCandidate(const edm::Ptr<ticl::Trackster>& trackster)
      : LeafCandidate(), time_(0.f), timeError_(-1.f), rawEnergy_(0.f), tracksters_({trackster}) {}

  inline float time() const { return time_; }
  inline float timeError() const { return timeError_; }

  void setTime(float time) { time_ = time; };
  void setTimeError(float timeError) { timeError_ = timeError; }

  inline const edm::Ptr<reco::Track> trackPtr() const { return trackPtr_; }
  void setTrackPtr(const edm::Ptr<reco::Track>& trackPtr) { trackPtr_ = trackPtr; }

  inline float rawEnergy() const { return rawEnergy_; }
  void setRawEnergy(float rawEnergy) { rawEnergy_ = rawEnergy; }

  inline const std::vector<edm::Ptr<ticl::Trackster> > tracksters() const { return tracksters_; };

  void setTracksters(const std::vector<edm::Ptr<ticl::Trackster> >& tracksters) { tracksters_ = tracksters; }
  void addTrackster(const edm::Ptr<ticl::Trackster>& trackster) { tracksters_.push_back(trackster); }
  // convenience method to return the ID probability for a certain particle type
  inline float id_probability(ParticleType type) const {
    // probabilities are stored in the same order as defined in the ParticleType enum
    return idProbabilities_[(int)type];
  }

  void setIdProbabilities(const std::array<float, 8>& idProbs) { idProbabilities_ = idProbs; }

private:
  float time_;
  float timeError_;
  edm::Ptr<reco::Track> trackPtr_;

  float rawEnergy_;

  // vector of Ptr so Tracksters can come from different collections
  // and there can be derived classes
  std::vector<edm::Ptr<ticl::Trackster> > tracksters_;

  // Since it contains multiple tracksters, duplicate the probability interface
  std::array<float, 8> idProbabilities_;
};
```


### Components

#### HGCAL RecHits

The `HGCAL RecHits` are the starting block of the HGCAL reconstruction chain.

The way in which they are calibrated is described in [another
section](http://hgcal.web.cern.ch/hgcal/HitCalibration/hitCalibration/).

They are produced by the
[`HGCalRecHitProducer`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalRecHitProducer.cc)
producer which is defined in the package
[RecoLocalCalo/HGCalRecProducers](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py).

Three different `RecHitCollections` of `C++` type `HGCalRecHit` are produced by
default:

   * `HGCEERecHits`
   * `HGCHEFRecHits`
   * `HGCHEBRecHits`

according to HGCAL regions described in the [introduction][1].

[1]: ../index.md

In order to consume them separately, you need to use the following `InputTags`:

   * `edm::InputTag("HGCalRecHit", "HGCEERecHits"))`
   * `edm::InputTag("HGCalRecHit", "HGCHEFRecHits"))`
   * `edm::InputTag("HGCalRecHit", "HGCHEBRecHits"))`

???+ tip

    It is rather complex, in few cases, to really follow how a specific `python`
    configuration file is expanded and modified by the different `eras` and
    `suberas` mechanisms that are used, particularly for the *Phase2*
    reconstruction. If you really want to discover the **final** `python`
    configuration that is used and passed down to configure the `C++` code, you
    can interactively load a configuration file in `ipython` and then, at the
    prompt, ask the `process` object to dump the configuration of a specific
    module. For example, in order to understand how the `HGCalRecHit` producer
    (this is its `python` label) will be configured, you can do the following:

       * At the `shell` command prompt, digit `ipython -i
       your_reconstruction_cfg.py`.
       * At the `ipython` prompt, digit `process.HGCalRecHit` and hit `Enter`.
       * You should see printed on the screen its fully expanded
       configuration.
       * If you are interested in specific parameters you can inspect them by
       requesting the corresponding object, e.g., by issuing the command
       `process.HGCalRecHit.layerWeights`.

#### HGCAL Layer Clustering

The current 2D layer clustering is based on the `CLUE` algorithm that is
described in [another
section](http://hgcal.web.cern.ch/hgcal/Reconstruction/clueAlgorithm/).

We will not review the full algorithm's logic here, but we list the main steps
that are execute in order to cluster `RecHits` into `LayerClusters`.

   * For each `RecHit`, compute its local density (our algorithm is density
     based)

   * For each `RecHit`, compute the distance to its nearest neighbour with
     higher local density

   * Promote `RecHits` that fulfill the proper requirements to be seed for the
     `LayerClusters`

   * Assign the remaining `RecHits` to the proper cluster, follow the **flow
     (i.e. the gradient) of density**.

Let's now focus on the configuration parameters, find where they are defined and
describe how to, in case, add novel clustering algorithms to be plugged directly
into the CMSSW Release.

##### Configuration Parameters

The configuration parameters for the clustering algorithms are defined in the
file
[`hgcalLayerClusters_cff.py`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/hgcalLayerClusters_cff.py)
in the package `RecoLocalCalo/HGCalRecProducers`.
We attach below the configuration:

```python
import FWCore.ParameterSet.Config as cms

from RecoLocalCalo.HGCalRecProducers.hgcalLayerClusters_cfi import hgcalLayerClusters as hgcalLayerClusters_

from RecoLocalCalo.HGCalRecProducers.HGCalRecHit_cfi import dEdX, HGCalRecHit

from RecoLocalCalo.HGCalRecProducers.HGCalUncalibRecHit_cfi import HGCalUncalibRecHit

from SimCalorimetry.HGCalSimProducers.hgcalDigitizer_cfi import fC_per_ele, hgceeDigitizer, hgchebackDigitizer

hgcalLayerClusters = hgcalLayerClusters_.clone()

hgcalLayerClusters.timeOffset = hgceeDigitizer.tofDelay
hgcalLayerClusters.plugin.dEdXweights = cms.vdouble(dEdX.weights)
hgcalLayerClusters.plugin.fcPerMip = cms.vdouble(HGCalUncalibRecHit.HGCEEConfig.fCPerMIP)
hgcalLayerClusters.plugin.thicknessCorrection = cms.vdouble(HGCalRecHit.thicknessCorrection)
hgcalLayerClusters.plugin.fcPerEle = cms.double(fC_per_ele)
hgcalLayerClusters.plugin.noises = cms.PSet(refToPSet_ = cms.string('HGCAL_noises'))
hgcalLayerClusters.plugin.noiseMip = hgchebackDigitizer.digiCfg.noise
```

As one case see, the parameters `dEdXweights`, `fcPerMip`,
`thicknessCorrection`, `fcPerEle`, `noises` and `noiseMip` are all imported from
the corresponding definitions used to create the `HGCalRecHits`. This will
ensure that if those are modified upstream, the changes will be automatically
propagated also to the clustering algorithms in a transparent way.


Also in this case the configuration parameters for the clustering algorithms are
not only stored in `python` files but are also **dynamically generated**
starting directly from the `C++` code, after the compilation phase. In this
specific case it is not done using directly the `static void
fillDescriptions(...)` function, but rather the `static void
fillPSetDescription(...)` function. This is due to the fact that the algorithm
itself is designed as a _plugin in CMSSW_, so that special functions have to be
used to dynamically generate (and **validate**) the configuration parameters.
More on this in the next section.

The configuration parameters are declared in [these
lines](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalCLUEAlgo.h#L78-L105),
that we paste here:

```c++ hl_lines="11 12 3"
static void fillPSetDescription(edm::ParameterSetDescription& iDesc) {
  iDesc.add<std::vector<double>>("thresholdW0", {2.9, 2.9, 2.9});
  iDesc.add<std::vector<double>>("deltac",
                                 {
                                     1.3,
                                     1.3,
                                     1.3,
                                     0.0315,  // for scintillator
                                 });
  iDesc.add<bool>("dependSensor", true);
  iDesc.add<double>("ecut", 3.0);
  iDesc.add<double>("kappa", 9.0);
  iDesc.addUntracked<unsigned int>("verbosity", 3);
  iDesc.add<std::vector<double>>("dEdXweights", {});
  iDesc.add<std::vector<double>>("thicknessCorrection", {});
  iDesc.add<std::vector<double>>("fcPerMip", {});
  iDesc.add<double>("fcPerEle", 0.0);
  edm::ParameterSetDescription descNestedNoises;
  descNestedNoises.add<std::vector<double>>("values", {});
  iDesc.add<edm::ParameterSetDescription>("noises", descNestedNoises);
  edm::ParameterSetDescription descNestedNoiseMIP;
  descNestedNoiseMIP.add<bool>("scaleByDose", false);
  iDesc.add<edm::ParameterSetDescription>("scaleByDose", descNestedNoiseMIP);
  descNestedNoiseMIP.add<std::string>("doseMap", "");
  iDesc.add<edm::ParameterSetDescription>("doseMap", descNestedNoiseMIP);
  descNestedNoiseMIP.add<double>("noise_MIP", 1. / 100.);
  iDesc.add<edm::ParameterSetDescription>("noiseMip", descNestedNoiseMIP);
  iDesc.add<bool>("use2x2", true);  // use 2x2 or 3x3 scenario for scint density calculation
}
```

We list in the following table the most important parameters, their meaning and
their default values.

 Paramenter Name | Meaning | Default Value | Notes
 --------------- | ------- | ------------- | -----
 **noises,   noiseMip,   fcPerEle,   fcPerMip,   dEdXweights, thicknessCorrection, sciThicknessCorrection**|$\sigma_{\mathrm{Noise}}=\frac{fcPerEle*noises[j]*dEdXweights[i]}{fcPerMip[j]*thicknessCorrection[j]}$ for the j-th thick, i-th layer for Si sensor,$\sigma_{\mathrm{Noise}}=\frac{noiseMip*dEdXweights[i]}{sciThicknessCorrection}$ for Sci sensor|-|They are all imported from the corresponding definitions used to create the HGCalRecHits
 **ecut**        | Select the minimum requirement, in terms of $\sigma_{\mathrm{Noise}}$, to select hits to cluster | 3.0 | The numbers are tailored to the specificity of each  detector components if `dependSensor` is set to `True` (its default value)
 **kappa**       | Minimum value, in terms of $\sigma_{\mathrm{Noise}}$, to promote a hit to become a seed candidate for a cluster | 9.0 |
 **deltac**      | Critical distance, in cm, over which compute the *local density* around each and every selected hit | 1.3 cm for Silicon Sensor, 0.0315 (as $\Delta\eta\times\Delta\phi$ search box) for Scintillators | The same values is also used to promote a `RecHit` as a seed if its distance to its nearest with higher density if greater than `deltac`. This parameter, doubled, is also used to defined outliers for hits with a local density below the threshold value. Distances in the case of $\eta\times\phi$ are computed in that space, namely $\mathrm{distance}=\sqrt{\eta^2+\phi^2}$.
 **use2x2**      |If true, the local density of Sci sensor is $\max(northeast, northwest,southeast, southwest)$|True|See the details in [[1](https://indico.cern.ch/event/780244/contributions/3247853/attachments/1770235/2876138/LayerClustering.pdf),[2](https://indico.cern.ch/event/827633/contributions/3463809/attachments/1861051/3058641/LC-June-12.pdf),[3](https://indico.cern.ch/event/832849/contributions/3492963/attachments/1878032/3093327/LC-July-10.pdf)]


##### `C++` Code

The clustering algorithms, in `CMSSW`, have been implemented as plugins. This
means that there is a *base class* from which they must be derived:
[`HGCalClusteringAlgoBase`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/interface/HGCalClusteringAlgoBase.h).

The
[`HGCalCLUEAlgo`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalCLUEAlgo.h)
is, in fact, derived from `HGCalClusteringAlgoBase`.


This means that there's also a **plugin factory**
([`HGCalLayerClusterPluginFactory`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalLayerClusterPluginFactory.cc))
that can be used to *instantiate* the proper *implementation*.

```c++
#include "RecoLocalCalo/HGCalRecProducers/interface/HGCalLayerClusterAlgoFactory.h"
#include "RecoLocalCalo/HGCalRecProducers/interface/HGCalClusteringAlgoBase.h"
#include "RecoLocalCalo/HGCalRecProducers/interface/HGCalImagingAlgo.h"
#include "RecoLocalCalo/HGCalRecProducers/plugins/HGCalCLUEAlgo.h"
#include "FWCore/ParameterSet/interface/ValidatedPluginFactoryMacros.h"
#include "FWCore/ParameterSet/interface/ValidatedPluginMacros.h"

EDM_REGISTER_VALIDATED_PLUGINFACTORY(HGCalLayerClusterAlgoFactory, "HGCalLayerClusterAlgoFactory");
DEFINE_EDM_VALIDATED_PLUGIN(HGCalLayerClusterAlgoFactory, HGCalImagingAlgo, "Imaging");
DEFINE_EDM_VALIDATED_PLUGIN(HGCalLayerClusterAlgoFactory, HGCalCLUEAlgo, "CLUE");
DEFINE_EDM_VALIDATED_PLUGIN(HGCalLayerClusterAlgoFactory, HFNoseCLUEAlgo, "HFNoseCLUE");
```

The usage of the *macro* `DEFINE_EDM_VALIDATED_PLUGIN` will ensure that the
automatic creation and validation of the configuration parameters for the
registered plugins will happen through the usage of the `static void
fillPsetDescription(...)`, as documented
[here](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideConfigurationValidationAndHelp#A_Plugin_Module_Using_Another_He).


An example of usage of a *plugin factory* is the following:

```c++
auto pluginPSet = ps.getParameter<edm::ParameterSet>("plugin");
algo = HGCalLayerClusterAlgoFactory::get()->create(
         pluginPSet.getParameter<std::string>("type"),
         pluginPSet);
algo->setAlgoId(algoId);
```

Also in this case, to support different detectors, i.e., to enable the
possibility of running the `CLUE` algorithm using different `Tile` data
structures, the main algorithm has been templated and derived from the common
base-class `HGCalClusteringAlgoBase`. Two template specializations exist, at
present, namely:

```c++
using HGCalCLUEAlgo = HGCalCLUEAlgoT<HGCalLayerTiles>;
using HFNoseCLUEAlgo = HGCalCLUEAlgoT<HFNoseLayerTiles>;
```

that are defined in [HGCalCLUEAlgo.h](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalCLUEAlgo.h)
Both specializations have been registered as plugins in `CMSSW`.

##### HGCalLayerClusterProducer

The real layer clusters producer is called
[`HGCalLayerClusterProducer`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalLayerClusterProducer.cc)
and lives in the package `RecoLocalCalo/HGCalRecProducers`. It internally has a
pointer to the real algorithm that the user selected via the `python`
configuration files.  Currently the clusters produced are of the `C++` type
`std::vector<reco::BasicCluster>` for legacy purposes, but in the near future we
would like to design novel data structures that better suite our needs.

This very same producer will also produce an `edm::ValueMap<std::pair<float,
float> >` that will store the **timing information plus error** for each layer
cluster created, if available. A default value of $-99.$ is used to indicate
that the cluster has no timing information whatsoever. The `python` label
assigned to the `ValueMap` is set from the configuration parameter `timeClname`
whose default value is `timeLayerCluster`,

In addition to create the 2D Layer Clusters and their associated `ValueMap` to
store their timing information, the `HGCalLayerClusterProducer` will also
**produce a mask**: `std::vector<float>>("InitialLayerClustersMask")`. This mask
will be initially filled with the value $1.0$ for all its elements. This means
that the *totality* of the energy associated to each Layer Cluster is still
available and could be used to form additional `Tracksters`. In other words, in
order for a Layer Cluster to be fully masked, a value of $0$ has to be stored at
the proper location inside this map (the location being identified by the
**global index** of this Layer Cluster in the overall 2D cluster collection).

The mask is expressed in terms of floating point numbers to allow for *sharing*
of 2D layer clusters among different TICL iterations (this has yet to be
implemented, since currently the masking is binary: either $0.$, i.e. fully
masked, or $1.$, i.e. fully available).

#### HGCAL Layer Masking

The action of masking the 2D Layer Clusters after they have been reconstructed
by a TICL iteration is performed by the
[`FilteredLayerClustersProducer`](https://github.com/rovere/cmssw/blob/TICL_inReco/RecoHGCal/TICL/plugins/FilteredLayerClustersProducer.cc)
producer. It is defined in the package `RecoHGCal/TICL`.

Once again the *default configuration* of a `FilteredLayerClustersProducer` is
**dynamically computed** at compile time via the `static void
fillDescriptions(...)` method:

```c++
void FilteredLayerClustersProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  // hgcalMultiClusters
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("HGCLayerClusters", edm::InputTag("hgcalLayerClusters"));
  desc.add<edm::InputTag>("LayerClustersInputMask", edm::InputTag("hgcalLayerClusters", "InitialLayerClustersMask"));
  desc.add<std::string>("iteration_label", "iterationLabelGoesHere");
  desc.add<std::string>("clusterFilter", "ClusterFilterByAlgoAndSize");
  desc.add<int>("algo_number", 9);
  desc.add<int>("min_cluster_size", 0);
  desc.add<int>("max_cluster_size", 9999);
  descriptions.add("filteredLayerClustersProducer", desc);
}
```

The most important parameters are described in the table below.

 Parameter Name | Meaning | Default Value | Notes
 -------------- | ------- | ------------- | -----
 **HGCalLayerClusters** | This is the **global collection** of all 2D Layer Clusters that have been created. | `edm::InputTag("hgcalLayerClusters")` | This is the product of the `HGCalLayerClusterProducer`
 **LayerClustersInputMask** | This is the **initial mask** starting from which we would like to apply an **additional masking** | `edm::InputTag("hgcalLayerClusters", "InitialLayerClustersMask")` | This is the product of the `HGCalLayerClusterProducer`
 **iteration_label** | This is the label that will be attached to the created mask. | `iterationLabelGoesHere` | Usually it matches the iteration name
 **clusterFilter** | A string identifying the **filter** that will be used to mask the Layer Clusters | `ClusterFilterByAlgoAndSize` | Filters are dynamically loaded using a `PluginFactory`
 **algo_number** | A integer number identifying the algorithm that created the 2D Layer Clusters | $9$ | Currently we only have 1 valid algorithm.
 **min_cluster_size** | The minimum size in terms of cells composing the Layer Cluster to **select it** | 0 | It is inclusive
 **max_cluster_size** | The maximum size in terms of cells composing the Layer Cluster to **select it** | 9999 | It is inclusive

The filter will produce a `<std::vector<float>>(iteration_label);` that
represents the **cumulative mask** to be considered when using the mask.
**Cumulative** in the sense that the selection of the filter is *applied on top
of the input mask selected by the user at configuration time*.  The logic is the
usual one: a value greater than $0.$ means that the cluster can still be used, a
value of $0.$ means that the corresponding cluster is fully masked.


##### Layer Clusters' Filters

All Layer Cluster Filters must be derived from the abstract base class
[`ClusterFilterBase`](https://github.com/rovere/cmssw/blob/TICL_inReco/RecoHGCal/TICL/plugins/ClusterFilterBase.h)
and implement the abstract method:

```c++
virtual void filter(const std::vector<reco::CaloCluster>& layerClusters,
                        const HgcalClusterFilterMask& mask,
                        std::vector<float>& layerClustersMask,
                        hgcal::RecHitTools& rhtools) const = 0;
```

Currently we have implemented 3 filters:
[`ClusterFilterByAlgo`](https://github.com/rovere/cmssw/blob/TICL_inReco/RecoHGCal/TICL/plugins/ClusterFilterByAlgo.h),
[`ClusterFilterBySize`](https://github.com/rovere/cmssw/blob/TICL_inReco/RecoHGCal/TICL/plugins/ClusterFilterBySize.h)
and
[`ClusterFilterByAlgoAndSize`](https://github.com/rovere/cmssw/blob/TICL_inReco/RecoHGCal/TICL/plugins/ClusterFilterByAlgoAndSize.h)



#### TICL Seeding Regions

A *Seeding Region* in the TICL reconstruction identifies a ROI inside which the
subsequent *Pattern Recognition Algorithm* will try to build `Tracksters`.

The seeding regions are produced by the
[`TICLSeedingRegionProducer`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/TICLSeedingRegionProducer.cc)
producer that is defined in the package `RecoHGCal/TICL`. The product saved by
the producer in the event is of `C++` type `std::vector<TICLSeedingRegion>`:
this means that a single producer can create several ROIs.

The **seeding algorithm** that will be used to create the seeding regions is
injected into the producer using the by now familiar `PluginFactory` mechanism.
Each seeding algorithm has to inherit from the base class
[`SeedingRegionAlgoBase`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/SeedingRegionAlgoBase.h)
and to implement the abstract methods:

```c++
virtual void initialize(const edm::EventSetup& es) = 0;

virtual void makeRegions(const edm::Event& ev,
                         const edm::EventSetup& es,
                         std::vector<TICLSeedingRegion>& result) = 0;
```

A single
[`TICLSeedingRegion`](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HGCalReco/interface/TICLSeedingRegion.h)
is defined as:

```c++
#include "DataFormats/HGCalReco/interface/Common.h"
#include "DataFormats/Math/interface/normalizedPhi.h"
#include "DataFormats/GeometryVector/interface/GlobalPoint.h"
#include "DataFormats/GeometryVector/interface/GlobalVector.h"
#include "DataFormats/Provenance/interface/ProductID.h"

struct TICLSeedingRegion {
  GlobalPoint origin;
  GlobalVector directionAtOrigin;

  // zSide can be either 0(neg) or 1(pos)
  int zSide;
  // the index in the seeding collection
  // with index = -1 indicating a global seeding region
  int index;
  // collectionID = 0 used for global seeding collection
  edm::ProductID collectionID;

  TICLSeedingRegion() {}

  TICLSeedingRegion(GlobalPoint o, GlobalVector d,
                    int zS, int idx, edm::ProductID id) {
    origin = o;
    directionAtOrigin = d;
    zSide = zS;
    index = idx;
    collectionID = id;
  }
};
```

At present only 2 seeding algorithms have been implemented:

   * [`SeedingRegionGlobal`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/SeedingRegionGlobal.cc)

   * [`SeedingRegionByTracks`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/SeedingRegionByTracks.cc).

The `SeedingRegionGlobal` will create a **global seeding region** in the sense
that the **full HGCAL detector** will be used by the Pattern Recognition
Algorithm to create Tracksters.

The `SeedingRegionByTracks`, instead, will create one region per track entering
the entrance surface of the HGCAL detector that will satisfy some selection
criteria.

Currently the configuration parameters for the `TICLSeedingRegionProducer` **and
for the seeding algorithm that will be internally used** are created via the
  `static void fillDescriptions(...)` mechanism:

```c+
void TICLSeedingRegionProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.add<int>("algo_verbosity", 0);
  desc.add<edm::InputTag>("tracks", edm::InputTag("generalTracks"));
  desc.add<std::string>("cutTk",
                        "1.48 < abs(eta) < 3.0 && pt > 2. && quality(\"highPurity\") && "
                        "hitPattern().numberOfLostHits(\"MISSING_OUTER_HITS\") < 10");
  desc.add<std::string>("propagator", "PropagatorWithMaterial");
  desc.add<int>("algoId", 1);
  descriptions.add("ticlSeedingRegionProducer", desc);
}
```

##### SeedingRegionByTracks Configuration

The most important parameters that could be used to configure this algorithm are
illustrated in the table below.

 Parameter Name | Meaning | Default Value | Notes
 -------------- | ------- | ------------- | -----
 **tracks**     | Defines the `InputTag` of the track collection used to create the seeding regions | `edm::InputTag("generalTracks")` |
 **cutTr**      | Defines a set of cuts that will be applied to each track within the selected track collection to consider them as potential seed candidate | `"1.48 < abs(eta) < 3.0 && pt > 2. && quality("highPurity") && hitPattern().numberOfLostHits("MISSING_OUTER_HITS") < 10"` |
 **propagator** | Set the track propagator to be used to extrapolate the track position at the front-face of the HGCAL detector | `PropagatorWithMaterial` |




#### TICL Pattern Recognition

The core of the TICL reconstruction is represented by the **Pattern Recognition
Algorithm**.

*From the code point of view*, the main building block is represented by the
[`TrackstersProducer`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/TrackstersProducer.cc)
producer that is defined in the package `RecoHGCal/TICL`. This producer will put
into the event a `std::vector<Trackster>` and the corresponding mask
`std::vector<float>` to be applied to the 2D Layer Clusters at later iterations.

The core point of the producer is that it internally **uses** the *pattern
recognition algorithm and the full list of the components that we have been
discussing before*. **In a sense, this will put everything at work together**.

Currently, the hard-coded pattern recognition algorithm is
[`PatternRecognitionbyCA`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/PatternRecognitionbyCA.h). Another available algorithm is called [`CLUE3D`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/PatternRecognitionbyCLUE3D.h)
On a more general ground, if the user wants to try a novel pattern recognition
algorithm, it has to derive it from the base class
[`PatternRecognitionAlgoBase`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/PatternRecognitionAlgoBase.h)
and implement the abstract method:

```c++
virtual void makeTracksters(const Inputs& input,
                            std::vector<Trackster>& result,
                            std::unordered_map<int, std::vector<int>>& seedToTracksterAssociation) = 0;
```

For practical purposes, all the necessary ingredients to create the `Tracksters`
have been collected into the `Inputs` data structure:

```c++
struct Inputs {
      const edm::Event& ev;
      const edm::EventSetup& es;
      const std::vector<reco::CaloCluster>& layerClusters;
      const std::vector<float>& mask;
      const edm::ValueMap<std::pair<float, float>>& layerClustersTime;
      const TICLLayerTiles& tiles;
      const std::vector<TICLSeedingRegion>& regions;
}
```

##### Configuration Parameters of PatternRecognizationbyCA

Once again, the main configuration parameters are automatically generated via
the `static void fillDescriptions(...)` mechanism.

???+ info

     It is worth stressing the fact that **the very same configuration
     parameters** used to configure the `TrackstersProducer` **will be passed to
     the Pattern Recognition Algorithm**.

We attach below its implementation.

```c++
void TrackstersProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  // hgcalMultiClusters
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("layer_clusters", edm::InputTag("hgcalLayerClusters"));
  desc.add<edm::InputTag>("filtered_mask", edm::InputTag("filteredLayerClusters", "iterationLabelGoesHere"));
  desc.add<edm::InputTag>("original_mask", edm::InputTag("hgcalLayerClusters", "InitialLayerClustersMask"));
  desc.add<edm::InputTag>("time_layerclusters", edm::InputTag("hgcalLayerClusters", "timeLayerCluster"));
  desc.add<edm::InputTag>("layer_clusters_tiles", edm::InputTag("ticlLayerTileProducer"));
  desc.add<edm::InputTag>("seeding_regions", edm::InputTag("ticlSeedingRegionProducer"));
  desc.add<std::vector<int>>("filter_on_categories", {0});
  desc.add<double>("pid_threshold", 0.);  // make default such that no filtering is applied
  desc.add<int>("algo_verbosity", 0);
  desc.add<double>("min_cos_theta", 0.915);
  desc.add<double>("min_cos_pointing", -1.);
  desc.add<int>("missing_layers", 0);
  desc.add<double>("etaLimitIncreaseWindow", 2.1);
  desc.add<int>("min_clusters_per_ntuplet", 10);
  desc.add<double>("max_delta_time", 3.);  //nsigma
  desc.add<bool>("out_in_dfs", true);
  desc.add<int>("max_out_in_hops", 10);
  desc.add<bool>("oneTracksterPerTrackSeed", false);
  desc.add<bool>("promoteEmptyRegionToTrackster", false);
  desc.add<std::string>("eid_graph_path", "RecoHGCal/TICL/data/tf_models/energy_id_v0.pb");
  desc.add<std::string>("eid_input_name", "input");
  desc.add<std::string>("eid_output_name_energy", "output/regressed_energy");
  desc.add<std::string>("eid_output_name_id", "output/id_probabilities");
  desc.add<std::string>("itername", "unknown");
  desc.add<double>("eid_min_cluster_energy", 1.);
  desc.add<int>("eid_n_layers", 50);
  desc.add<int>("eid_n_clusters", 10);
  descriptions.add("trackstersProducer", desc);
}
```

The most important parameters are described in the table below.

 Parameter Name | Meaning | Default Value | Notes
 -------------- | ------- | ------------- | -----
 **layer_clusters** | The collection of all the 2D Layer Clusters | `edm::InputTag("hgcalLayerClusters")` | This is used to properly address the 2D LayerClusters by **index** in the `Tile`
 **filtered_mask**  | The mask to be applied to select 2D Layer Clusters | `edm::InputTag("filteredLayerClusters", "iterationLabelGoesHere")` | This is the mask created by the `FilteredLayerClustersProducer`
 **original_mask**  | The mask used as starting point to create the final output mask from this iteration | `edm::InputTag("hgcalLayerClusters", "InitialLayerClustersMask")` | The final output mask will add masking from the current iteration on top of this mask
 **time_layerclusters** | The `ValueMap` used to assign the timing to the 2D Layer Clusters | `edm::InputTag("hgcalLayerClusters", "timeLayerCluster")` | This is created by the [`HGCalLayerClusterProducer`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalLayerClusterProducer.cc)
 **layer_clusters_tiles** | The `TICLLayerTiles` to be used | `edm::InputTag("ticlLayerTileProducer")` |
 **seeding_regions** | The seeding region to be used | `edm::InputTag("ticlSeedingRegionProducer")` |
 **min_cos_theta** | Defines the minimal alignment condition for 2 doublets to be linked together | $0.915$ | This is a cut on the angle between the 2 doublets, see picture below.
 **min_cos_pointing** | Defines the angular compatibility between each outermost doublet and the **origin of the seeding region** | $-1$ | For the global seeding region the origin is $(0,0,0)$. For the track-based seeding region its the intersection of the track wrt the front fact of HGCAL and its momentum in that point. See picture below.
 **missing_layers** | Defines the number of layers to skip while linking 2D Layer Clusters together | $0$ | $0$ implies to connect only sibling layers w/o jumps.
 **min_clusters_per_ntuplet** | Defines the minimum number of 2D Layer Clusters in a Trackster to keep it | $10$ | **The number is cumulative, not by layer**
 **max_delta_time** | Defines the time compatibility (in ns) between doublets | $0.09$ | Set to $-1$ to **disable the cut**. The cut is applied iff **both clusters have timing information**
 **out_in_dfs** | Boolean to enable/disable the out_in recovery step while doing dfs on the graph | `True` |
 **max_out_in_hops** | Maximum number of out_in jumps to perform | $10$ | *To be used with care* |
 **eid\*** | Parameters related to PID and energy regression | | They will be covered later

![min_cos_theta Angle Definition]( doubletAlignment.png "min_cos_theta Angle Definition")
![min_cos_pointing Angle Definition]( doubletPointing.png "min_cos_pointing Angle Definition")


##### Configuration Parameters of PatternRecognizationbyCLUE3D
<font color=blue>Yu : add description of CLUE3D</font>
Below is the implementation.

```c++
void PatternRecognitionbyCLUE3D<TILES>::fillPSetDescription(edm::ParameterSetDescription &iDesc) {
  iDesc.add<int>("algo_verbosity", 0);
  iDesc.add<double>("criticalDensity", 4)->setComment("in GeV");
  iDesc.add<int>("densitySiblingLayers", 3);
  iDesc.add<double>("densityEtaPhiDistanceSqr", 0.0008);
  iDesc.add<bool>("densityOnSameLayer", false);
  iDesc.add<double>("criticalEtaPhiDistance", 0.035);
  iDesc.add<double>("outlierMultiplier", 2);
  iDesc.add<int>("minNumLayerCluster", 5)->setComment("Not Inclusive");
  iDesc.add<std::string>("eid_input_name", "input");
  iDesc.add<std::string>("eid_output_name_energy", "output/regressed_energy");
  iDesc.add<std::string>("eid_output_name_id", "output/id_probabilities");
  iDesc.add<double>("eid_min_cluster_energy", 1.);
  iDesc.add<int>("eid_n_layers", 50);
  iDesc.add<int>("eid_n_clusters", 10);
}
```

The parameters are described in the table below.


  Parameter Name | Meaning | Default Value | Notes
  -------------- | ------- | ------------- | -----
  **criticalDenstity** $\rho_{c}$| threshold of density for seeding     | 4 GeV              |
  **densitySiblingLayer**| layer window for density calculation    |               3|
  **densityEtaPhiDistanceSqr**|  square of distance threshold in $\eta\times\phi$ plane for density calculation|    0.0008 |
  **densityOnSameLayer**| whether add the cluster in the same layer | 0| 
  **criticalEtaPhiDistance** $\delta_{c}$| if the distance to higher density cell is larger than this value, the cell will be promoted as a seed. | 0.035|  
  **outlierMultiplier** | if $\delta_{i}<outlierMultiplier*\delta_{c}$ and $\rho_{i}<\rho_{c}$, the seed is taken as outlier.| 2|
  **minNumLayerCluster**| minimum number of layer clusters| 5|

#### Particle ID probabilities  and Energy Regression

As we saw, particle ID and energy regression are first citizens in the TICL
reconstruction. There are several parameters that, in the present
implementation, are needed in order to run particle ID and energy regression.
They are listed in the table below.

??? note

    Particle ID and energy regression have been implemented using machine
    learning techniques. It is important to stress few other points.

       * In order to properly run them, an external network model has to be
         loaded as an *external data module* in CMSSW

       * **The model have been trained with a slightly different configuration
         of the TICL iterations, therefore, at present, the performances are
         definitely sub-optimal. A retraining is in the making.**

       * The inference for particle ID and energy regression is always run on
         the reconstructed `Tracksters`. The selection using those quantities,
         though, is up to the user.

 Parameter Name | Meaning | Default Value | Notes
 -------------- | ------- | ------------- | -----
  **eid_graph_path** | Location of the Tensorflow model | `RecoHGCal/TICL/data/tf_models/energy_id_v0.pb`
  **eid_input_name** | Name of the input features | `input`
  **eid_output_name_energy** | Output variable for energy regression | `output/regressed_energy`
  **eid_output_name_id** | Output variable for particle ID | `output/id_probabilities`
  **eid_min_cluster_energy** | Minimum energy requirement to run pid and energy regression | 1.
  **eid_n_layers** | Number of layers to build the feature input image (cols) | 50
  **eid_n_clusters** | Number of Layer Cluster to build the feature input image (rows) | 10

###### Energy regression and particle identification strategy

The particle ID and energy regression is currently hard-coded directly into the
[`PatternRecognitionbyCA`](https://github.com/cms-sw/cmssw/blob/master/RecoHGCal/TICL/plugins/PatternRecognitionbyCA.cc#L120)
code.

Luckily, the code has been thoroughly documented in place. We report here the
main steps involved in running it.


   1. Set default values for regressed energy and particle id for each trackster.

   1. Store indices of tracksters whose total sum of cluster energies is above
      the `eidMinClusterEnergy_` (GeV) threshold. Inference is not applied for
      soft tracksters.

   1. When no trackster passes the selection, return.

   1. Create input and output tensors. The batch dimension is determined by the
      number of selected tracksters.

   1. Fill input tensors with layer cluster features. Per layer, clusters are
      ordered descending by energy. Given that tensor data is contiguous in
      memory, we can use pointer arithmetic to fill values, even with batching.

   1. Zero-fill features for empty clusters in each layer.

   1. Batched inference.

   1. Assign the regressed energy and id probabilities to each trackster.

The output of the energy regression inference process will be saved into the
`regressed_energy` data member of the `Trackster`.

The output of the particle ID inference process will be saved into the
`id_probabilities` array of the `Trackster`.


### Designing a TICL Iteration

#### Preliminary Setup

We have now all the ingredients needed to assemble a single iteration:

   * 2D Layer Clusters
   * A filter on them (if needed)
   * A seeding region (global or regional)
   * A Pattern Recognition Algorithm (the *Cellular Automaton*) with its parameters properly tuned.
   * A PID&Energy Regression

We could start from scratch and, with some patience, put together an iteration,
but maybe it would be quicker and also more instructive to start from a real
example.

In order to do that it is best if we start from a recent *Integration Build* and
*merge* a development branch on top. This is easily done using the following
instructions:

```shell
cmsrel CMSSW_12_1_0_pre2
cd CMSSW_12_1_0_pre2/src/
cmsenv
git cms-init
```

We will base our example on top of `CMSSW_12_1_0_pre2` since this is the first
pre-release that integrates the `TICL` framework and sequences in the official
Phase2 reconstruction workflow.

???+ warning

    For compatibility reasons with the rest of the `Phase2` reconstruction, the
    `Tracksters` created by TICL will be always *translated into the legacy
    format of `MultiClusters`*. This will allow the user to visually inspect them
    using `Fireworks/cmsShow`.

##### Generic TICL Iteration

Let's have a look at how a possible electro-magnetic TICL iteration can be defined in
`CMSSW`.

```python
import FWCore.ParameterSet.Config as cms

from RecoHGCal.TICL.TICLSeedingRegions_cff import ticlSeedingGlobal
from RecoHGCal.TICL.ticlLayerTileProducer_cfi import ticlLayerTileProducer as _ticlLayerTileProducer
from RecoHGCal.TICL.trackstersProducer_cfi import trackstersProducer as _trackstersProducer
from RecoHGCal.TICL.filteredLayerClustersProducer_cfi import filteredLayerClustersProducer as _filteredLayerClustersProducer
from RecoHGCal.TICL.multiClustersFromTrackstersProducer_cfi import multiClustersFromTrackstersProducer as _multiClustersFromTrackstersProducer

# CLUSTER FILTERING/MASKING

filteredLayerClustersEM = _filteredLayerClustersProducer.clone(
    clusterFilter = "ClusterFilterByAlgoAndSize",
    min_cluster_size = 2, # inclusive
    algo_number = 8,
    LayerClustersInputMask = 'ticlTrackstersTrk',
    iteration_label = "EM"
)

# CA - PATTERN RECOGNITION

ticlTrackstersEM = _trackstersProducer.clone(
    filtered_mask = cms.InputTag("filteredLayerClustersEM", "EM"),
    original_mask = 'ticlTrackstersTrk',
    seeding_regions = "ticlSeedingGlobal",
    filter_on_categories = [0, 1],
    pid_threshold = 0.8,
    max_out_in_hops = 4,
    missing_layers = 1,
    min_clusters_per_ntuplet = 10,
    min_cos_theta = 0.978,  # ~12 degrees
    min_cos_pointing = 0.9, # ~25 degrees
    max_delta_time = 3.,
    itername = "EM",
    algo_verbosity = 0,
)

# MULTICLUSTERS

ticlMultiClustersFromTrackstersEM = _multiClustersFromTrackstersProducer.clone(
    Tracksters = "ticlTrackstersEM"
)

ticlEMStepTask = cms.Task(ticlSeedingGlobal
    ,filteredLayerClustersEM
    ,ticlTrackstersEM
    ,ticlMultiClustersFromTrackstersEM)
```

Few comments and explanations:

   * lines 1-7 will load all the modules that are needed in order to define a
     single iterations. The modules can be imported either from *physical files*
     or using the automatically produced configuration files (by the
     `fillDescriptions(...)` method. In the latter case, the configuration files
     will **not appear** in your local folder but are fetched centrally from an
     upstream directory

   * lines 11-17 will define the input `FilteredLayerClustersProducer`,
   overriding the default parameters.

   * lines 21-35 will defined the `TrackstersProducer` and properly configure
   the CA Pattern Recognition Algorithm.

   * line 23 will, in this case, define also the `original_mask`, which is the
     mask produced by the previous iteration. In this way we can chain
     iterations and propagate the masks accordingly.

   * lines 25-26 will customize the pattern recognition so that the `Tracksters`
   *will be masked* iff the combined probability of the `Trackster` of being
   identified as $e^\pm$ or $\gamma$ is above a certain `pid_threshold`. A
   `pid_threshold` of $0.0$ will filter **all objects**

   * lines 39-41 will produce the legacy format `MultiClusters` for each
   reconstructed `Trackster`

   * lines 43-46 will defined the body of this TICL-iteration:

      * seeding
      * cluster filtering
      * pattern recognition

##### Track-Based TICL Iteration

Let's have a look at how a possible Track-based TICL iteration can be defined in
`CMSSW`.

```python
import FWCore.ParameterSet.Config as cms

from RecoHGCal.TICL.TICLSeedingRegions_cff import ticlSeedingTrk
from RecoHGCal.TICL.ticlLayerTileProducer_cfi import ticlLayerTileProducer as _ticlLayerTileProducer
from RecoHGCal.TICL.trackstersProducer_cfi import trackstersProducer as _trackstersProducer
from RecoHGCal.TICL.filteredLayerClustersProducer_cfi import filteredLayerClustersProducer as _filteredLayerClustersProducer
from RecoHGCal.TICL.multiClustersFromTrackstersProducer_cfi import multiClustersFromTrackstersProducer as _multiClustersFromTrackstersProducer

# CLUSTER FILTERING/MASKING

filteredLayerClustersTrk = _filteredLayerClustersProducer.clone(
  clusterFilter = "ClusterFilterByAlgo",
  algo_number = 8,
  iteration_label = "Trk"
)

# CA - PATTERN RECOGNITION

trackstersTrk = _trackstersProducer.clone(
  filtered_mask = cms.InputTag("filteredLayerClustersTrk", "Trk"),
  seeding_regions = "ticlSeedingTrk",
  filter_on_categories = [2, 4], # filter muons and charged hadrons
  pid_threshold = 0.0,
  missing_layers = 5,
  min_clusters_per_ntuplet = 10,
  min_cos_theta = 0.866, # ~30 degrees
  min_cos_pointing = 0.798, # ~ 37 degrees
  max_delta_time = -1.,
  algo_verbosity = 0
)

# MULTICLUSTERS

multiClustersFromTrackstersTrk = _multiClustersFromTrackstersProducer.clone(
    label = "TrkMultiClustersFromTracksterByCA",
    Tracksters = "trackstersTrk"
)

TrkStepTask = cms.Task(ticlSeedingTrk
    ,filteredLayerClustersTrk
    ,trackstersTrk
    ,multiClustersFromTrackstersTrk)
```

#### Putting it altogether

Now that we saw *where* all the components of TICL live, *how* to properly
configure them, we are ready to *put together a very basic version of a
reconstruction workflow*.

```python
import FWCore.ParameterSet.Config as cms

from RecoHGCal.TICL.MIPStep_cff import *
from RecoHGCal.TICL.TrkStep_cff import *
from RecoHGCal.TICL.EMStep_cff import *
from RecoHGCal.TICL.HADStep_cff import *
from RecoHGCal.TICL.ticlLayerTileProducer_cfi import ticlLayerTileProducer
from RecoHGCal.TICL.ticlCandidateFromTrackstersProducer_cfi import ticlCandidateFromTrackstersProducer as _ticlCandidateFromTrackstersProducer
from RecoHGCal.TICL.pfTICLProducer_cfi import pfTICLProducer as _pfTICLProducer
from RecoHGCal.TICL.trackstersMergeProducer_cfi import trackstersMergeProducer as _trackstersMergeProducer
from RecoHGCal.TICL.multiClustersFromTrackstersProducer_cfi import multiClustersFromTrackstersProducer as _multiClustersFromTrackstersProducer


ticlLayerTileTask = cms.Task(ticlLayerTileProducer)

ticlTrackstersMerge = _trackstersMergeProducer.clone()
ticlMultiClustersFromTrackstersMerge = _multiClustersFromTrackstersProducer.clone(
    Tracksters = "ticlTrackstersMerge"
)
ticlTracksterMergeTask = cms.Task(ticlTrackstersMerge, ticlMultiClustersFromTrackstersMerge)

ticlCandidateFromTracksters = _ticlCandidateFromTrackstersProducer.clone(
      tracksterCollections = ["ticlTrackstersMerge"],
      # A possible alternative for momentum computation:
      # momentumPlugin = dict(plugin="TracksterP4FromTrackAndPCA")
    )
pfTICL = _pfTICLProducer.clone()
ticlPFTask = cms.Task(ticlCandidateFromTracksters, pfTICL)

iterTICLTask = cms.Task(ticlLayerTileTask
    ,ticlMIPStepTask
    ,ticlTrkStepTask
    ,ticlEMStepTask
    ,ticlHADStepTask
    ,ticlTracksterMergeTask
    ,ticlPFTask
    )

def injectTICLintoPF(process):
    if getattr(process,'particleFlowTmp', None):
      process.particleFlowTmp.src = ['particleFlowTmpBarrel', 'pfTICL']

    return process
```

This defines 4 different iterations, each of which is contained inside a
`cms.Task`:

   * `MIPStepTask`, defined in the file `RecoHGCal/TICL/python/MIPStep_cff.py`
   * `TrkStepTask`, defined in the file `RecoHGCal/TICL/python/TrkStep_cff.py`
   * `EMStepTask`,  defined in the file `RecoHGCal/TICL/python/EMStep_cff.py`
   * `HADStepTask`, defined in the file `RecoHGCal/TICL/python/HADStep_cff.py`

#### Test it

In order to test the reconstruction and see its output, we can use one of the
several workflows that have been pushed into the release that are tailored to
HGCAL reconstruction.

The official tool to test the validity of the reconstruction in CMSSW is the
python script `runTheMatrix.py`. If you to get familiar with such a useful and
wonderful tool, your life will become easier and your development cycle much
faster. The usual way to get used with a new tool is to read its manual:

```shell
runTheMatrix.py --help
```

This will explain the main options available and their meaning.

We will use the workflow number 23293 from the `upgrade` workflows:

```shell
runTheMatrix.py -w upgrade -l 23293.0
```

This workflow will use the `CloseByParticleGun` that is documented
[here](http://hgcal.web.cern.ch/hgcal/Generation/CloseByParticleGun/) .It
generates a couple of $\gamma$s at the front-face of the HGCAL detector
($Z_{origin} \in [320.99,321.01] \textrm{cm}$, $R_{origin} \in [89.99, 90.01]
\textrm{cm}$). The $\gamma$s will have random energy in the range
$E\in[25.,200]$ GeV. Their direction will point towards the nominal CMS Center
and their (arc) separation will be of $10$ cm.

The previous command will create the directory
`23293.0_CloseByParticle_Photon_ERZRanges_2026D49_GenSimHLBeamSpotFull+DigiFullTrigger_2026D49+RecoFullGlobal_2026D49+HARVESTFullGlobal_2026D49`
and all the configuration files needed to run the job within. The python
configuration files that will be created and executed are basically 4:

   * `CloseByParticle_Photon_ERZRanges_cfi_GEN_SIM.py` will generate the two $\gamma$s

   * `step2_DIGI_L1_L1TrackTrigger_DIGI2RAW_HLT.py` will create the
     `CaloParticles` and, more in general, run the so called `DIGI2RAW` step

   * `step3_RAW2DIGI_L1Reco_RECO_RECOSIM_PAT_VALIDATION_DQM.py` will run the
     opposite step, i.e. `RAW2DIGI`, plus the digitization and the actual
     reconstruction.

   * If you want to inspect how the TICL iterative reconstruction has been
     configured for real, you can interactively open the previous file with the
     command: `ipython -i
     step3_RAW2DIGI_L1Reco_RECO_RECOSIM_PAT_VALIDATION_DQM.py` and, at the
     `ipython` prompt, digit the command `process.iterTICLTask`. You should see
     the list of all the modules that are needed by the current version of the
     TICL iterative reconstruction.

When the job has finished, in the very same directory there is a file named
`step3.root`. This file will contain the output of the reconstruction step and
will include the objects produced by TICL.

A nice trick to explore the content of an `EDM File` (i.e. of a ROOT file
written out by a `CMSSW` job) is to use the command `edmDumpEventContent`. Also
in this case, `edmDumpEventContent -h` will print on the screen a very brief
summary of the options it accepts. For our purposes we can just run it, without
any particular options:

```shell
edmDumpEventContent step3.root | \
  grep -i -E '(tracksters|multiClustersFromTracksters|ticlCandidateFromTracksters|pfTICL)' | \
  sort
```

The output of the previous command should look like:

```
vector<TICLCandidate>                 "ticlCandidateFromTracksters"   ""                "RECO"
vector<float>                         "ticlTrackstersEM"          ""                "RECO"
vector<float>                         "ticlTrackstersHAD"         ""                "RECO"
vector<float>                         "ticlTrackstersMIP"         ""                "RECO"
vector<float>                         "ticlTrackstersTrk"         ""                "RECO"
vector<reco::HGCalMultiCluster>       "ticlMultiClustersFromTrackstersEM"   ""                "RECO"
vector<reco::HGCalMultiCluster>       "ticlMultiClustersFromTrackstersHAD"   ""                "RECO"
vector<reco::HGCalMultiCluster>       "ticlMultiClustersFromTrackstersMIP"   ""                "RECO"
vector<reco::HGCalMultiCluster>       "ticlMultiClustersFromTrackstersMerge"   ""                "RECO"
vector<reco::HGCalMultiCluster>       "ticlMultiClustersFromTrackstersTrk"   ""                "RECO"
vector<reco::PFCandidate>             "pfTICL"                    ""                "RECO"
vector<ticl::Trackster>               "ticlTrackstersEM"          ""                "RECO"
vector<ticl::Trackster>               "ticlTrackstersHAD"         ""                "RECO"
vector<ticl::Trackster>               "ticlTrackstersMIP"         ""                "RECO"
vector<ticl::Trackster>               "ticlTrackstersMerge"       ""                "RECO"
vector<ticl::Trackster>               "ticlTrackstersTrk"         ""                "RECO"
```

A few comments:

   * lines 12-16 contain the `Tracksters` produced by the 4 iterations that have
     been configured.

   * lines 6-10 are their translation in legacy format that can be understood by `cmsShow`

   * lines 2-5 represent the masks produced by the 4 iterations

   * line 1 refers to the collection of `TICLCandidate` that is derived **from
     all the `Tracksters` at the same time**

   * line 11 refers to the `PFCandidate` that are derived directly from the `TICLCandidate`

   * Other products involved, like, e.g., the `TICLSeedingRegion` or the
     `TICLLayerTiles` are temporary and will not be persisted on disk.

##### Visualize the output

We will not review in details the steps needed to visualize *objects within
HGCAL in Fireworks*, since they have been documented in [another
section](http://hgcal.web.cern.ch/hgcal/Visualization/visualization/). In our
specific case, we will just outline few of the steps below.

   * The first thing you need is a proper geometry file description that
     `cmsShow` can use to correctly render objects within the HGCAL detector.
     The procedure to do so is documented
     [here](http://hgcal.web.cern.ch/hgcal/Visualization/visualization/#generating-input-data),
     but we report below the command you can run directly inside your working
     area:

```shell
cd $LOCALRT/src/ && cmsRun Fireworks/Geometry/python/dumpRecoGeometry_cfg.py tag=2026 version=D49
```

   * The next step is to launch the `cmsShow` using the geometry file created
     with the previous command and the output file of the workflow `23293`:

```shell
cd $LOCALRT/src/23293.0_CloseByParticleGun+CloseByParticle_Photon_ERZRanges_2026D49_GenSimHLBeamSpotFull+DigiFullTrigger_2026D49+RecoFullGlobal_2026D49+HARVESTFullGlobal_2026D49  && cmsShow -g ../cmsRecoGeom-2026.root --no-version-check -i step3.root
```

   * This will load `cmsShow` using the default configuration that does not
     include any TICL output products. The easiest way for you to visually
     inspect what has been reconstruction by the electro-magnetic iteration is
     to use the feature `Add Collection`. Once the `Add Collection` pop-up window
     appears, you can digit in the search box the string `trackstersEM`. You
     will be presented with several choices, each of which will display the
     trackster in a different `View` in `cmsShow`. You can select the one whose
     purpose is `HGCAL MultiCluster`.

   * A typical picture that should be displayed would look like the following:

![CloseByPhotons](Tutorial_TICL_inReco.png)

**Update on:
Mon 20 Apr 2020 07:18:40 PM CEST
**
