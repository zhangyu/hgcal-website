# CLUE Algorithm

*CLUE (CLUstering of Energy) Algorithm* is a highly parallelizable clustering
([link](https://arxiv.org/abs/2001.09761)), developed based on *Imaging Algorithm*
and derived from a published paper
([link](http://science.sciencemag.org/content/344/6191/1492.full.pdf)).

Comparing with Imaging Algorithm, *CLUE Algorithm* has the following changes:

* use tiles as data struction (instead of kdtree in Imaging Algorithm)
* remove core/halo 
* calculate nearest highter within local searchbox
* remove density sorting and delta sorting
* assign cluster id based on list of followers
* cut on delta for determining outliers


## Algorithm's Logic

The main logic of the algorithm can be split into three logical sections, that
are illustrated below via some pseudo-code.

![General algorithm logic](HGCALCLUEAlgorithm.png)

### Calculate the local density
![Calculate the local density](HGCALCLUEAlgorithm_CalculateDensity.png)
### Calculate the closest distance to a cell with higher density
![Calculate the closest distance to a cell with higher density](HGCALCLUEAlgorithm_CalculateDistanceToHigher.png)
### Find Seeds and make clusters
![Find Seeds and make clusters](HGCALCLUEAlgorithm_FindAndAssignClusters.png)