# HGCAL Supported Proxies
The following proxies/changes are guaranteed to with CMSSW_10_6_0_pre1 and above.

## Fireworks
In order to successfully visualize an event in Fireworks both a **geometry** and an **event** file have to be generated.

## Generating Input Data
1. **Geometry**: there are two kinds of geometries that can be generated and used with Fireworks.
     * **Reconstruction Geometry** [Fireworks/Geometry/python/dumpRecoGeometry_cfg.py](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Geometry/python/dumpRecoGeometry_cfg.py)  
   __example usage__  
   ``cmsRun Fireworks/Geometry/python/dumpRecoGeometry_cfg.py tag=2023 version=D28``  
   the flag for loading the output of the script in FW is __--geom-file__

     * **Simulation Geometry** [Fireworks/Geometry/python/dumpSimGeometry_cfg.py](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Geometry/python/dumpSimGeometry_cfg.py)  
   __example usage__  
   ``cmsRun Fireworks/Geometry/python/dumpSimGeometry_cfg.py tag=2023 version=D28``  
   the flag for loading the output of the script in FW is **--sim-geom-file**

2. __Event__: [Configuration/PyReleaseValidation/scripts/runTheMatrix.py](https://github.com/cms-sw/cmssw/blob/master/Configuration/PyReleaseValidation/scripts/runTheMatrix.py)  
   __example usage__  
   ``runTheMatrix.py -w upgrade -l 24016.0``  
   to load the generated root file in FW you just have to pass its relative path as an argument.

!!! example
```bash
cmsRun Fireworks/Geometry/python/dumpRecoGeometry_cfg.py tag=2023 version=D28 &&  
runTheMatrix.py -w upgrade -l 24016.0 &&  
cmsShow --geom-file cmsRecoGeom-2023.root 24016.0_SingleGammaPt35Extended+DoubleGammaPt35Extended_pythia8_2023D28_GenSimHLBeamSpotFull+DigiFullTrigger_2023D28+RecoFullGlobal_2023D28+HARVESTFullGlobal_2023D28/step3.root
```

### Geometry
Some handlers have been added for loading/unloading the geomery of HGCAL under the 'Detector' tab of each **3D View**.
You can easily find the 'Detector' tab by clicking on the (i)nfo icon at the top right part of the view.

![Selector](GeomSelector.png)

### Collections
To load a collection into Fireworks you have to click on the "Add Collection" button and type in the the search box either the **module label**(ex. hgcalLayerCluster, CaloParticle) **or** the **class type**(ex. CaloCluster) of the proxy that you want to load.

#### Configuring
When the colletion has been loaded, you can modify some of its properties by selecting it and clicking on the (i)info icon.

Under the '_Graphics_" tab there are various options including,
![selector](Selector0.png)

- Color
- Layer selector
   if the input value is equal to 0 it renders the **whole** collection, otherwise it only renders the elements that are located at the same layer as the input value
- Heatmap
   toggle on/off the heatmap
- EnergyCutOff
- Z+/Z-
   endcap visibility flag

___
## Technical Details

### Summary
Modified Proxies:
- FWCaloClusterProxyBuilder (__+heatmap__)
- FWHGTowerProxyBuilder
- FWHGCalMultiClusterProxyBuilder (__+heatmap__)
- FWCaloParticleProxyBuilder (__+heatmap__)

New Proxies:
+ FWHGCalMultiClusterLegoProxyBuilder
+ FWHeatmapProxyBuilderTemplate

___

## Proxies

### FWHeatmapProxyBuilderTemplate
[Fireworks/Core/interface/FWHeatmapProxyBuilderTemplate.h](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Calo/interface/FWHeatmapProxyBuilderTemplate.h)

**Description**
This template adds 5 parameters under the "graphics" tab of each collection that inherits from this class.
1. Layer
2. Energy CutOff
3. Heatmap
4. Z+ (Right Endcap)
5. Z- (Left Endcap)

*Heatmap Gradient*
```c++
static constexpr uint8_t gradient_steps = 9;
static constexpr gradient[3][gradient_steps] = {
   {0.2082*255, 0.0592*255, 0.0780*255, 0.0232*255, 0.1802*255, 0.5301*255, 0.8186*255, 0.9956*255, 0.9764*255},
   {0.1664*255, 0.3599*255, 0.5041*255, 0.6419*255, 0.7178*255, 0.7492*255, 0.7328*255, 0.7862*255, 0.9832*255},
   {0.5293*255, 0.8684*255, 0.8385*255, 0.7914*255, 0.6425*255, 0.4662*255, 0.3499*255, 0.1968*255, 0.0539*255}
};
```
___

### FWCaloParticleProxyBuilder
[Fireworks/SimData/plugins/FWCaloParticleProxyBuilder.cc](https://github.com/cms-sw/cmssw/blob/master/Fireworks/SimData/plugins/FWCaloParticleProxyBuilder.cc)

**C++ Class**
``vector<CaloParticle>``

**Module Label**
CaloParticle

**Description**
This plugin renders all the RecHits within HGCAL, in **RhoZ** and **3D** view.

_The plugin is registered as_:
REGISTER_FWPROXYBUILDER(FWCaloParticleProxyBuilder, CaloParticle, "CaloParticle", FWViewType::kAll3DBits | FWViewType::kAllRPZBits);

![CaloParticle](CaloParticle.png)

___

###  FWCaloClusterProxyBuilder
[Fireworks/Calo/plugins/FWCaloClusterProxyBuilder.cc](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Calo/plugins/FWCaloClusterProxyBuilder.cc)

**C++ Class**
``vector<CaloCluster>``
This also works for other objects that inherit from a CaloCluster. These are:
- vector< SuperCluster >
- vector< HGCalMutliCluster >
- vector< PFCluster >

**Module Label**
hgcalLayerClusters, particleFlowSuperClusterHGCalFromMultiCl, particleFlowSuperClusterHGCal

**Description**
This plugin renders the RecHits of each single **2D Layer Cluster** in the **3D RecHit** view

A cross is being drawn over the **seed** of each HGCalLayerCluster, the size of the cross is proportional to the enegy of the cluster.
An additional option for computing the heatmap has been added, the first(0) option computes the heatmap based on the energy of the LayerCluster and the second(1) based on the energy of individual rechits.

Additionaly, two new input fields have been added under the 'Graphics' tab for specifying a time range of HGCalLayerClusters that the user wants to display.

![HGCalLayerClusterSelector](HGCalLayerClusterSelector2.png)

_The plugin is registered as_:
REGISTER_FWPROXYBUILDER(FWCaloClusterProxyBuilder, reco::CaloCluster, "Calo Cluster", FWViewType::kISpyBit);

![heatmap](CaloClusterHeatmap.png)

___

### FWHGCalMultiClusterProxyBuilder
[Fireworks/Calo/plugins/FWHGCalMultiClusterProxyBuilder.cc](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Calo/plugins/FWHGCalMultiClusterProxyBuilder.cc)

**C++ Class**
``vector<HGCalMultiCluster>``

**Module Label**
hgcalMultiClusters, TrackstersToMultiCluster{,MIP}

**Description**
This plugin will render the RecHits of each single MultiCluster in the 3D RecHit view.

_The plugin is registered as_:
REGISTER_FWPROXYBUILDER( FWHGCalMultiClusterProxyBuilder, reco::HGCalMultiCluster, "HGCal MultiCluster", FWViewType::kISpyBit );

![HGCal_MultiCluster](HGCal_MultiCluster.png)

___

###  FWHGTowerProxyBuilder
[Fireworks/Calo/plugins/FWHGTowerProxyBuilder.cc](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Calo/plugins/FWHGTowerProxyBuilder.cc)

**C++ Class**
``edm::SortedCollection<HGCRecHit, …>``

**Module Label**
HGCalLego

**Description**
This plugin renders the RecHits in the HF View.

**Changes**
Fixed the minmax eta/phi calculation, for both Cells(Hexagons) and Scintilators(Boxes)

_The plugin is registered as_:
REGISTER_FWPROXYBUILDER(FWHGTowerProxyBuilderBase, HGCRecHitCollection, "HGCalLego", FWViewType::kLegoHFBit);

![HF_View](HGCalLegoHF.png)

___

### FWHGCalMultiClusterLegoProxyBuilder
[Fireworks/Calo/plugins/FWHGCalMultiClusterLegoProxyBuilder.cc](https://github.com/cms-sw/cmssw/blob/master/Fireworks/Calo/plugins/FWHGCalMultiClusterLegoProxyBuilder.cc)

**C++ Class**
`vector<reco::HGCalMultiCluster>`

**Module label**
HGCMultiClusterLego

**Description**
Renders a tower for each HGCAL MultiCluster in LEGO | RPZ | 3D views with cross window selection and highlighting. The height of the tower corresponds to the pt of each cluster.

_The plugin is registered as_:
REGISTER_FWPROXYBUILDER(FWHGCalMultiClusterLegoProxyBuilder, `std::vector<reco::HGCalMultiCluster>`, "HGCMultiClusterLego", FWViewType::k3DBit|FWViewType::kAllRPZBits|FWViewType::kAllLegoBits);


![HGCalLego](HGCalLego.png)
![HGCalLego2](HGCalLego2.png)

