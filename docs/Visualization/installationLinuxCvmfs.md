# Installing cmsShow/Fireworks on Linux using /cvmfs

## Introduction
A quick way to have the latest and greatest `cmsShow`/`Fireworks` release
available on your Linux machine is to install `cvmfs` on it. Through the
`/cvmfs` file system you'll have access to all `cmssw` releases, including
nightly builds. There are also pre-configured `singularity` containers that
could be used to setup a proper `cmssw` environment in which you can run
`cmsShow`. Since all usual commands will be available, you could also develop
new proxies or fix bugs in the existing ones, all comfortably from your machine.

## Installing `/cvmfs`

The main documentation site with the **updated installation instructions** is
available [here](https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html).

!!! info
    You can find here below the instructions that were tested on `Ubuntu 21.04` on
    `22/09/2021`.


```bash
wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb

sudo dpkg -i cvmfs-release-latest_all.deb
sudo apt update
sudo apt install cvmfs
sudo cvmfs_config setup
systemctl restart autofs
```

Now edit the file `/etc/cvmfs/default.local` (using `sudo`). The content of the
files should be like the following:

```bash
cat /etc/cvmfs/default.local

CVMFS_REPOSITORIES=cms-ib.cern.ch,cms.cern.ch,grid.cern.ch,sft.cern.ch
CVMFS_CLIENT_PROFILE=single
# Proxy settings from users inside the cern.ch network (see
# https://twiki.cern.ch/twiki/bin/view/CvmFS/ClientSetupCERN ),
# with fallback to a direct connection and internal caching
CVMFS_HTTP_PROXY="DIRECT"

# Limit the CVMFS cache to 20 GB (a full CMSSW installation occupies about 16 GB)
CVMFS_QUOTA_LIMIT=20000
```

Test if your installation was successful:

```bash
cvmfs_config probe

>> Probing /cvmfs/cms-ib.cern.ch... OK
>> Probing /cvmfs/cms.cern.ch... OK
>> Probing /cvmfs/grid.cern.ch... OK
>> Probing /cvmfs/sft.cern.ch... OK
```

If everything is fine, you should have access locally on the `/cvmfs` folder
containing all **CMS** software.

## Installing `singularity`

After having installed, configured and tested `cvmfs` locally, proceed to
install singularity and its needed component, `go`.

### Installing `go`

We preferred to install `go` centrally using `apt` rather than manually via
`tar.gz`. This has the advantage that you do not have to manually set any
additional env variables. Either choices are fine, provided they are correctly
executed and configured.

```bash
sudo apt install golang-go
```

### Installing `singularity`

The installation instructions for singularity are available at [this
link](https://sylabs.io/guides/3.8/admin-guide/installation.html).

!!! danger
    You are warmly invited to read the installation instructions linked above
    **before** proceeding.

!!! info
    You can find here below the instructions that were tested on `Ubuntu 21.04` on
    `23/09/2021`.

```bash
sudo apt-get update && sudo apt-get install -y \
    build-essential \
    uuid-dev \
    libgpgme-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup-bin

mktemp -d
cd /tmp/tmp.sWhCrdWYYP # <-- TO BE MODIFIED!!!
export VERSION=3.8.0 && \
    wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \
    tar -xzf singularity-ce-${VERSION}.tar.gz && \
    cd singularity-ce-${VERSION}
./mconfig && \
    make -C ./builddir && \
    sudo make -C ./builddir install
```

## Launching `cmsShow`

In order to use `cmsShow` locally, you need to source the `CMS` environments and
enter the singularity container to access the `cmssw` releases.

```bash
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmssw-cc7
```

You can pass additional options to singularity by adding them directly to
`cmssw-cc7` script, e.g.:

```bash
cmssw-cc7 --bind /home/local_cmssw/:/home/local_cmssw --bind /home/ROOT_files/:/ROOT_files
```

Singularity, by default, will mount your `${HOME}` directory and `${PWD}`
automatically for you. In addition to that, `/cvmfs` will be automatically
mounted and available within the container.

Other variants exist of the same command, namely `cmssw-cc6`
and `cmssw-cc8`. Use the one related to the `OS` you are interested in. 

Once inside the singularity container, source the usual `CMS` startup script:

```bash
source /cvmfs/cms.cern.ch/cmsset_default.sh
```

Now you can start `cmsShow` as you would do on `lxplus`:

```bash
cmsrel CMSSW_X_Y_Z
cd CMSSW_X_Y_Z/src
cmsenv
cmsShow -g .....
```


