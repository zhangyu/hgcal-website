# SimPFProducer

The `SimPFProducer` (code available at [this
link](https://github.com/cms-sw/cmssw/blob/master/RecoParticleFlow/PFSimProducer/plugins/SimPFProducer.cc))
is an `edm::global::EDProducer<>` in `CMSSW` software. Its purpose is to *fill
the gap* between the HGCAL reconstruction and the `ParticleFlow` world. In
particular, it will inject a Monte Carlo driven reconstruction of HGCAL objects
into `ParticleFlow`, with some sort of realism that has been ad-hoc added
in order to make it a little more ...  *realistic*. The main logic and algorithm
will be described in the later sections.

## Algorithm's logic

The main logic of the algorithm can be split into three logical sections, that
are illustrated below via some pseudo-code.

### Loop over CaloParticles

It is worth noting that by default, as of Tue 09 Jul 2019, the `CaloParticles`
are produced both for the _hard scatterer_ event **and** for the _in-time
pile-up_. It follows that the description given by this Monte Carlo driven
approach takes both inputs into account (in the best possible way).
The next loop over `CaloParticles` is meant to perfectly describe 
_electrons_, _photons_ and _neutral hadrons_.

The cases of _unconverted $\gamma$s_ and _non-brem electrons/positrons_ are
easy. A _converted $\gamma$_ is added as a Block with 2 BlockElements. A brem
electron is added as a single Block with the correct number of BlockElements,
depending on the number of $\gamma$s emitted.

![Loop Over Calo Particles](simPFProducerAlgo_ParseCaloParticles.png)

### Loop over Reconstructed Tracks

There are two track collections consumed: **generalTracks** and
**hgcalTrackCollection:TracksInHGCal**. The former is the outcome of the regular
_offline tracking reconstruction_, while the latter is produced by
`HGCalTrackCollectionProducer` (code at [this
link](https://github.com/cms-sw/cmssw/blob/master/RecoParticleFlow/PFTracking/plugins/HGCalTrackCollectionProducer.cc))
The configuration of the `HGCalTrackCollectionProducer` is available at [this
link](https://github.com/cms-sw/cmssw/blob/master/RecoParticleFlow/PFTracking/python/hgcalTrackCollection_cfi.py).

!!! danger
    We need to review the selection made by ``HGCalTrackCollectionProducer (code available at [this
    link](https://github.com/cms-sw/cmssw/blob/master/RecoParticleFlow/PFTracking/plugins/HGCalTrackCollectionProducer.cc#L102-L125)),
    since it likely became out of sync after all the changes we made to the
    HGCAL Geometry.

!!! warning
    The following loop will go over **generalTracks**, but will only consider them
    **if they belong also to the hgcalTrackCollection:TracksInHGCal collection**.


!!! warning
    The tracks that are part of both collections but that **have no link with the
    Monte Carlo truth information (i.e. fakes) are silently ignored and
    skipped**.



![Loop Over Reconstructed Tracks](simPFProducerAlgo_LoopOverTracks.png)


### Loop over blocks not yet assigned
![Loop Over Block not yet Assigned](simPFProducerAlgo_LoopOverBlocks.png)


