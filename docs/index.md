<!-- # Welcome to MkDocs

For full documentation visit [mkdocs.org](https://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files. -->

# Welcome to the HGCAL website

This website contains all information about CMS HGCAL reconstruction software.

## Short introduction to HGCAL

The High-Granularity Calorimeter (HGCAL) is an imaging calorimeter with both
lateral and longitudinal fine granularity.  HGCAL will be installed in the
forward region of the CMS experiment to face the HL-LHC challenges, which will
integrate ten times more luminosity than the LHC.

The proposed design uses silicon sensors as active material in the front
sections and plastic scintillator tiles, with the scintillation light read out
by SiPMs, towards the rear.

In addition to the very precise 3D-positions, HGCAL provides also precise
information on the timing of the energy deposits to discriminate clusters from
pileup within a single bunch crossing.  The five dimensional information
(energy, x, y, z, and time) is ideally suited for particle-flow reconstruction. 

HGCAL (CE) is divided in two sections:

**Electromagnetic (CEE)**: constructed of cassettes each containing two layers
of sensors, and two layers of absorber; the absorbers are lead, and
copper/tugsten

**Hadronic (CEH)**: constructed of stainless steel absorber plates, with both
silicon sensors and scintillator tiles read out silicon photomultipliers.

The CEH is divided into two sections, the first having a finer sampling (i.e.
thinner absorber plates), and the second having a coarser sampling.

The sensitive cells in the first few layers of CEH-fine are exclusively silicon
sensors, while all the remaining layers have a mix of silicon sensors at smaller
radii, and scintillator tiles in the outer regions exposed to less dose and less
neutron fluence

Current versions of the GEANT geometry, v11 - v15,
[(link)](https://github.com/cms-sw/cmssw/blob/master/Configuration/Geometry/README.md)
model the geometry baseline defined in December 2018 which had 14 cassettes (28
layers) in CEE, and 22 layers in CEH.  The recent (March 2021) longitudinal
reoptimization will reduce the CEE by 1 cassette. The lead absorber in the last
5 cassettes will have additional thickness such that the overall CEE thickness
will increase from 25.4 to 27.7 X0. At the same time the number of all-silicon
CEH-fine layers will be reduced from 8 to 7. The resulting CEH then has 10 fine
and 11 coarse sampling layers. The total calorimeter depth remains about 10λ.

