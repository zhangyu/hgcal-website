# Still not tired to read about HGCAL reconstruction?

Articles:

* *The Phase-2 Upgrade of the CMS Endcap Calorimeter*, 
  [cds](https://cds.cern.ch/record/2293646?ln=en)
* *CLUE: A Fast Parallel Clustering Algorithm for High Granularity Calorimeters in High Energy Physics*, 
  [arXiv](https://arxiv.org/abs/2001.09761)

Talks (most recent at the top):

* *Clustering and Reconstruction in HGCAL*, M. Rovere,
  [Slides at FCC Software Meeting](https://indico.cern.ch/event/949440/contributions/3988881/attachments/2092853/3516903/20200828_FCC_TICL_MR.pdf)
* *A novel reconstruction framework for an imaging calorimeter for HL-LHC*, L. Gouskos,
  [CINCO](https://cms-mgt-conferences.web.cern.ch/conferences/pres_display.aspx?cid=2793&pid=21167),
  [Conference website](https://indico.cern.ch/event/831165/contributions/3717130/) 
* *Reconstruction in an imaging calorimeter for HL-LHC*, 
  [CINCO](https://cms-mgt-conferences.web.cern.ch/conferences/pres_display.aspx?cid=2708&pid=20822), 
  [Conference website](https://indico.cern.ch/event/818783/contributions/3598473/), 
  [Proceeding](https://iopscience.iop.org/article/10.1088/1748-0221/15/06/C06023)
* *New ideas on jets reconstruction in high granularity calorimeters*, F. Pantaleo,
  [CINCO](https://cms-mgt-conferences.web.cern.ch/conferences/pres_display.aspx?cid=2580&pid=19165),
  [Conference website](https://indico.cern.ch/event/753671/timetable/)

If something is missing you can also check out the [Conference Committee and Editorial Board twiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HGCAL_CCandEB).
