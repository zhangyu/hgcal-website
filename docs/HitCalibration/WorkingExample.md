Here below the $dE/dx$ weights calculation for the second step 
and the regional em factors for the third step described in this
[section][1].

[1]: hitCalibration.md 

## dEdx weights

The dEdx weights calculation is performed by the [dEdxWeights.ipynb](https://github.com/cms-sw/cmssw/blob/master/SimTracker/TrackerMaterialAnalysis/test/dEdxWeights.ipynb) script. It needs 
an input file that the [SimTracker/TrackerMaterialAnalysis](https://github.com/cms-sw/cmssw/tree/master/SimTracker/TrackerMaterialAnalysis) package provides. However, we should 
mention that in this [line](https://github.com/cms-sw/cmssw/blob/master/SimTracker/TrackerMaterialAnalysis/plugins/TrackingMaterialProducer.cc#L155-L158) the Z low boundary of the first Stainless Steel 
volume is hardcoded, since in the txt file we only save the upper Z boundary of each volume. The  
[Geometry/HGCalCommonData/test/testHGCalParameters_cfg.py](https://github.com/cms-sw/cmssw/blob/master/Geometry/HGCalCommonData/test/python/testHGCalParameters_cfg.py) script was used for that. 

Regarding the [dEdxWeights.ipynb](https://github.com/cms-sw/cmssw/blob/master/SimTracker/TrackerMaterialAnalysis/test/dEdxWeights.ipynb) script and starting backwards, you will see at Out[29] cell the dedxweights 
alongside with dedxweights calculated when we used what Geant provides for dEdx. However, 
we decided to go with the PDG dedx values since Geant expert advice us not to trust those. 
So, you only care on the "dedxweights" column there and we removed the other values. 

When the calculation is finished and we have at hand the calculated dEdx weights, we should create a Pull Request and put them in the official CMSSW repository in the [RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py) file using the correct era modifier. 

The recipe follows: 

```
#Take the latest integration build. 
cmsrel CMSSW_...
cd CMSSW_.../src/
cmsenv
git cms-addpkg SimTracker/TrackerMaterialAnalysis
(cd $LOCALRT && scram b disable-biglib)
#It needs a second cmsenv
cmsenv
scram b -j 8
cd SimTracker/TrackerMaterialAnalysis/test
#The command below will create the txt file. 
#But first open the 
#SimTracker/TrackerMaterialAnalysis/test/trackingMaterialProducer10GeVNeutrino_ForHGCalPhaseII.py
#file and put the geometry you want to study at line 14. 
#Also, always check that hgcal z front is correctly set
#as explained above. 
cmsRun trackingMaterialProducer10GeVNeutrino_ForHGCalPhaseII.py
#Open the dEdxWeights.ipynb and run. 
#Personally, I run this locally, not in lxplus. 
#Take the weights and put them in 
#RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py
#with the correct era modifier. To find the era modifier for e.g. D46 do
runTheMatrix.py -n -w upgrade |grep D46 
#Choose one, e.g. 
runTheMatrix.py -w upgrade -l 22002.0 --dryRun
#Open the cmd file in the folder and take the era. 
#phase2_hgcalV11 in this case, since  Phase2C9 defined in 
#Configuration/Eras/python/Era_Phase2C9_cff.py contains that. 
```

<!-- 

Comment for the following section: Thickness factors are not used anymore 
since the number of MIPs is computed using the mean signal from a MIP, 
i.e. PDG value of 388 ev/μm, which uses the nominal active thickness of the silicon as input.

        -->

## Regional em factors

This calculation is a cumbersome procedure far more time consuming than the one in the previous step and it depends on that, in the sense that the dEdx weights of the geometry we are working should be in place. This step requires sending many jobs multiple times (3 data tiers times 
2, where 2 is for the normal calculation plus the performance test, times 8 for the different regions of the detector). 

So, a few comments: 

- Make sure you have the correct era and weights in [RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py)

- Since we want to calculate thickness correction factors, those factors initially should be set to 1's. Then, at the end of this exercise, together with the dEdx weights, they should be sent with a PR in the CMSSW repository. 
So, go to [RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py) and put e.g. for D46 ```
phase2_hgcalV11.toModify( HGCalRecHit , thicknessCorrection = [1.0,1.0,1.0] ) #120um, 200um, 300um```

- Another issue that we should keep in mind is the fCperMIP, that should be placed in 
[RecoLocalCalo/HGCalRecProducers/python/HGCalUncalibRecHit_cfi.py](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalUncalibRecHit_cfi.py) using the correct era modifier. Unless the group wants to revert back to the MPV, we don't need to change the values, which for now are using the mean. However, even for the same values a modifier should be introduced for the new geometry version. 

- If the previous comments are handled, then we are ready to launch a production using the `closeby` tool and shooting particles in front of regions of the detector, targetting specific sensor thickness. For this task 
we will use the [reco-prodtools](https://github.com/CMS-HGCAL/reco-prodtools) package. Once setup is finished, open the [reco\_prodtools/templates/partGun\_NTUP\_template.py](https://github.com/CMS-HGCAL/reco-prodtools/blob/master/templates/partGun_NTUP_template.py)
and put in the relevant places for V11

```
from RecoLocalCalo.HGCalRecProducers.HGCalRecHit_cfi import dEdX_weights_v11 
readGenParticles = cms.bool(True),
dEdXWeights = dEdX_weights_v11,
verbose = cms.bool(False),
```

Weights may not be used at the moment, but just to be on the safe side. 
Also, there may not be a relevant sh file to produce the python fragments so you should create one. So, find a workflow with the desired geometry and after copying an existing sh file make the adjustments.  
Finally, at the end of this subsection below, you can see the whole chain of commands for shooting in various places of the detector. 

- Now, we are ready to proceed to the final step, where we will fit the `sum(E_i*fraction)/Egen` distribution. We chose to work with the sequential approach, that is we split the procedure in 3 stages: 
   * Stage 0: Choose the histogram with the least contamination.
   * Stage 1: Rerun the calibration procedure by applying the previously defined calibration factor to the relevant hits and calculate the second least contaminated thickness factor.
   * Stage 2: Rerun the calibration but this time apply both previous calculated thickness corrections to find the last one.
     
   For this task, we will work with the [CMS-HGCAL/ntuple-tools](https://github.com/CMS-HGCAL/reco-ntuples) package. Some comments: 
   
   - We use the [HGCalImagingAlgo.py](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/HGCalImagingAlgo.py), but only the [recHitAboveThreshold](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/HGCalImagingAlgo.py#L641) function. 
   
   - The silicon thickness indexes are being defined in [HGCalImagingAlgo.py](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/HGCalImagingAlgo.py#L660-L666), where also the threshold on the noise is being calculated. This [sigmaNoise](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/HGCalImagingAlgo.py#L670) could be defined also in CMSSW, but for the moment it is there. 
  
   - The noise for each sensor/subdetector region is determined using the [RecHitCalibration](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/RecHitCalibration.py) script.
   - Open the [RecHitCalibration](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/RecHitCalibration.py) and put the dEdx weights that you calculated above. Also, replace the thickness correction factors with 1's, since we are calibrating. Check against [SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py](https://github.com/cms-sw/cmssw/blob/master/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py) that you have the correct nonAgedNoises values. For the fCPerMIP we are going with the mean for now. 

   - Finally, we have the scripts, which performs the fitting and calculate the thickness correction factors. In each Stage we will be sending a large amount of jobs in condor and although the scripts are included in the 
code [here](https://github.com/apsallid/ntuple-tools/tree/rechitcalib/hgcalRecHitCalibration), the user **should know exactly what he is doing** to avoid overwriting things. So, either make new scripts or make sure you understand what is happening. Having said that, first create the jobs with 
[createRecHitCalibrationJobs\_multiplejobs\_advanced.csh](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/hgcalRecHitCalibration/createRecHitCalibrationJobs_multiplejobs_advanced.csh), then submit the jobs with [sendRecHitCalibrationJobs.csh](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/hgcalRecHitCalibration/sendRecHitCalibrationJobs.csh), afterwords merge the output with [mergeRecHitCalibrationJobs.csh](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/hgcalRecHitCalibration/mergeRecHitCalibrationJobs.csh) and finally calculate the factors with [calcthickness.csh](https://github.com/apsallid/ntuple-tools/blob/rechitcalib/hgcalRecHitCalibration/calcthickness.csh). This would need to be done by inspecting the results for Stage 0 and making the choice with the least contamination. Then, proceed to the rest of the stages with the same idea. 

   - With the new thickness correction factors at hand, the performance checks should be performed and if the group approves the results, a new PR should be made with the new factors. With the performance checks samples you should always run "zerostage" or else extra factors will be applied which is wrong. 

The recipe follows: 

```
#Take the latest integration build. 
cmsrel CMSSW_...
cd CMSSW_.../src/
cmsenv
git cms-addpkg RecoLocalCalo/HGCalRecProducers
#OPEN 
#RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py
#RecoLocalCalo/HGCalRecProducers/python/HGCalUncalibRecHit_cfi.py
#Edit as described above 
git clone git@github.com:CMS-HGCAL/reco-ntuples.git RecoNtuples
git clone git@github.com:CMS-HGCAL/reco-prodtools.git reco_prodtools
scram b -j
cd reco_prodtools/templates/python/
#If there is no sh you want, create it. e.g.
#cp produceSkeletons_D41_NoSmear_noPU.sh produceSkeletons_D46_NoSmear_noPU.sh
#Open produceSkeletons_D46_NoSmear_noPU.sh and adjust it. 
./produceSkeletons_D46_NoSmear_noPU.sh
cd -
scram b -j 
cd reco_prodtools/
cp /afs/cern.ch/work/a/apsallid/public/HGCal/Validation/production_withdeltas.py .
#Open the production_withdeltas.py and choose 60 GeV energy only. 
#Also, choose one delta just to run the loop. No second particle will be
#generated in any case. Remove the ' --keepDQMfile ' option in the 
#RECO step, due to file limitations in eos. 
#Run each step, wait to finish and proceed to the next. 
```

Finally, the helpful commands to shoot in different regions of the detector, that **should be edited**. 

```
Close by photons
#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
# ----- GSD -----
#CE_E_Front_120um
#-----
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 320.99 --zMax 321.01 --rMin 54.99 --rMax 55.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_E_Front_120um 

#CE_E_Front_200um
#-----
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 320.99 --zMax 321.01 --rMin 89.99 --rMax 90.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_E_Front_200um 

#CE_E_Front_300um
#---------
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 320.99 --zMax 321.01 --rMin 134.99 --rMax 135.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_E_Front_300um 

#CE_H_Coarse_300um
#-------
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 429.99 --zMax 430.01 --rMin 79.99 --rMax 80.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_H_Coarse_300um 

#CE_H_Coarse_Scint
#-------
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 429.99 --zMax 430.01 --rMin 179.99 --rMax 180.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_H_Coarse_Scint 

#CE_H_Fine_120um
#-------
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 362.519 --zMax 362.521 --rMin 49.99 --rMax 50.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_H_Fine_120um 

#CE_H_Fine_200um
#--------
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 362.519 --zMax 362.521 --rMin 89.99 --rMax 90.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_H_Fine_200um 

#CE_H_Fine_300um
#--------
python production_withdeltas.py --datTier GSD --nevts 10000 --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --zMin 362.519 --zMax 362.521 --rMin 134.99 --rMax 135.01 --gunMode closeby --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --tag CE_H_Fine_300um 

#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
# ----- RECO -----
#-------
#CE_E_Front_120um
#-----
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue longlunch --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190908 --tag CE_E_Front_120um 

#CE_E_Front_200um
#-----
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue longlunch --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190908 --tag CE_E_Front_200um 

#CE_E_Front_300um
#---------
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue longlunch --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_E_Front_300um 

#CE_H_Coarse_300um
#-------
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue longlunch --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Coarse_300um 

#CE_H_Coarse_Scint
#-------
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190912 --tag CE_H_Coarse_Scint 

#CE_H_Fine_120um
#-------
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue longlunch --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Fine_120um 

#CE_H_Fine_200um
#--------
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Fine_200um 

#CE_H_Fine_300um
#--------
python production_withdeltas.py --datTier RECO --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Fine_300um 

#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
# ----- NTUP -----
#-------
#CE_E_Front_120um
#-----
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190908 --tag CE_E_Front_120um 

#CE_E_Front_200um
#-----
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190908 --tag CE_E_Front_200um 

#CE_E_Front_300um
#---------
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_E_Front_300um 

#CE_H_Coarse_300um
#-------
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Coarse_300um 

#CE_H_Coarse_Scint
#-------
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190912 --tag CE_H_Coarse_Scint 

#CE_H_Fine_120um
#-------
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Fine_120um 

#CE_H_Fine_200um
#--------
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Fine_200um 

#CE_H_Fine_300um
#--------
python production_withdeltas.py --datTier NTUP --evtsperjob 100 --queue tomorrow --partID 22 --nPart 1 --eosArea /eos/cms/store/user/apsallid/HGCal/Validation/Photons --gunMode closeby --date 20190905 --tag CE_H_Fine_300um 
#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------

```
