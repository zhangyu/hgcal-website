## Introduction

The simulated signals from GEANT4 represents the **energy** (**keV**) 
deposited in the silicon or scintillator detectors 
according to their active thickness.

These signals are then translated into 
rechits coming out the detector electronics 
with magnitude measured in **femtoCoulomb** (**fC**).
The calibration procedure will start from
those hits and produce **calibrated** rechits that can be used for clustering
purposes (hence energy measurements).

In a sampling calorimeter with varying thicknesses of absorber, in
order to measure the energy of a shower (which to good approximation
is the energy deposited in the absorber - the sensor thicknesses being
negligible), the energy deposited in the sensor
samplings must be weighted to account for the different absorber thicknesses.

It is generally agreed that for this purpose the thickness should be measured in
terms of $dE/dx$, i.e. one uses the
integrated energy loss for a MIP in a layer of absorber. This can be considered
as being due to the fact that the dominant energy absorption process in the
shower is ionization.

The steps to calibrate the rechits are summarised here and described in more detail below:

* ^^Rescaling to MIPs^^: Translate the collected signal charge (fC) into number of MIPs
* ^^Re-weighting for absorber thickness^^ :
Convert the number of MIPs into GeV using $dE/dx$ layer weights, which
represent the integrated $dE/dx$ for a MIP in the absorber material
* ^^Shower energy rescale^^: Rescale the rechit magnitudes so that their sum approximately 
represents the energy deposited by the shower

Currently, the rechits are calibrated only looking at the em energy scale.
This is not a good approximation for hadron showers, and significant energy
scale corrections will be developed soon for hadron clusters.

More information are available in [this
document](https://twiki.cern.ch/twiki/pub/CMS/HGCALSimulationAndPerformance/CalibratedRecHits.pdf).

## Detailed description

### Rescaling to MIPs

The first step consists of taking the value in fC coming out of the ADC and
convert it into a **number of MIPs**. Originally this was done in
terms of the **most probable value of the Landau distribution for a
MIP in Si**. We now use the **mean energy deposit** of a MIP, taking
the value of 388 eV/$\mu$m given in [PDG][1]
(see the figure below - it has been found that the energy deposition
of muons by GEANT reproduces this rather well).

The advantages of using the mean rather than the most probable value are:

  - the mean is proportional to silicon thickness, unlike the most
  probably value; so using the mean avoids the problem
  of the three different Si wafer thicknesses, and, in particular the
  problem that particles do not traverse the Si perpendicularly but at
  an angle which significantly increases the thickness seen at low
  values of $\eta$ (at $\eta = 1.5, 1/cos(\theta) = 1.1$)
  - shower measurements, the sum of many MIPs, are measurements of the mean

The following figure, taken from the [PDG][1] document shows the most probable energy loss,
scaled to the mean loss of a MIP in Silicon:

![Most probable energy loss, scaled to the mean loss of a MIP in Silicon](MPV_dEdx_min_Si.png)

### Re-weighting for absorber thickness

Let's denote the **number of MIPs in the i-th sensor layer**
(calculated as described in the previous section) by $n_i$.

In this step the energy lost in the i-th absorber layer
is calculated using the $dE/dx$ for a MIP in the absorber material, 
obtained by reading out the dE/dx information coming from the geometry files. 
The procedure is documented in this [section][2].

The best estimate of the number of MIPs in the i-th
**absorber** layer (i.e. in the passive layer directly in front of the
sensor layer) is the mean of $n_{i-1}$ and $n_i$, i.e. the mean of the
layer in front and the layer behind the absorber.

So the energy lost in the i-th absorber layer is:

$E_i = \frac{n_{i-1} + n_i}{2} \times \lambda_i$

where

$\lambda_i$ is the integrated $dE/dx$ of the absorber layer in front
of sensor layer $i$. It represents the energy deposit of a MIP in the
sensor layer.

The total energy lost in the shower is then:

$\sum_i^{layers} E_i = \sum_i^{layers} \frac{n_{i-1} + n_i}{2} \times
\lambda_i$

This can be rewritten as:

$\sum_i^{layers} E_i = \sum_i^{layers} \frac{\lambda_i + \lambda_{i+1}}{2}
\times n_i$

The interpretation of this formula for the last sensor layer looks
puzzling, but it is clear that if there is any signal in the last
sensor layer then there is leakage from the back of the calorimeter
which requires a correction...

### Shower energy scale

In this final step, the energy of all rechits with $E > 1 \textrm{MIP}$ is summed up 
and re-scaled so that their sum approximately
represents the energy deposited by an electromagnetic shower.

This is performed according to 'regional em factors'
created by comparing the energy of a simulated injected photon 
to the sum of the calibrated rechit signals.
The procedure is documented in detailed in this [section][2].

These factors are calculated for 7 regions: 
CE-E 120, CE-E 200, CE-E 300, CE-H 120, CE-H 200, CE-H 300, CE-H scint.
The numbers refer to the silicon sensor thickness, 
and the fine and coarse refer to the two different CE-H absorber thicknesses.

As already mentioned previously, this sets the rechits on a uniform em scale. 
Additional corrections/adjustments will be needed in particular for hadrons 
since the calorimeters e/h ratio is not equal to 1.

[1]: http://pdg.lbl.gov/2018/reviews/rpp2018-rev-passage-particles-matter.pdf
[2]: WorkingExample.md 

## Software Implementation

What has been described in the previous section has been implemented in CMSSW.
There are few steps to run in order to convert the digital signal received from
the electronics (in ADC counts) into a calibrated hit. These are documented in
the following sections.

### Uncalibrated RecHits

The ```C++``` module responsible for the creation of the un-calibrated rechits
is
[```HGCalUncalibRecHitProducer```](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalUncalibRecHitProducer.cc),
configured with the python label ```HGCalUncalibRecHit```. The main goal of this
module is to convert the input HGCAL Digis, which are measured in ADC count,
into rechits whose amplitude is expressed in terms of average number of MIPs.
The module is also configured so that it can dynamically convert ADC counts into
a real amplitide, measured in $\textrm{fC}$, which is then normalised to MIP,
depending on the thickness of the sensor. Internally, all the heavy lifting is
done by a plugin, acting as a worker. The plugin to load is specified in the
configuration itself and is dynamically loaded following the standard procedure
in cmssw. The currently (29 june 2018, 12:09:21 (UTC+0200)) configured
plugin/worker is
[```HGCalUncalibRecHitWorkerWeights```](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalUncalibRecHitWorkerWeights.cc).
Under the hood, one level deeper, the module that is responsible of making this
conversion is
[```HGCalUncalibRecHitRecWeightsAlgo<HGCHEDataFrame>```](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecAlgos/interface/HGCalUncalibRecHitRecWeightsAlgo.h)
via the ```makeRecHit``` method.

!!! note
    The takeway message is that the product produced by the
    ```HGCalUncalibRecHitProducer``` is a collection of HGCAL un-calibrated rechits
    (```edm::SortedCollection<HGCUncalibratedRecHit>```) whose ```amplitude```
    method will result in the average number of MIPs corresponding to the measured
    signal for that specific Silicon detector.

This implements the [rescale](#rescaling) section.


### Calibrated RecHits

The ```C++``` module responsible for the creation of the calibrated rechits is
[```HGCalRecHitProducer```](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalRecHitProducer.cc)
configured with the python label ```HGCalRecHit```. Also in this case, all the
heavy lifting is internally done by an external plugin, loaded using the
canonical cmssw mechanism, and configured via python. The currently
(29 juin 2018, 14:29:44 (UTC+0200)) configured plugin is
[```HGCalRecHitWorkerSimple```](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalRecHitWorkerSimple.cc).
The plugin will loop over all the input collection (EE, FH and BH uncalibrated
rechits, separately), and call the ```run``` method of its own configured
plugin/worker. The worker itself will outsource much of its duties to another
```C++``` object:
[```HGCalRecHitSimpleAlgo```](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecAlgos/interface/HGCalRecHitSimpleAlgo.h).
The main plugin will read the weights for each single layer from the
configuration file and pass them down to its internal `worker`. This will read
each hit, one by one, determine to which layer is belongs to and rescale its
```amplitude``` by the weights, eventually converting it from $\textrm{MeV}$
to $\textrm{GeV}$. This step is implementing the equation:

$E_i = \frac{\lambda_i+\lambda_{i+1}}{2}\times n_i$

where the input weights are, in fact, the factors:

$\frac{\lambda_i+\lambda_{i+1}}{2}$

as derived directly from the Geant4 simulation.


### Final Energy Scale

Currently the [final calibration energy](#absolute) has been implemented only at
`particleFlow` level, in particular only in the context of `realistic
SimClusters`, which are the truth-based reconstruction of HGCAL that is feed
into Particle Flow. The `C++` module that is responsible of the creation of the
realistic SimClusters and of their validation is `PFClusterProducer` with the
python label `particleFlowClusterHGCal`. The python configuration of the module
contains 2 sets of eta-binned energy corrections, one for `egamma` objects and
one for `hadronic` objects.

!!! warning
    At present this distinction is guaranteed by the fact that this step is
    derived from Monte Carlo truth information. In the real life either we need to
    find a unique set of constants or, better, find a fully reconstruction based way
    to distinguish between the 2 types.

Also in this case, most of the heavy lifting is done by an external worker
loaded via the usual cmssw plugin mechanism. This is
[`RealisticSimClusterMapper`](https://github.com/cms-sw/cmssw/blob/master/RecoParticleFlow/PFClusterProducer/plugins/SimMappers/RealisticSimClusterMapper.cc).
The output of this module is a collection of `PFClusters` that will have the
`energy` set to the **uncorrected one**, and the `correctedEnergy` set to the
**corrected** one.

!!! danger
    The `PFCluster`s derived from the `MultiClusters` **will not have this final
    calibration applied**.


### Configuration files

* [Uncalibrated RecHits](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalUncalibRecHit_cfi.py)
* [Calibrated RecHits](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py)
* [PFCluster Calibration](https://github.com/cms-sw/cmssw/blob/master/RecoParticleFlow/PFClusterProducer/python/particleFlowRealisticSimClusterHGCCalibrations_cfi.py)


## Useful numbers

* The density of silicon is $2.328 \textrm{g}/\textrm{cm}^3$
* The mean value of $\frac{\textrm{d}E}{\textrm{d}x}$ for a
  minimum-ionizing particle, is
$1.664 \textrm{MeV} \textrm{g}^{-1} \textrm{cm}^2$, i.e.
$3.88 \textrm{MeV}/\textrm{cm}$ ($388 \textrm{eV}/\mu\textrm{m}$)
* The energy required to create an e-h pair in Silicon is $3.62
\textrm{eV}$
* The charge on an electron is 1.602e-4 fC
* The most probable energy loss value in $300 \mu\textrm{m}$ silicon (it can
be extracted from the plots in the [*Rescaling to MIPs*](#rescale))
section is $73 \textrm{e}^-/\mu\textrm{m}$ corresponding to
$300 \times 73\times 3.62 \textrm{eV} = 79 \textrm{keV}$.
The corresponding numbers for $200 \mu\textrm{m}$ Silicon are
$70 \textrm{e}^-/\mu\textrm{m}$ and $50 \textrm{keV}$.
For $120 \mu\textrm{m}$ Silicon
$67 \textrm{e}^-/\mu\textrm{m}$ and $29 \textrm{keV}$.
