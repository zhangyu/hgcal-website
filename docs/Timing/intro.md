# Introduction

The HGCROC is able to provide a timing information for each silicon cell, 
with a precision of few tens of ps for energy deposits above $\sim$ 12 fC.   
 
Timing information is simulated for the HGCAL and propagated through the reconstruction.
A first goal is to assign to each reconstructed shower (high-level object) a time measurement 
corresponding to its production time (the time of the associated vertex).
Timing information can also be used in the local reconstruction as additional 
handle to identify compatible energy deposits.
The inputs and algorithms used to provide the timing information in the HGCAL
are based on the first developments done for the HGCAL TDR.
Improvements and tuning could be provided at each level of the simulation and reconstruction.

A simplified list of the key steps related to the timing is:

* Simulation: provide a time-of-arrival (ToA) measurement for every cell, 
accordingly to the ToA expected performance for the HGCROC

* Local reconstruction: propagate the timing information to recHits, layerClusters and tracksters

* High level reconstruction: correct the measured time for the proper time-of-flight(particle id, 
velocity, path length)