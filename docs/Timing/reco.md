# HGCAL timing Reconstruction

## How to estimate the average time of a shower: the truncation function

Is was proven at the time of the TDR that the best resolution on the time estimate
of a shower is obtained focusing on the core of the energy deposition, 
where the development should be instantaneous, thus discarding the tails in the time sorted 
distribution of the energy deposits.   
The appropriate algorithm must be able to identify the core of a distribution, 
to reject too early and too late outliers, regardless of the absolute time of the shower:
the expected spread on the time and position of the vertices at the HL-LHC will lead to 
a spread in the time of showers of few hundreds of picoseconds.   
In zero pileup, the interval containing the 68$\%$ of the time sorted elements (i.e. hits)
is a good solution to identify the core of the distribution and discard tails.
In high pileup the total number of hits explodes, with contribution from multiple showers overlapping 
and creating a non flat distribution in the time domain. As a consequence the interval with 
68$\%$ of the information is no longer a good choice to identify the peaks of 
energy deposits in the time domain.   
To bypass this problem, a fixed size interval, corresponding to the width containing the 68\% of showers in zero pileup, 
can be used to scan the time sorted energy deposits and look for the time of the highest density.
This algorithm proved robust for single showers in 200 pileup and displaced production vertex.
The implementation is provided in the class `ComputeClusterTime` and is available 
[`here`](https://cmssdt.cern.ch/dxr/CMSSW/source/RecoLocalCalo/HGCalRecProducers/src/ComputeClusterTime.cc#64).   
Optimization of the algorithm and new ideas are envisaged to study and correct the 
offsets in the time estimate due to the truncation, and to exploit the full shower information (energy, position)
in the computation of its average time.


## recHit level

The ToA information packed in the `HGCSample` is 
[unpacked](https://cmssdt.cern.ch/dxr/CMSSW/source/RecoLocalCalo/HGCalRecAlgos/interface/HGCalUncalibRecHitRecWeightsAlgo.h#72-74) 
and assigned as ToA to recHits.
Each recHit with ToA in the silicon is also assigned an error, that corresponds to the expected 
resolution based on its [`S/N`](https://cmssdt.cern.ch/dxr/CMSSW/source/RecoLocalCalo/HGCalRecProducers/plugins/HGCalRecHitWorkerSimple.cc#219-224).
An error $= -1$ is set for scintillator cells and silicon cells without ToA information.

!!! info
    The class that handles the parameters relevant for timing in the HGCAL is 
    `RecoLocalCalo/HGCalRecProducers/interface/ComputeClusterTime.h`
    The expected timing resolution on single cells is configured 
    [`here`](https://cmssdt.cern.ch/dxr/CMSSW/source/RecoLocalCalo/HGCalRecProducers/python/HGCalRecHit_cfi.py#121-125).
    These parameters should probably correspond to the same values injected in the simulation step
    to smear the gen-level ToA, and should come from beam test measurements of the final chain 
    of sensor $+$ frond end readout.

!!! info 
    The plots below show the average time and resolution vs energy for recHits belonging 
    to a photon and pion shower of $p_T = 10$, from (0,0,0,0).
    Only recHits within 2 cm from the shower axis as considered.
    The observed trend for the resolution is expected and reflects the precision on the ToA measurements
    injected in the simulation. 
    The trend in the average time is due to the residual time-of-flight
    due to the transverse shower development, as a time delay of about 60 ps is expected for
    energy deposits at a distance of 2 cm traveling at the speed of light.
    It is worth to note that the average time expected for a pion shower is bigger, 
    characteristic of the slow secondary emission in the hadronic shower development.
![Average time for photons](Mean_per_sn_PhotonPt10.png)
![Average resolution for photons](Resolution_per_sn_PhotonPt10.png)
![Average time for pions](Mean_per_sn_low_PionPt10.png)
![Average resolution for pions](Resolution_per_sn_PionPt10.png)


## layerCluster level

For each layerCluster, the time is computed as the weighted average over the time 
of the contributing recHits, with weights corresponding to the error on the time of the recHits.
The error on the layerCluster time is the error on the weighted average.   
To balance efficiency with purity, a minimum number of recHits with ToA is required to compute 
the time for the layerCluster (this criteria should be revisited).   
As mentioned at the beginning of the section, a truncation algorithm is applied on the recHits.
The time for layerClusters is computed
[`here`](https://cmssdt.cern.ch/dxr/CMSSW/source/RecoLocalCalo/HGCalRecProducers/plugins/HGCalLayerClusterProducer.cc#208-227):

* the offset on the ToA added during the simulation is here subtracted
* a minimum of [`nHitsTime`](https://github.com/cms-sw/cmssw/blob/master/RecoLocalCalo/HGCalRecProducers/plugins/HGCalLayerClusterProducer.cc#L122) 
$= 3$ is currently required. This criteria could be improved: 
what if only 2 hits with small (to be defined) time difference are required?

!!! info 
    The two plots below show the average time and resolution vs energy for layerClusters belonging 
    to a photon shower from (0,0,0,0) shot at $\eta = $ 1.75. 
    As in the case for recHits, the observed trend for the resolution is expected 
    and it reflects the precision on the ToA measurements
    injected in the simulation. The trend in the average time is due to the observed trend in the 
    recHits time vs energy.
![Average time](Mean_2Dcl_6plus1_vsE_weighted_lowEta.png)
![Average resolution](Resolution_2Dcl_6plus1_vsE_weighted_lowEta.png)


<!-- ## tracksters level -->

<!-- The time for tracksters can be measured following the same strategy mentioned above:
as weighted average over the times of the contributing layerClusters, 
with weights corresponding to the error in the time of layerClusters.
First implementations of this have shown results consistent with the expectation.
Timing of layerClustersts could also be used as additional compatibility criteria 
in the pattern recognition to help the tracksters reconstruction. -->


<!-- ## time-of-flight correction: restore the time of the production vertex -->

<!-- The time measured by each reconstructed object in the HGCAL (each trackster) -->
<!-- corresponds to the measured time in the calorimeter. -->