# HGCAL timing simulation and Digitization

## Simulation

Timing is injected in the HGCAL simulation with a parametric model, to reproduce the expected performance 
measured in laboratory, and bypassing the problem of lacking from a complete description of the HGCROC electronics simulation.
The ToA is fired with a constant threshold discrimination, at the generator level for every silicon cell, 
and it is afterwords smeared accordingly to the cell specifications.
The involved package is `SimCalorimetry/HGCalSimProducers`.   

The firing of the ToA at generator level is carried out [`here`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/src/HGCDigitizer.cc#708-780)
and it is based on the total energy accumulated in each cell, through `PCaloHits`, that are
produced by Geant4, each carrying information of energy, position, detID and time.
In each cell, the charge accumulation of the time-sorted `PCaloHits` describes the formation of the signal:
when the accumulated charge is at the [`configured threshold`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcROCParameters_cfi.py#34)
the corresponding time is taken as ToA.  

!!! info
    Dealing with the full list of time sorted `PCaloHits`, for all the silicon cells, in 200 PU is
    very demanding in terms of CPU and memory resources. Moreover the interesting information 
    is contained in the first N time sorted `PCaloHits`, provided their cumulative charge is above the ToA firing threshold.
    The implemented version provides an on-the-fly sorting of hits as they are accumulated, 
    to limit the list of saved hits to the minimum needed.
    It is important to note that to compute the total charge deposited in each cell (for the energy estimate) the 
    contribution of all the accumulated `PCaloHits` is needed, but this information can be saved 
    in a single float variable for each cell.   
 
!!! info
    The time of `PCaloHits` corresponds to the time of the charge deposition and it spans 0-25 ns in each bunch-crossing.
    The current implementation only considers hits in the BX=0.
    To collapse all the timing values in a smaller range and prevent BX flipping, due to numerical effects near 0,
    the time of each hit is corrected by 
    [`the time-of-flight of a photon from the vertex (0,0,0)`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/src/HGCDigitizer.cc#546-547), 
    and an
    [`offset`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcalDigitizer_cfi.py#82) is applied.
    
!!! warning
    An improved simulation of the signal pulse shaping is envisaged to 
    quantify the current possible bias from overlapping showers, and correct for it.
    At the same time a realistic time-walk effect, as expected from the HGCROC 
    could be easily accounted for, while this is missing in the current simulation version.



## Digitization

The precision and accuracy of the overall timing measurement depends on the intrinsic performance of the sensor 
plus preamplifier and discriminator, the characteristics of the TDC used for the ToA measurement, 
and the clock distribution system.
The per cell timing resolution expected from the silicon plus electronics chain can be expressed as

$$
\sigma_{time} = \sigma_{jitter} \oplus \sigma_{constant} 
\quad \mathrm{, where } \quad 
\sigma_{jitter} = \frac{A}{S/N} \quad 
\mathrm{, and } \quad \sigma_{constant} = C
$$

The timing resolution is rather uniform over sensors with different thicknesses
when it is expressed as a function of _S/N_.   
The expected value of _A_ and _C_ is provided by electronics measurements.   
The TDC used for the ToA measurement is simulated in the digitization process of the smeared timing values.   
The main effect due to the clock distribution system is to introduce correlated delays in the detector, 
while the corresponding jitter is expected to be negligible compared to _$\sigma_{constant}$_.
Currently there is no a realistic parametrization of the expected effects due to the clock distribution system
and it is not simulated in the HGCAL simulation, thus assuming a perfect calibration of the time offsets in the detector.

The smearing of the ToA values is injected in the simulation in the 
[`HGCFEElectronics`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/src/HGCFEElectronics.cc#239-243)
with parameters [$A^2$](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcROCParameters_cfi.py#19)
and [$C^{2}$](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcROCParameters_cfi.py#21).   
The digitization process is applied [here](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/src/HGCFEElectronics.cc#447)
with a [`bin size`](https://cmssdt.cern.ch/dxr/CMSSW/source/SimCalorimetry/HGCalSimProducers/python/hgcROCParameters_cfi.py#36)
corresponding to a TDC of 25 ns full range and 10 bits.   
The digitized ToA is packed in the `HGCSample` as described [here](https://amartell.web.cern.ch/amartell/HGCal/test/public/Digitization/digis/#hgcsample).    


!!! tip
    The [vertex configuration for HL-LHC](https://cmssdt.cern.ch/dxr/CMSSW/source/IOMC/EventVertexGenerators/python/VtxSmearedHLLHC_cfi.py)
    foresees a spread $\sigma_{z} \sim$ 4.5 cm and $\sigma_{t} \sim$ 180 ps.
    Most of the vertex configurations available in the package _IOMC/EventVertexGenerators_ foresee
    a correlated spread for the vertex time and z position.
    To produce a sample with fixed vertex position, in 0,0,0,0, for example you can import in your python
    configuration for the _GEN-SIM_ step the following
!!! example
```bash
process.load('IOMC.EventVertexGenerators.VtxSmearedGauss_cfi')
process.VtxSmeared.SigmaZ = cms.double(0.)
process.VtxSmeared.SigmaX = cms.double(0.)
process.VtxSmeared.SigmaY = cms.double(0.)
```

!!! info
    The digitization is applied on the ToA smeared values, with a fixed TDC pedestal at zero.
    The effect of dealing with an unsmeared distribution that drops very steeply from a low-edge at zero, 
    and then digitizing it with a TDC that has always a bin edge at zero (which is, of course, unrealistic)
    results in a bias in the average distribution of the digitized values.
    Some plots illustrating the issue are available 
    [`here`](https://amartell.web.cern.ch/amartell/HGCal/timingTDCeffect/)