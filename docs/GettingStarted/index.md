# Quick recipe to run HGCAL reconstruction

## Introduction

In order to test the reconstruction and see its output, we can use one of the
several workflows that have been pushed into the release that are tailored to
HGCAL reconstruction.

## Setup the environment

Let's start with setting up CMSSW.
Open lxplus and run
```shell
cmsrel CMSSW_12_1_0_pre2
cd CMSSW_12_1_0_pre2/src/
cmsenv
git cms-init
```

Compared with the old release CMSSW_11_3_0_pre5, there are some major changes in the new release:

 * TICL v3 for HLT TDR
 * Moving the PatternRecognization step to be plugin based
 * SeedingRegion from L1 object developed by EGamma

The TICL algorithm and framework is documented extensively in the [TICL section][1].

??? tip
    It would be wise to locally setup an Integration Build(IB) that is closer in
    time to the day you are actually doing the work. IBs are regularly deleted
    and will be available for just 2 weeks.


## Simulation and reconstruction recipe

The official tool to test the validity of the reconstruction in CMSSW is the
python script `runTheMatrix.py`. If you to get familiar with such a useful and
wonderful tool, your life will become easier and your development cycle much
faster. The usual way to get used with a new tool is to read its manual:

```shell
runTheMatrix.py --help
```

This will explain the main options available and their meaning.

We will use the workflow number 23293 from the `upgrade` workflows:

```shell
runTheMatrix.py -w upgrade -l 23293.0
```

This workflow will use the `CloseByParticleGun` that is documented
in the [Generation section][2].
It generates a couple of $\gamma$s at the front-face of the HGCAL detector
($Z_{origin} \in [320.99,321.01] \textrm{cm}$, $R_{origin} \in [89.99, 90.01]
\textrm{cm}$). The $\gamma$s will have random energy in the range
$E\in[25.,200]$ GeV. Their direction will point towards the nominal CMS Center
and their (arc) separation will be of $10$ cm.

The previous command will create a directory whose name starts with the workflow number, 
something like `23293.0_CloseByParticleGun**`, which contains
all the configuration files needed to run the job within. The python
configuration files that will be created and executed are basically four:

   * `CloseByParticle_Photon_ERZRanges_cfi_GEN_SIM.py` to generate the two $\gamma$s

   * `step2_DIGI_L1_L1TrackTrigger_DIGI2RAW_HLT.py` to create the
     `CaloParticles` and, more in general, run the so called `DIGI2RAW` step

   * `step3_RAW2DIGI_L1Reco_RECO_RECOSIM_PAT_VALIDATION_DQM.py` to run the
     opposite step, i.e. `RAW2DIGI`, plus the digitization and the actual
     reconstruction

   * `step4_HARVESTING.py` to run the `HARVESTING` step and produce the ROOT file
     containing all the final plots for validation

In the same folder, the root file produced by the correspondent python configuration 
can also be found. Specifically, the step3 and step4 root files will contain results for the whole CMS. However, there may be times that a lighter file is needed, containing only the TICL objects and histograms. There are two files to accomplish that, which can be found under [Validation/HGCalValidation/test/python](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/test/python): 
   
   * `standalone_onlyTICL_fromRECO.py`: This should be inspected and together with a desired workflow from `runTheMatrix.py` tool, `Era` and `globalTag` should be altered. It takes as input an existing step3 root file.
   * `standalone_onlyTICL_HARVESTING.py`: With this file only the HGCalValidator plots will be harvested and the final DQM file will discard plots from other modules. Again, `Era` and `globalTag` should be changed to the desired values. 

### Check out the  HGCAL reconstruction procedure

The information on the HGCAL reconstruction procedure is contained in the `step3`
python configuration file.

If you want to inspect how it has been configured for real, you can interactively open the previous file with the
command: `ipython3 -i
step3_RAW2DIGI_L1Reco_RECO_RECOSIM_PAT_VALIDATION_DQM.py` and, at the
`ipython3` prompt, digit the command `process.iterTICLTask`. You should see
the list of all the modules that are needed to run the HGCAL TICL iterative reconstruction.

## Inspection of the output files

The `step3.root` file will contain the output of the reconstruction step and
will include the objects produced by TICL.

### Explore the EDM File content

A nice trick to explore the content of an `EDM File` (i.e. of a ROOT file
written out by a `CMSSW` job) is to use the command `edmDumpEventContent`. Also
in this case, `edmDumpEventContent -h` will print on the screen a very brief
summary of the options it accepts. For our purposes we can just run it, without
any particular options:

```shell
edmDumpEventContent step3.root | 
  grep -i -E '(tracksters|multiClustersFromTracksters|ticlCandidateFromTracksters|pfTICL)' | 
  sort
```

### Visualize the output

The first thing you need is a proper geometry file description that
`cmsShow` can use to correctly render objects within the HGCAL detector:

```shell
cd $LOCALRT/src/ && git cms-addpkg Fireworks/Geometry && cmsRun Fireworks/Geometry/python/dumpRecoGeometry_cfg.py tag=2026 version=D49
```

The next step is to move to the directory containing the `step3.root` file and launch the `cmsShow` using the geometry file created
with the previous command:

```shell
cmsShow -g ../cmsRecoGeom-2026.root --no-version-check -i step3.root
```

`cmsShow` will present as output the event view in the CMS detector.
By default, it will not show include any TICL output products. 

In order to do that,you click on `Window -> Add Collection` and once the `Add Collection` pop-up window appears, you can digit in the search box the string tracksters and you will be presented with several choices depending on the Module Label and iteration (EM, HAD, ...). You can select the one whose purpose is Tracksters. Here an example for a ttbar event:

![TTbar_Tracksters](ttbar_Tracksters.png)

You can also enable the 3D RecHit view with `(View -> Next Viewer -> 3D RecHit)` and add the collection `Tracksters Hits`. A typical picture on the 3D RecHit view should look like the following:

![CloseByPhotons](Recipe_3D_rechit_view.png)

In case you would like to know more details on the visualization of *objects within
HGCAL in Fireworks*, have a look at the [Visualisation section][3].

## Validation plots

The HGCAL validator is described in detail in the [Validator section][4].
It can be run on the file produced by the `step4` python configuration with
```shell
makeHGCalValidationPlots.py DQM_V0001_R000000001__Global__CMSSW_X_Y_Z__RECO.root
```

The folder produced as output can be moved to a `www` local repository for inspection.

[1]: ../Reconstruction/TICL/
[2]: ../Generation/CloseByParticleGun/
[3]: ../Visualization/visualization/
[4]: ../Validation/HGCalValidator/

