# Material budget from vertex up to in front of Muon Stations

## Introduction

The V10 HGCAL geometry in D41 scenario introduced significant reduction of material budget from V9 (~1.5λ). Motivated by the strong interest in how is this change effecting the Muon system, we tried to estimate the total thickness, in nuclear interaction lengths, in front of Muon Stations versus eta, starting from the vertex. We still produce and inspect the relevant plots described in this section in the current D86 (V16 for HGCAL) geometry scenario. 

For this task, we extended the material budget tool that performed the standard HGCAL-only analysis. Special care was taken so that the existing HGCAL material budget study is not effected, so that someone can easily run both tasks without any conflict.

The results of this work has been presented in the HGCAL DPG ([link](https://indico.cern.ch/event/820095/contributions/3428509/attachments/1844946/3026574/MatBudInFrontofMuonStations_15May19.pdf)) where the relevant plots produced can be inspected. Specifically, apart from the standard material budget plots described previously, there are:

1. A combined 2D map of all sub-detectors to see what exactly are the detectors we are studying and which space they occupy. Keep in mind that the different subdetectors that are plotted here in colour may contain Air as material.
![alt text](MaterialBdg_FromVertexToBackOfHGCal_l_vs_z_vs_Rsum.png "Compined picture of what subdetectors we are adding")

2. The stacked profile histogram of all sub detectors in front of muon stations. This profile histogram displays the mean value of the material budget in units of interaction length in each eta bin. 250 bins in eta (-5,5), so eta is divided in 0.04 width bins.
![alt text](MaterialBdg_FromVertexToBackOfHGCal_l_vs_eta.png "")

3. The 2D profile histograms that displays the mean value of the material budget in units of interaction length in each R-z cell. Radius is defined in 1 cm width bins. z is defined in 2 mm width bins. So, R-z cell is 1 cm x 2 mm. The plot depicts the accumulated material budget as seen by the track, as the track travels throughout all the detectors.
![alt text](FromVertexToBackOfHGCal_l_vs_z_vs_Rsum.png "")

4. All above plots in units of radiation length.

All these plots can be inspected in the [official HGCAL validation website](https://cms-hgcal-validation.web.cern.ch/cms-hgcal-validation/) under the material budget subsection of the geometry and the `From vertex up to in front of muon stations` bullet. 

## Description of the tool

Once again, since the core code is being used by all subdetectors (Tracker, ECAL, HCAL) care was taken not conflict with others. The extension of the material budget tool was introduced in PR [#27126](https://github.com/cms-sw/cmssw/pull/27126). Specifically:

1. Validation/Geometry/interface/MaterialBudgetHGCalHistos.h
Validation/Geometry/src/MaterialBudgetHGCalHistos.cc were extended to be able to have adjustable ranges.

2. Validation/Geometry/src/MaterialBudgetAction.cc was altered to be able to take the adjustable ranges from the cfg file.

3. Plotting tools were updated to accept a multi detector option so that there is no conflict with the standard material budget analysis.
Validation/Geometry/python/plot\_hgcal\_utils.py
Validation/Geometry/test/MaterialBudgetHGCal.py

4. Added an extra main script to run the code:
Validation/Geometry/test/runP\_FromVertexUpToInFrontOfMuonStations\_cfg.py

## A working example

### Prerequisites

Before you get into the material budget volume study, especially for all detectors from vertex up to in front of Muon Stations, you should inspect if there is any change in the detector geometry volume names. The current names are the one in `CMSSW`, which at the moment of this writing is [here](https://github.com/cms-sw/cmssw/blob/master/Validation/Geometry/python/plot_hgcal_utils.py#L36-L53).

To see how to find the names of the volumes check the [Visualization](https://hgcal.web.cern.ch/Visualization/visualization/) section. Below you can find a recipe containing, apart from the normal visualization, another faster way, to get the information we want, since many times cmsShow crashes due to local machines configurations. 

```
#For the visualization
#Take the latest integration build. It should work since we are also 
#running a unit test on the material budget code. 
cmsrel CMSSW_...
cd CMSSW_.../src/
cmsenv
git cms-addpkg Fireworks/Geometry
git cms-addpkg Fireworks/Core
scram b -j
#D86
#We need the sim geometry file, but ok produce also the reco.
cmsRun Fireworks/Geometry/python/dumpRecoGeometry_cfg.py  tag=2026 version=D86
#This is the root file below we need.
cmsRun Fireworks/Geometry/python/dumpSimGeometry_cfg.py  tag=2026 version=D86
#Now there are two ways to see the names. One is with cmsShow, which has
#a nice graphical interface, but it is slow and it may lead to crashes 
# depending on one million things that is not your fault.
cmsShow -c Fireworks/Core/macros/simGeo.fwc --sim-geom-file cmsSimGeom-2026D86.root
# The other is just to open the root file :
root.exe cmsSimGeom-2026D86.root
# Open the TBrowser and go to the 'Master Volume' directory. Then in there 
# right click on the volumes you want to see what they contain and choose 
# 'Print', since for example the thermal screen is not visible in TBrowser. 
# You are ready to adjust the values in plot_hgcal_utils.py 
```

!!! info
    At the D86 geometry there was a gap between ~2800 mm to 2950 mm. After losing time with fireworks and the simulation geo file, at the end the dEdx code was used by checking the output `VolumesZPosition.txt` file, which was the best, easier and safer way to see that the gap was air and that nothing was missing from the CALO geometry. So, use the CMSE world volume that contains everything and check the material.

### The main event

Now that we know the volumes names, we can continue with the main task. The work can be essentially divided in 3 parts:

1. For this task a neutrino gun is used, since they interact minimally with the detector material and have straight trajectories. So, first step is to produce the neutrino gun sample (`single_neutrino_random.root`).
2. Then, use multiple times the `runP_FromVertexUpToInFrontOfMuonStations_cfg.py` script to produce the `matbdg_*.root` file, where * the different subdetectors we wish to add.
3. Finally, produce the final plots mentioned above by using the `MaterialBudgetHGCal.py` script.

The complete recipe follows:

```
mkdir materialbudgetforV16
cd materialbudgetforV16
#Take the latest integration build.
cmsrel CMSSW_...
cd CMSSW_...
cmsenv
git cms-addpkg Validation/Geometry
#We will need HGCalValidator for the webpage creation
git cms-addpkg Validation/HGCalValidation
(cd $LOCALRT && scram b disable-biglib)
#It needs a second cmsenv
cmsenv
#Compile
scram b -j 4
cd Validation/Geometry/test
# Open single_neutrino_cfg.py and put:
# 1. 1 million events for the official results (100000 may be enough).
# 2. Change the eta region to what is in front of HGCAL plus a little more:
#        MinEta = cms.double(1.0),
#        MaxEta = cms.double(5.5),
# 3. AddAntiParticle = cms.bool(True)
# since this file is being used by others too.
# Produce single_neutrino_random.root .
cmsRun single_neutrino_cfg.py nEvents=10000
# Produce the matbdg_*.root file.
#Here we choose the D86 (V16) geometry scenario.
# It will take some time, especially for 1 M events.
# You will need at least 12 hours running time. 
# Observe python3 used and not cmsRun.
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label=BeamPipe
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label=Tracker
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label=ECAL
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label=HCal
#EndcapTimingLayer (Barrel Timing Layer is included in the Tracker)
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label=EndcapTimingLayer
#Neutron Moderator + Thermal Screen
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label='Neutron Moderator + Thermal Screen'
#HGCal + HGCal Service + Thermal Screen
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label='HGCal + HGCal Service + Thermal Screen'
#Solenoid Magnet
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label='Solenoid Magnet'
#Muon Wheels and Cables
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label='Muon Wheels and Cables'
#Finally, all together
time python3 runP_FromVertexUpToInFrontOfMuonStations_cfg.py geom=Extended2026D86 label=FromVertexToBackOfHGCal
# Produce the final plots
mkdir matbdg86
mv *.root matbdg86/.
cd matbdg86/.
#The plots with -s will be under the Images directory (single detector)
#while the plots -m for all detectors will be under Figures directory
#These commands below will take some time.
#For the material budget of all detectors do
python3 ../MaterialBudgetHGCal.py -m -d FromVertexToBackOfHGCal
# The plots will all be under the Figures directory
# To get the total 2d R sum accumulated plot remember to use the single option -s not -m).
python3 ../MaterialBudgetHGCal.py -s -d FromVertexToBackOfHGCal
# Since tha above is treated as a single detector the plots will all be under the Images directory.
```

If you have finished also the HGCAL-only material budget analysis, then you can proceed to the [Campaign Validation](https://hgcal.web.cern.ch/Validation/RelVals/) geometry section that contains the recipe to put all these results to the official HGCAL Validation webpage. 



