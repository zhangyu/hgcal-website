# Standard HGCAL-only material budget analysis

## Introduction

A valuable tool has been developed, not only for calculating the material budget of the HGCAL detector but also for validating the simulation
geometry. The tool lives under the [Validation/Geometry](https://github.com/cms-sw/cmssw/tree/master/Validation/Geometry) package.
Material budget in units of radiation length is defined as path length in the
material/material’s radiation length, while material budget in units of interaction length is defined as path length in the material/material’s
interaction length.

After running the tool a large number of plots will be produced:

1. Stacked profile histograms of all materials. These profile histograms display the mean value of the
material budget in units of radiation/interaction length in each eta/phi bin.
2. 2D profile histograms that display the mean value of the material budget in units of radiation/interaction length
in each eta-phi cell.
3. 2D profile histograms that display the mean value of the material budget in units of radiation/interaction length
in each R-z cell (orthogonal local, local, accumulated).
4. A large number of individual material plots.

Due to the vast amount of plots initially a [specific function](https://github.com/cms-sw/cmssw/blob/master/Validation/Geometry/python/plot_hgcal_utils.py#L128) was used to print out information, that was placed in a twiki page, so that someone can easily see all the plots by clicking open/hide switches. In there apart from the png, the pdf version of the plots could be also inspected, which can be enlarged without losing resolution.  

However, now we moved to a dedicated section in the official HGCAL validation website. In the [central webpage](https://cms-hgcal-validation.web.cern.ch/cms-hgcal-validation/), that contains the results of the campaigns validation, we have added a section ([PR #33131](https://github.com/cms-sw/cmssw/pull/33131)), where we wish to monitor several aspects of geometry. The first bullet contains the material budget plots. More technical info of how to create this page can be found on the [Validation](https://hgcal.web.cern.ch/Validation/HGCalValidator/) section.  

The tool is used whenever a new, substantially different HGCAL geometry is introduced. The presentation for V9 is [here](https://indico.cern.ch/event/742979/contributions/3068706/attachments/1684960/2709072/HGalMaterialAnalysis_11_07_2018_HGCAL_DPG.pdf), while for V10 can be found in this [link](https://indico.cern.ch/event/813555/contributions/3400815/attachments/1831509/2999507/Psallidas-MaterialBudgetV10.pdf). The latest comparison between the D49 and the D76 geometry scenatio or V11 vs V14 for HGCAL can be found in the [official validation website](https://cms-hgcal-validation.web.cern.ch/cms-hgcal-validation/Extended2026D49_vs_Extended2026D76/index.html). V11 is the default baseline scenario that was used for the HLT TDR. It is the post-TDR geometry with 50 layers while CE-E, CE-H layers are using wafer centering and corner centerings respectively. For V14 the support structure is updated and the sensor layer layouts are exactly modelled from flat files, while partial wafers are implemented. 

Apart from the material budget calculation itself, a meticulous inspection of the individual material plots can reveal problems and shouldn't be overlooked. In the past numerous issues in the absorber structure have been spotted. 

## Description of the tool

Since the core code is being used by all subdetectors (Tracker, ECAL, HCAL) care was taken not conflict with others. The HGCAL part of the package was first introduced in PR [#23770](https://github.com/cms-sw/cmssw/pull/23770), while it was updated on the V10 (2026D41) HGCAL geometry scenario in PR [#26384](https://github.com/cms-sw/cmssw/pull/26384).
Specifically:

1. Added two files to declare/define HGCAL materials:
Validation/Geometry/data/hgcalMaterials.l0
Validation/Geometry/data/hgcalMaterials.x0

2. Added two files for the HGCAL histos that will be produced:
Validation/Geometry/interface/MaterialBudgetHGCalHistos.h
Validation/Geometry/src/MaterialBudgetHGCalHistos.cc

3. Added HGCAL option:
Validation/Geometry/interface/MaterialBudgetAction.h
Validation/Geometry/src/MaterialBudgetAction.cc

4. Added HGCAL map of materials alongside the tracker one:
Validation/Geometry/interface/MaterialBudgetCategorizer.h
Validation/Geometry/src/MaterialBudgetCategorizer.cc

5. Added the reading of HGCAL data.
Validation/Geometry/interface/MaterialBudgetData.h
Validation/Geometry/src/MaterialBudgetData.cc

6. Plotting tools for all HGCAL material analysis plots that are not in the usual plot\_utils.py . Whatever is needed and was already in the repository was imported in order to avoid code duplication:
Validation/Geometry/python/plot\_hgcal\_utils.py
Validation/Geometry/test/MaterialBudgetHGCal.py

7. Added the main script of running the code:
Validation/Geometry/test/runP\_HGCal\_cfg.py

## A working example

The work can be essentially divided in 3 parts:

1. For this task a neutrino gun is used, since they interact minimally with the detector material and have straight trajectories. So, first step is to produce the neutrino gun sample (`single_neutrino_random.root`).
2. Then, use the `runP_HGCal_cfg.py` script to produce the `matbdg_HGCal.root` file.
3. Finally, produce the final plots mentioned above by using the `MaterialBudgetHGCal.py` script.

In the complete recipe that follows below observe that we use `python3` and not `cmsRun`. To find out more information about this, check out [PR #31504](https://github.com/cms-sw/cmssw/pull/31504). Also, lately there was an update to use `python3` tools ([PR #33597](https://github.com/cms-sw/cmssw/pull/33597)). The complete recipe that was used for the V16 HGCAL geometry scenario (D86) follows:

```
#Create a dedicated area
mkdir materialbudgetforV16
cd materialbudgetforV16
#Take the latest integration build. e.g.
cmsrel CMSSW_...
cd CMSSW_...
cmsenv
git cms-addpkg Validation/Geometry
#We will need HGCalValidator for the webpage creation
git cms-addpkg Validation/HGCalValidation
(cd $LOCALRT && scram b disable-biglib)
#It needs a second cmsenv
cmsenv
#Compile
scram b -j 4
cd Validation/Geometry/test
# Open single_neutrino_cfg.py and put:
# 1. 1 million events for the official results (100000 may be enough).
# 2. Change the eta region to what is in front of HGCAL plus a little more:
#        MinEta = cms.double(1.0),
#        MaxEta = cms.double(5.5),
# 3. AddAntiParticle = cms.bool(True)
# since this file is being used by others too.
# Produce single_neutrino_random.root .
cmsRun single_neutrino_cfg.py nEvents=10000
# Produce the matbdg_*.root file.
#Here we choose the D86 (V16) geometry scenario.
# It will take some time, especially for 1 M events. 
# Observe python3 used and not cmsRun. 
python3 runP_HGCal_cfg.py geom=Extended2026D86 label=HGCal
mkdir matbdg86
mv *.root matbdg86/.
cd matbdg86/.
#Now make the HGCAL specific results because remember the results 
#from the material budget study from vertex up to in front of muon 
#stations is here
mkdir forHGCAL
cp matbdg_HGCal.root forHGCAL/.
cd forHGCAL/
time python3 ../../MaterialBudgetHGCal.py -s -d HGCal
#Rename to be in agreement with website
mv Images Extended2026D86
```

If you have finished also the material budget analysis from vertex up to in front of Muon Stations, then you can proceed to the [Campaign Validation](https://hgcal.web.cern.ch/Validation/RelVals/) geometry section that contains the recipe to put all these results to the official HGCAL Validation webpage. 

!!! warning
    Keep in mind that in this workflow the neutron moderator plus the thermal screen is not taken into account. This is studied in a different work where we calculate the total amount of material budget starting from the vertex up to in front of the muon stations. Instructions for this task are written in the next subsection.

!!! warning
    Whenever a new geometry scenario is created, we should check if it contains new materials. In that case the "Other" category distributions will be filled. This is not acceptable. The new materials should be introduced to the package and then a new PR should be submitted to the CMSSW repository, so that at the end the "Other" category distributions are all empty.

!!! info
    We have added with [PR #31565](https://github.com/cms-sw/cmssw/pull/31565) a [unit test](https://github.com/cms-sw/cmssw/blob/master/Validation/Geometry/test/genHGCalPlots.sh), so that every time a new integration build is created, we can be aware of any problems/issues. 

!!! info
    Keep in mind in the recipe above that you should disable biglib before compiling, since if you don't the code will crash complaining that plugin `MaterialBudgetAction` shouldn't have been loaded from biglib. 