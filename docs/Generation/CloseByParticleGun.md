# CloseByParticleGun

A useful tool that can be used to study events inside HGCAL only is the
[`CloseByParticleGun`](https://github.com/cms-sw/cmssw/blob/master/IOMC/ParticleGuns/interface/CloseByParticleGunProducer.h).
With this event producer it's possible to create several vertices (i.e. in front
of HGCAL). Mind that it is only available in `CMSSW_10_6_0` or later.  Several
parameters can be set and are described in the table below.

| Parameter | Usage |
| --------- | ----- |
| `PartID` | Each particle will be produced with a pdg ID picked randomly from this vector |
| `EnMin`/`EnMax` | Each particle will be shot with a random `E` (energy) within this range |
| `RMin`/`RMin` | All the particles will be shot with a random `R` (distance from the beamline) within this range |
| `ZMin`/`ZMin` | Each particle will be shot with a random `Z` (depth) within this range |
| `Delta` | If `Overlapping` is `True`, each particle will be shot within a window [`R-Delta`, `R+Delta`],[`phi-Delta/R`, `phi+Delta/R`]. If `Overlapping` is `False`, it is the arc-distance between two consecutive vertices over the circle of radius `R` |
| `Pointing` | If `True`, particles are shot pointing to (0,0,0). If `False`, they will be produced parallel to the beamline |
| `Overlapping` | If `True`, particles will be shot in a window as described for `Delta` and showers can overlap. If `False` particles are separated by a distance `DeltaPhi=Delta/R` |
| `RandomShoot` | If `True`, a random number of particles will be shot in the range [`1`, `NParticles-1`]. If `False`, `NParticles` will be shot |
| `NParticles` | Either the number or the maximum number of particles to shoot, as described for `RandomShoot` |
| `MinPhi`/`MaxPhi` | A random `phi` is selected within this range. Then each particle will be shot with `phi` within the range [`phi-Delta/R`, `phi+Delta/R`] if `Overlapping` is `True`, otherwise `phi` will be increased by `Delta/R` with respect to the previous particle shot |
| `MinEta`/`MaxEta` | Each particle could be shot with a random `eta` within this range. These parameters are not used at the moment (`R` is used instead)|

## Sample snippet

As an example, the configuration taken from a working [snippet](https://github.com/cms-sw/cmssw/blob/master/Configuration/Generator/python/CloseByParticle_Photon_ERZRanges_cfi.py) available in `CMSSW` is shown below.

```python
generator = cms.EDProducer("CloseByParticleGunProducer",
    PGunParameters = cms.PSet(
        PartID = cms.vint32(22),
        EnMin = cms.double(25.),
        EnMax = cms.double(200.),
        RMin = cms.double(60),
        RMax = cms.double(120),
        ZMin = cms.double(320),
        ZMax = cms.double(321),
        Delta = cms.double(10),
        Pointing = cms.bool(True),
        Overlapping = cms.bool(False),
        RandomShoot = cms.bool(False),
        NParticles = cms.int32(2),
        MaxEta = cms.double(2.7),
        MinEta = cms.double(1.7),
        MaxPhi = cms.double(3.14159265359),
        MinPhi = cms.double(-3.14159265359),
    )
```

This configuration will "shoot" 2 photons in front of HGCAL with energy between
25 and 200 GeV, the same `R` between 60 and 120 cm, separated by a distance
`DeltaPhi=Delta/R` and pointing towards (0,0,0).

## Tricks to handle **non-pointing** `CaloParticles`

### Step 2 phase: simulation

The `CloseByParticleGun` gives the ability to the user to generate
**non-pointing** particles that are parallel to the beamline. This implies that
their associated $\eta$ values are unphysical and the implicit requirement on
$\eta$ that has to be satisfied, fails:
[link](https://github.com/cms-sw/cmssw/blob/master/SimGeneral/MixingModule/python/caloTruthProducer_cfi.py#L10).
For this reason, if the user does not change the code, the simulation will
properly generate the `CaloParticles` but they will not be saved into the
`Event` or in the output ROOT file. In order to solve this issue, the user has
to edit the `step2.py` file and add the following snippet (usually towards the
end of the file):

```python
process.mix.digitizers.calotruth.MaxPseudoRapidity = cms.double(float('+inf'))
```

This will enlarge the $\eta$ acceptance to accomodate also the **non-pointing**
`CaloParticles`.

### Step 3 phase: reconstruction and validation

In the standard validation sequence for `HGCAL` there is a selection on the
`CaloParticles` that the user could select in order to compute the efficiency
and the duplicate rate. This selection is applied using a
`CaloParticleSelector`:
[link](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/interface/CaloParticleSelector.h).
Unfortunately, the way in which the $\eta$ cut was computed prevented the
possibility to use **non-pointing** `CaloParticles` for validation purposes. To
overcome this issue, a new configuration parameter has been added in order to
completely skip the selection and simply use all the available `CaloParticles`.
In order to activate it, the user must change the value:

```python
process.hgcalValidator.doCaloParticleSelection = False
```


