# Monte Carlo Event Generators

The very first step is to generate the event we want to reconstruct.

Monte Carlo Event Generators are used to generate high-energy-physics events, 
that in the end will be a set of outgoing particles produced in the 
interactions between two incoming particles (mostly proton-proton).

The generator in CMSSW can go from the very simple particle guns to the more complex events (such as ttbar).

In the [Quick recipe section][1], the `runTheMatrix` tool was already briefly presented to generate particles in CMSSW. 
You can look at all the workflows available in your release for the upgrade with the command:

```shell
runTheMatrix.py -w upgrade -e -n | less
```

[1]: ../GettingStarted/

## Definitions

Objects which are particularly important for the HGCAL simulation are:

* **SimTrack**: a
  [`SimTrack`](https://github.com/cms-sw/cmssw/blob/master/SimDataFormats/Track/interface/SimTrack.h)
  is a track that has been produced by **Geant4**, i.e. this track can be a
  product of the interaction with the detector.
* **GenParticle** : a
  [`GenParticle`](https://github.com/cms-sw/cmssw/blob/master/DataFormats/HepMCCandidate/interface/GenParticle.h)
  is a track that has been produced by **Pythia**, i.e. this track is usually
  the product of the initial p-p collision and cannot come from the _downstream_
  detector simulation.

!!! note
    Not all **SimTracks** are **GenParticles** (the ones coming from decays and
    from the interaction with the detector), but usually all the **GenParticles**
    (within the detector acceptance) are **SimTracks**.

All GenParticles created in a particle shower in the calorimeter are associated
to the GenParticle 'mother' by GEANT4.
