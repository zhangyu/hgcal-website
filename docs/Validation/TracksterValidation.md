# Trackster metrics

## Definitions

### Association Metric between `CaloParticles` and `Tracksters`

We use the following metric in order to associate $\mathrm{CaloParticle}_i$ to
$\mathrm{Trackster}_j$:

$$
\mathrm{score3D}_{ij} = \frac{%
\sum_\limits{k,l}^{{\mathrm{DetId}_k\mathrm{~of~SC}_l \mathrm{\in CP}_i}}
\left(
\mathrm{fr}^{\mathrm{TST}_{j\mathrm{,reco}}}_{k} -
\mathrm{fr}^{\mathrm{SC}_{l\mathrm{,MC}}}_k
\right)^2 \times \epsilon_{k}^{2}
}{
\left[\sum_\limits{h,l}^{{\mathrm{DetId}_h\mathrm{~of~SC}_l \mathrm{\in
CP}_i}}\mathrm{fr}^{\mathrm{SC}_{l\mathrm{,MC}}}_h\epsilon_h\right]^2
}
$$

where:

  * $SC_l$ represents the $l$-SimCluster of the $i$-CaloParticle under investigation,

  * $\mathrm{fr}^{\mathrm{TST}j\mathrm{,reco}}_{k}$ represents the fraction of the reconstructed $j$-Trackster for the $k$-DetId,

  * $\mathrm{fr}^{\mathrm{SC}_{l\mathrm{,MC}}}_k$ represents the fraction of the generated $l$-SimCluster for the $k$-DetId belonging to the $i$-CaloParticle,

  * $\epsilon$ is the energy associated to a particular RecHit/DetId.

### Association Metric between `Tracksters` and `CaloParticles` 

In this case the association metric between $\mathrm{TST}_i$ and $\mathrm{CP}_j$ was chosen to be: 

$$
\mathrm{score3D}_{ij} = \frac{%
  \sum\limits_{k,l}^{\{\mathrm{DetId}_k \in \mathrm{TST}_i,
  \mathrm{SC}_l \mathrm{~of~CP}_j\}}
\left(
  \mathrm{fr}^{\mathrm{TST}_i\mathrm{,reco}}_{k} -
  \mathrm{fr}^{\mathrm{SC}_l\mathrm{,MC}}_k
\right)^2 \times \epsilon_{k}^{2}
}{
  \left[\sum\limits_{h}^{\{\mathrm{DetId}_h\mathrm{~of~TST}_i\}}\mathrm{fr}^{\mathrm{TST}_i\mathrm{,reco}}_h\epsilon_h\right]^2
}
$$

where:

   * $\mathrm{TST}_i$ represent the $i$-`Trackster`.

### Efficiency

Ratio between `CaloParticles` that have been associated to a reconstructed
`Trackster` divided by all the generated `CaloParticles`. Those plots are
produced versus the **generated** (i.e. of the `CaloParticle`) $\eta$ and
$\phi$. The association is performed by setting a threshold in
$\mathrm{score3D}$ below of which it is considered successful. The successful
associations are counted in the **DQM histo** *Num\_CaloParticle\_Eta*,
*Num\_CaloParticle\_Phi* while we keep track of the generated `CaloParticles` in
*Denom\_CaloParticle\_Eta*, *Denom\_CaloParticle\_Phi*.

### Duplicate (Trackster Splitting)

Ratio between the `CaloParticles` that have been associated to *more than one*
`Trackster` divided by all the generated `CaloParticles`. Those plots are
produced versus the **generated** (i.e. of the `CaloParticle`) $\eta$ and
$\phi$. The association is performed by setting a threshold in
$\mathrm{score3D}$ below of which it is considered successful. The multiple
successful associations are counted in the **DQM histo**
*NumDup_CaloParticle_Eta* and *NumDup_CaloParticle_Phi*.

### Fake Rate

One minus the ratio between the `Tracksters` that have been associated to a
`CaloParticle` divided by all the reconstructed `Tracksters`. Those plots are
produced versus the **reconstructed** (i.e. of the `Trackster`) $\eta$ and
$\phi$. The successful associations are counted in the **DQM histo**
*Num\_Trackster\_Eta* and *Num\_Trackster\_Phi*, while we keep track of
all the reconstructed `Tracksters` with *Denom\_Trackster\_Eta* and
*Denom\_Trackster\_Phi*.

### Merge Rate

Ratio between the `Tracksters` that have been associated to *more than one*
`CaloParticle` divided by all the reconstructed `Tracksters`. Those plots are
produced versus the **reconstructed** (i.e. of the `Trackster`) $\eta$ and
$\phi$. The multiple successful associations are counted in the **DQM histo**
*NumMerge\_Trackster\_Eta* and *NumMerge\_Trackster\_Phi*.


!!! tips
    In short, if one `Trackster` is associated with two `CaloParticles` we
    refer to it as merge, while if one `CaloParticle` is associated with two
    `Tracksters` we refer to it as split. 

## Plots

### CP2TST

#### Score CP2TST

These plots shows the distribution of the score of the CP2TST association. There
is one entry per `CaloParticle`. This should help guiding the eye to set a
proper threshold to associate CP2TST. `CaloParticles` that have a good match
with a `Trackster` will tend to have a score close to 0, while poorly matched
ones will tend to have a score closer to 1.

!!! info
    A high score means that several components (`DetIds`) of the `CaloParticle`
    are not reconstructed by the `Trackster` for which the score is computed.
    This is scaled with the energy squared, so that a missed energetic
    contribution is given a higher penalty.

![Example of Score of CP2TST](Score_caloparticle2trackster.png "Example of distribution of CP2LC")

#### Shared Energy of CP and TST

This plot shows the distribution of the energy shared between a `CaloParticle`
and a `Trackster`.

!!! warning
    **The plot is filled w/o requiring any threshold on the association between
    the 2 objects**.

It is filled for all `CaloParticles` times all the `Tracksters` that
contributed somehow to it. The ratio is between the energy deposited by the
`Trackster` on all layers **belonging to this particular `CaloParticle`** and
the energy associated to the same `CaloParticle`.

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{TST in CP}}}{\textrm{Energy}_{\textrm{CP}}}$

A ratio close to 1 implies that a single `Trackster` collected the majority
of the energy released by a `CaloParticle`. A ratio close to 0 implies that the
`Trackster` has reconstructed a small fraction of the Monte Carlo one.

???+ info
    By definition the ratio is limited to the [0,1] interval. The fraction
    represents the amount of energy of a single `CaloParticle` assigned to a
    **unique** `Trackster`. Values below 1 and considerably greater than 0,
    i.e. in the range [0.2,0.8] are indication of **cluster splitting**, since a
    single Monte Carlo contribution has been assigned to several
    `Tracksters`, each with a fraction considerably smaller than 1. 
    
    The relative high population at very low values, i.e. in the range [0,0.1],
    represents _dust_ around the **core** of the shower that has been
    reconstructed as single `Tracksters` which will inevitably hold an
    extremely low fraction of the overall `CaloParticle` energy. This could be
    more evident in the case of **hadronic showers**.

![Example of shared energy between CP and TST](SharedEnergy_caloparticle2trackster.png
"Example of shared energy between CP and TST")

These plots are produced as profiles versus $\eta$ and $\phi$ of the generated
`CaloParticle`.

!!! warning
    **The plots versus $\eta$ and $\phi$ are only filled if the association
    criteria for CP2TST is satisfied and, in case of multiple matches, the best
    TST (the one with the lowest score) is selected.**

#### Energy versus Score CP2TST

This is a 2D histogram that shows the correlation between the score of the
CP2TST (on the X-axis) and the fraction of energy shared between the two objects
(on the Y-axis). The definition of the shared energy is identical to the case
above, namely:

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{TST in CP}}}{\textrm{Energy}_{\textrm{CP}}}$

By definition the ratio is limited to the [0,1] interval.

!["Energy versus Score for CP2TST for DR = 2.5"](Energy_vs_Score_caloparticle2trackster_0.png "Energy versus Score for CP2TST for DR = 2.5")

!["Energy versus Score for CP2TST for DR = 5.0"](Energy_vs_Score_caloparticle2trackster_1.png "Energy versus Score for CP2TST for DR = 5.0")

These plots are useful to guide the eye in selecting a good threshold in order
to associate a `CaloParticle` to a `Trackster`, while also taking into
account how much of the actual energy is indeed shared between the 2 objects.

!!! warning
    **The plot is filled w/o requiring any threshold on the
    association between the 2 objects**.

???+ info

     A high concentration of points in the upper-left corner means that we have
     many `CaloParticles` with excellent score (0 on the X-Axis) that are
     associated to a reconstructed `Trackster` that gathered a considerable
     amount of the Monte Carlo energy (1 on the Y-Axis).
     
     An accumulation of points with relatively larger scores approaching 1 and distributed along a wide range of Y-values could be an
     indication of **cluster splitting**, as explained in the previous section.

### TST2CP

#### Score TST2CP

These plots show the distribution of the score of the TST2CP association. There
is one entry per reconstructed `Trackster`. This should help guiding the eye
to set a proper threshold to associate TST2CP.  Extremely good `Tracksters`
will tend to have a score closer to 0, while poorly assigned/reconstructed
`Tracksters` will have a score closer to 1.

![Example of Score of TST2CP](Score_trackster2caloparticle.png "Example of distribution of TST2CP")

#### Shared Energy of TST and CP

This plot shows the distribution of the energy shared between a `Trackster`
and a `CaloParticle`.

!!! warning
    **The plot is filled w/o requiring any threshold on the
    association between the 2 objects**.

It is filled for all reconstructed `Tracksters` times all the `CaloParticles`
that contributed somehow to each `Trackster`. The ratio is between the energy
deposited by the `CaloParticle` in all layers and reconstructed by a
`Trackster` and the overall energy associated to the same `Trackster`.

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{TST in CP}}}{\textrm{Energy}_{\textrm{TST}}}$

A ratio close to 1 implies that the `Trackster` collected the majority of the
energy released by a **unique** `CaloParticle`. A ratio close to 0 implies that
the contribution of the `CaloParticle` to the `Trackster` is negligible.

???+ info
    
    By definition the ratio is limited to the [0,1] interval. The fraction
    represents the amount of energy of the `Trackster` assigned to a
    **unique** `CaloParticle`. Values below 1 and considerably greater than 0,
    i.e. in the range [0.2,0.8], are indication of **cluster merging**, since a
    single `Trackster` has contributions coming from several `CaloParticles`,
    each with a fraction considerably smaller than 1. 

![Example of shared energy between TST and CP](SharedEnergy_trackster2caloparticle.png
"Example of shared energy between TST and CP")

These plots are also produced as profiles versus $\eta$ and $\phi$ of the
reconstructed `Tracksters`.

!!! warning    
    **The plots versus $\eta$ and $\phi$ are only filled if the association criteria for TST2CP is satisfied and, in case of multiple matches, the best CP (the one with the lowest score) is selected.**

#### Energy versus Score TST2CP


This is a 2D histogram that shows the correlation between the score of the
TST2CP (on the X-axis) and the fraction of energy shared between the two objects
(on the Y-axis). The definition of the shared energy is identical to the case
above, namely:

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{TST in CP}}}{\textrm{Energy}_{\textrm{TST}}}$

!["Energy versus Score DR = 2.5"](Energy_vs_Score_trackster2caloparticle_0.png "Energy versus Score for TST2CP DR = 2.5")

!["Energy versus Score DR = 5.0"](Energy_vs_Score_trackster2caloparticle_1.png "Energy versus Score for TST2CP DR = 5.0")

These plots are useful to guide the eye in selecting a good threshold in order
to associate a `Trackster` to a `CaloParticle`, while also taking into
account how much of the actual energy is indeed shared between the 2 objects.

!!! note
    **The plot is filled w/o requiring any threshold on the
    association between the 2 objects**.

???+ info
     A high concentration of points in the upper-left corner means that we have
     many reconstructed `Tracksters` with excellent score (0 on the X-Axis)
     that collected a considerable amount of the Monte Carlo energy (1 on the
     Y-Axis).

     An accumulation of points with relatively small scores (in the range
     [0.1,0.4]) and distributed along a wide range of Y-values could be an
     indication of **cluster merging**, as explained in the previous section.

	














