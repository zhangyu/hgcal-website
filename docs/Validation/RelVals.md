# Campaing Validation

PdmV and UPSG groups have initiated the validation of CMSSW versions on a
regular basis through the so called RelVal samples.

The RelVal samples are: 
   
   * regularly produced at every pre-release and release,
   * produced with a relatively small statistics (9K events).

Following the usual approach, validation campaigns are opened on
[ValDb](https://cms-pdmv.cern.ch/valdb/) where we are required to submit a
report. 

We will here give a working example for the validation of 11\_0\_0\_pre7\_phase2
validation campaing. 

# A working example 

First, we setup a working area in lxplus based on the CMSSW release we are asked
to validate. In this example we will validate 11\_0\_0\_pre7. So, 

```
cmsrel CMSSW_11_0_0_pre7
cd CMSSW_11_0_0_pre7/src/
cmsenv
git cms-addpkg Validation/HGCalValidation
git cms-addpkg Validation/RecoTrack
```

The [Validation/RecoTrack](https://github.com/cms-sw/cmssw/blob/master/Validation/RecoTrack)
package is checked out, since we will use the very powerful script [Validation/RecoTrack/python/plotting/validation.py](https://github.com/cms-sw/cmssw/blob/master/Validation/RecoTrack/python/plotting/validation.py) to easily read all the RelVal samples. You can find the DQM files of the RelVal samples [here](https://cmsweb.cern.ch/dqm/relval/data/browse/ROOT/RelVal/). Find the relevant folder with the release we are interested ([this one](https://cmsweb.cern.ch/dqm/relval/data/browse/ROOT/RelVal/CMSSW_11_0_x/)
in our example) and do `Ctrl+A` to copy all files and paste them in a file in
lxplus, e.g. `relvals.txt` . 

Now, we are ready to print the DQM samples of the campaign. From the
announcement of the campaign you should know the global tag of the target
release. In our case it is `110X_mcRun4_realistic_v1`. Do, 

```
cat relvals.txt |grep 110X_mcRun4_realistic_v1 | grep CMSSW_11_0_0_pre7 | awk '{print $1}' > samples.txt
```

Usually, a lot of samples are produced and will for sure be helpful in the future.
However, at the moment where many things are under development in the detector,
we will first start with the **CloseByParticleGun** samples. Then, we will check
also the other samples. These RelVal samples where introduced with [PR #27631](https://github.com/cms-sw/cmssw/pull/27631)
and generate two photons, separated with DR = 10 cm, with a random energy
between 25 GeV to 200 GeV while the gun points to the vertex. We chose to shoot
in 8 different regions of the detector:

1. CE\_E\_Front\_120um (R = 55 cm, Z = 321 cm).
2. CE\_E\_Front\_200um (R = 90 cm, Z = 321 cm).
3. CE\_E\_Front\_300um (R = 135 cm, Z = 321 cm).
4. CE\_H\_Fine\_120um (R = 50 cm, Z = 362 cm).
5. CE\_H\_Fine\_200um (R = 90 cm, Z = 362 cm).
6. CE\_H\_Fine\_300um (R = 135 cm, Z = 362 cm).
7. CE\_H\_Coarse\_300um (R = 80 cm, Z = 430 cm).
8. CE\_H\_Coarse\_Scint (R = 180 cm, Z = 430 cm).

since we want to target sensors of specific thicknesses. Some tweaks will be needed for the exact (R,Z) position of the gun in V16 HGCAL geometry scenario and in any later geometry scenario deemed necessary.

??? tip
    Keep in mind when inspecting the plots that by definition in these samples there is no logic to add the antiparticle. 

??? tip
    There are no RelVals with PU with the closeby gun, since at the moment we want to understand basic things of the performance of the detector. 

Open the [Validation/RecoTrack/python/plotting/validation.py](https://github.com/cms-sw/cmssw/blob/master/Validation/RecoTrack/python/plotting/validation.py) and add in the `_globalTags` variable our release and global tag. 

```
"CMSSW_11_0_0_pre7" : {"default": "110X_mcRun4_realistic_v1"}
```

as well the url with the RelVals if it doesn't exist in the `_relvalUrls`
variable. You should compile when ready with `scram b -j`.

The validation of the campaign can be done either by inspecting the [DQM GUI](https://cmsweb.cern.ch/dqm/relval)
or running some standalone code. The objects that we are going to check are
`SimHits`, `Digis`, `RecHits`, `Calibrations`, `CaloParticles`, `hgcalLayerClusters`, `simClusters`, `simTracksters`, `tracksters`. No higher level objects in this list since these will be validated by the POG/PAG. 

In order to automate the validation procedure and make it simpler, we have created the `Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py` script. Each time a new campaign is announced we should: 

1. Update the information in the Input section of the script by providing everything needed as is being described: 
   * NewRelease: The new release being validated.
   * RefRelease: The reference release we want to test the new one against.
   * thereleases: All releases validated for which we have validation results including the NewRelease. 
   * phase2samples_noPU: These are the noPU phase 2 RelVal samples that are regurarly produced.   
   * phase2samples_PU: These are the PU phase 2 RelVal samples that are regurarly produced.  
   * RefRepository, NewRepository: The path where the DQM files of the campaign will be placed.
  
   To better know the format of the Sample class, that is used for the `phase2samples_noPU` and `phase2samples_PU` variables, check it [here](https://github.com/cms-sw/cmssw/blob/master/Validation/RecoTrack/python/plotting/validation.py#L474). In the Sample name in the basic `Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py` script the `scenario` is dummy, the `appendglobaltag` does all the work. Also, if you want to have more meaningful legends, open [validation.py](https://github.com/cms-sw/cmssw/blob/master/Validation/RecoTrack/python/plotting/validation.py) and in the Sample class around line 1219 in the `legendLabels` comment out the return and instead put 
`return [t[1][t[1].find("RelVals")+len("RelVals")+1: t[1].find("DQM_V")-1] for t in self._fileLegends]` or something similar, since we do not want to interfere with the tracker validation tools. 


2. Run first the script with the `"-d"` option in order to download the DQM files in the opt.INPUT area, which usually is the official HGCal DPG eos store area, but check if you have permissions to write there. 

```
python Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py -d
```


??? tip
    It is mandatory to first activate your certificate. Test your grid certificate `grid-proxy-init -debug -verify`. Are you a member of the CMS VO? `voms-proxy-init -voms cms --valid 168:00`


3. Now, we are ready to run through the basic objects. Check below each section in order to see how to run the script in each case. However, what should be inspected before running each step is the lines with the main 
command where the `Validation/HGCalValidation/scripts/makeHGCalValidationPlots.py` script is run. There are cases where certain RelVals are of v2 version or have a small difference either in the reference or in the new campaign. Someone should then correct these lines as in the example commented line in order to read in the DQM files without complaining about missing files. 

4. After all the objects have been run (and only after!) the summary step must be run. 

All plots could be checked in the DQM GUI, but since we have a lot of samples and plots these tools are really necessary. 


??? tip
    Two files that are updated in every validation campaign cannot possibly pushed in CMSSW each time. So, we usually copy the last files from the previous validation campaign. That is, `Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py` and 
`Validation/RecoTrack/python/plotting/validation.py`. 


# SimHits

Let's start with a standalone study with muons. Open [Validation/HGCalValidation/test/runHGCalSimHitStudy_cfg.py](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/test/runHGCalSimHitStudy_cfg.py) and put the correct era. Also, open [Validation/HGCalValidation/python/hgcSimHitStudy_cfi.py](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/python/hgcSimHitStudy_cfi.py) and put the correct modifier. These may be already correct by default, but we should always check. For example for `D41` geometry the modifier is `phase2_hgcalV10`. Time to choose an input file, which for now it is: 
[/RelValSingleMuPt10/CMSSW\_11\_0\_0\_pre7-110X\_mcRun4\_realistic\_v1_2026D41noPU-v1/GEN-SIM-DIGI-RAW](https://cmsweb.cern.ch/das/request?input=dataset%3D%2FRelValSingleMuPt10%2FCMSSW_11_0_0_pre7-110X_mcRun4_realistic_v1_2026D41noPU-v1%2FGEN-SIM-DIGI-RAW&instance=prod/global). Choose a file and put it in [Validation/HGCalValidation/test/runHGCalSimHitStudy_cfg.py](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/test/runHGCalSimHitStudy_cfg.py). 

??? tip
    Check with the `dasgoclient` the correct input. First find the file `dasgoclient -query='dataset=/RelValSingleMuPt100/*110X_mcRun4_realistic_v1_2026D41noPU*/GEN-SIM-DIGI-RAW'`. Then, the files related: `dasgoclient -query='file dataset=/RelValSingleMuPt100/CMSSW_11_0_0_pre7-110X_mcRun4_realistic_v1_2026D41noPU-v1/GEN-SIM-DIGI-RAW'`. You should also put the root global prefix: `root://cms-xrd-global.cern.ch//store/relval/`

Run, 

```
cmsRun Validation/HGCalValidation/test/runHGCalSimHitStudy_cfg.py
python Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --Obj SimHits --html-validation-name SimHits
```

# Geometry 

## Standalone 

From the standalone study above, the [RZ_AllDetectors.png](http://apsallid.web.cern.ch/apsallid/RelVals/CMSSW_11_0_0_pre7/hgcalSimHitStudy/RZ_AllDetectors.png) in the `hgcalSimHitStudy` folder can be checked to find holes or other problems with the geometry. 

## Material budget package

As already mentioned in the [Material Budget](https://hgcal.web.cern.ch/MaterialBudget/MaterialBudget/) section, the package can be used in order to perform a more thorough geometry check. Check the [Material Budget](https://hgcal.web.cern.ch/MaterialBudget/MaterialBudget/) section for the detailed recipes. Then and only after the completion of that section tasks, below you can proceed to the recipe to create the relevant webpages. 

You will need to copy the files with the plots produced from the [Material Budget](https://hgcal.web.cern.ch/MaterialBudget/MaterialBudget/) section to
the relevant folder of `/eos/project/h/hgcaldpg/www` (e.g. `Extended2026D83_vs_Extended2026D86`). This eos space holds all the plots from the release validation campaigns and you will need to have permission in order to write. Once the files are in there you will be able to inspect them throught the [website](https://cms-hgcal-validation.web.cern.ch/cms-hgcal-validation/Extended2026D83_vs_Extended2026D86/index.html).

We will gather here the full recipe for V16 (D86), since it is lengthy, but you should also go through the relevant material budget sections of this website. 

```
cd $CMSSW_BASE/src

# We should tweak the main validation script for the webpage to be 
# correctly created. 

# Open Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py
# and change/add geometryTests and GeoScenario. Don't forget to get the latest
# hgcalPerformanceValidation.py from the latest RelVal campaign because 
# you are going to overwrite the main index.html file. In our case we 
# added Extended2026D83_vs_Extended2026D86. Run
python3 Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --gather "summary,hitCalibration,hitValidation,layerClusters,tracksters" --copyhtml

#You may have some complains or even crash e.g. due to Release CMSSW_... not
#found from globaltag map in validation.py. Don't worry the main thing is to 
#create the folder structure, which you should see that it is printed in your 
#screen. 

# Now with the structure created, we can also create the geometry 
# specific index files. Do, 
python3 Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --geometry
 
# With the structure in eos ready, we can start copying files. Observe 
# the structure of the previous geometry comparison folders and do the same.
# A recipe with the latest D83vsD86 comparison follows.
cd /eos/project/h/hgcaldpg/www
# Take the reference geometry results first.
cp -r Extended2026D76_vs_Extended2026D83/Extended2026D83 Extended2026D83_vs_Extended2026D86/.
# Now, go to the directory you are interested. 
cd Extended2026D83_vs_Extended2026D86
# Take the results for the new geometry.
cd /path/to/results/forHGCAL/.
cp -r Extended2026D86 /eos/project/h/hgcaldpg/www/Extended2026D83_vs_Extended2026D86
# At this point only the fromvertex plots should be missing.
# Get in the fromvertex directory
cd /eos/project/h/hgcaldpg/www/Extended2026D83_vs_Extended2026D86/fromvertex
# Take the reference results
cp -r ../../Extended2026D76_vs_Extended2026D83/fromvertex/Extended2026D83 .
# Prepare the new results
mkdir Extended2026D86
cd Extended2026D86
# Copy the new results, something like
cp -r /path/to/results/matbdg86/Figures .
```

You should now be ready to inspect the plots in the [official webpage](https://cms-hgcal-validation.web.cern.ch/cms-hgcal-validation/). Scroll down to get to the geometry section. 

 
# Digis 

For the digis the relevant script for the standalone study is [Validation/HGCalValidation/test/runHGCalDigiStudy_cfg.py](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/test/runHGCalDigiStudy_cfg.py). Do 

```
cmsRun Validation/HGCalValidation/test/runHGCalDigiStudy_cfg.py
python hgcalPerformanceValidation.py --Obj Digis --html-validation-name Digis
```

??? tip
    Use dasgoclient e.g. for D49: `dasgoclient -query='file dataset=/RelValTTbar_14TeV/*CMSSW_11_1_0_pre3-110X_mcRun4_realistic_v3_2026D49noPU*/GEN-SIM-RECO'`


# RecHits

For the [eta-phi occupancy map](http://apsallid.web.cern.ch/apsallid/RelVals/CMSSW_11_0_0_pre7/HGCValid_hitValidation_Plots/plots_RelValCloseByParticleGun_CE_H_Fine_300um__CMSSW_11_0_0_pre7-110X_mcRun4_realistic_v1_2026D41noPU-v1noPU_RecHits_EtaPhi_zplus/) the boundary of the 200 um and 300 um is visible. The difference is due to the noise hit threshold. 

# Calibrations

Almost all plots show the sum of all matched rechits with simhits (via detid)
energy times fraction over the CaloParticle energy. However, there are some
differences with respect to `h_EoP_CPene_*_calib_fraction`, which indeed shows
the above sum:

- `hgcal_EoP_CPene_*_calib_fraction`: Here the code loops through `particleFlowClusterHGCalFromMultiCl` and finds the closest one to the CaloParticle. He fills only if `DR^2 < 0.01`. 
- `hgcal_photon_EoP_CPene_*_calib_fraction`: He does the same for photons `photonsFromMultiCl`.
- `hgcal_ele_EoP_CPene_*_calib_fraction`: Same but for electrons `ecalDrivenGsfElectronsFromMultiCl`. 

The peak at zero is due to CaloParticles that missed CE-E as can be seen also
from the eta distribution of the CaloParticles and with a fiducial cut at eta
this should go away. Indeed, in the `closeby` samples the peak is gone. 

For `h_EoP_CPene_*_calib_fraction` the peak should be very close to 1 or there
is a problem. For the rest, there is an 8-10% less energy gathered. This is due
to containment issues and inefficiency (RecHits->2D layer clusters->multiclusters).
Keep in mind that at the time of writing these notes, `TICL` is not part of the
standard phase 2 reconstruction sequence. 

There is also the `LayerOccupancy` plot, which essentially counts the number of
RecHits that have been matched with SimHits in each layer. To obtain all these plots do: 

```
python Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --html-validation-name hitCalibration --Obj hitCalibration
```

# hgcalLayerClusters

As the situation is for the moment the amount of plots when running on this object is huge. So, these plots should be kept only for the ongoing validation and then for the previous ones the folder should be zipped, at least until we reduce the amount of plots. So, run

```
python Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --html-validation-name hgcalLayerClusters --Obj hgcalLayerClusters
```

# hitValidation

The biggest amount of plots produced after the hgcalLayerClusters object is when running on the hitValidation plots. Do, 

```
python Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --html-validation-name hitValidation --Obj hitValidation
```

# Summary 

Finally, after all the objects output is in place, gather the results: 

```
python Validation/HGCalValidation/scripts/hgcalPerformanceValidation.py --gather "summary,standalone,hitCalibration,hitValidation,hgcalLayerClusters"
```

After the TICL objects are produced you should enhance the gather option with the relevant strings. The order of the objects in the [webpage](http://cms-hgcal-validation.web.cern.ch/cms-hgcal-validation/) will be the one that is in the gather option. 



# RelVals

The list below is what was centrally produced at every pre-release/release cycle. However, it is under review and when it will be finalized we will update accordingly. 

In the future, more information on the composition of these samples will be
given as well as on how they could be used by the HGCAL validation procedure. 

Sample Name | PU | Notes 
-------------- | ------------------ | ------------
MinBias	 | | 
ttbar |	0, 140, 200| 
Electron+/- gun - flat pT [2,100] GeV, \|eta\| < 3.1 |	0, 200| 
Muon+/- gun - flat pT [0.7,10] GeV, \|eta\| < 3 |	0, 200| 
Muon+/- gun - flat pT [2,100] GeV, \|eta\| < 2.85	 | 0, 200| 
Displaced Mu Gun - flat dxy 0-1000mm, 3 pt bins (2-10, 10-30, 30-100 GeV) |	0, 200| 
Displaced Mu Gun - flat dxy 0-3000mm, 3 pt bins (2-10, 10-30, 30-100 GeV) |	0, 200| 
Photon gun - flat pT [8,150] GeV, \|eta\| < 3.1 |	0, 200| 
Pi+/- gun - flat pT [0.7,10] GeV, \|eta\| < 3 |	0, 200| 
Tau+/- hadronic gun - flat pT [2,150] GeV, \|eta\| < 3.1	 | 0, 200| 
ZTT, HGG, ZMM, ZEE |	0, 200| 
QCD - flat pT-hat [0,1000] GeV, \|eta\| < 5.2 |	0, 200| 
QCD\_Pt-15To7000, QCD\_Pt-20toInf\_MuEnrichedPt15,	 | 0, 200| 
Dark SUSY (dark photon -> displaced mu) 0, 1, 10, 100 cm |	0, 200| 
Tau -> 3mu- B0s -> mu+ mu- |	0, 200| 
B0s -> e+ e- |	0, 200| 
B0s -> phi phi (-> K+ K- K+ K-) |	0, 200| 
B0s -> Jpsi phi (-> mu+mu- K+ K-) |	0, 200| 
B0s -> Jpsi gamma (Jpsi->mu+mu-, aim to reconstr. converted gamma) |	0, 200| 
B0 -> K*(892) mu+ mu-- |	0, 200|

# Troubleshooting 

In this section we would like to gather a couple of real problems that came up during campaign validation as well as the approach we followed to solve them. 

## Central production problem

As we have mentioned, apart from the standard set of FullSim samples we have asked for the regural production the so called `closeby` samples. In the release validation campaign [11\_3\_0\_pre3\_G4\_phase2](https://hypernews.cern.ch/HyperNews/CMS/get/relval/15446.html) we noticed that for those extra HGCAL Phase 2 workflows, while the campaign said that it was using the D49 geometry scenario, it turned out that in fact it was using the D76 scenario. For the standard workflows there was no problem, D49 was used. This lead to huge discrepancies, which couldn't be understood, since we believed to compare the same geometry scenario. Therefore, we should always have in mind that apart from certain problems in a release, there may be other issues with the central production. 

In the announcement of each campaign there are always links to the twiki with the production samples ([like this](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PdmVRelVals2021)) as well as links to samples ([like this](https://dmytro.web.cern.ch/dmytro/cmsprodmon/requests.php?campaign=CMSSW_11_3_0_pre3_G4VECGEOM__UPSG_Std_2026D49noPU-1613632162)). Details for each sample can be seen in the [RegMgr link](https://cmsweb.cern.ch/reqmgr2/fetch?rid=pdmvserv_RVCMSSW_11_3_0_pre3_G4VECGEOMCloseByPGun_CE_H_Coarse_Scint__2026D49noPU_210218_090839_7547) (e.g. [this one](https://dmytro.web.cern.ch/dmytro/cmsprodmon/workflows.php?prep_id=CMSSW_11_3_0_pre3_G4VECGEOM__UPSG_Std_2026D49noPU-CloseByPGun_CE_E_Front_120um-00001)). In case of not expected differences it is worth taking a look at the relevant configuration from the central production. 


## Reverting pull requests

Whenever a new release appears to have discrepancies, we should track down the reasons for this. First, we should start examining all the pull requests that are merged. These can be found in the campaign announcement, where there is always a link ([like this one](https://github.com/cms-sw/cmssw/releases/CMSSW_11_3_0_pre5) for example). Most of them will be irrelevant to our problems, however there will be some that will definitely need to be checked. Therefore, we should revert these ones. 

Below we give an example from a real problem we faced. We needed to revert [PR# 32932](https://github.com/cms-sw/cmssw/pull/32932) due to a shift observed in the calibration of the CE-E section reconstructed hits. The recipe follows 

```
# First setup the release you are currently validating
# For the PR we are discussing here this is CMSSW_11_3_0_pre4
cmsrel CMSSW_11_3_0_pre4
cd CMSSW_11_3_0_pre4/src
git cms-init
# Go to the PR you are interested and find out the packages it 
# uses. Then, add those packages. 
git cms-addpkg Geometry/HGCalCommonData
# Now, it's time to revert each commit starting from the most
# recent, which is the one at the bottom. Here there is only one commit, 
# so we revert only this one. 
git revert --no-edit 1bcce513973b2127f75ead08f6f0625420e62c7f
# Compile
scram b -j 
```
  
At this point you will have set up a release, which doesn't contain the PR you believe is causing the problems. Pick a workflow (usually a `closeby` one) and check if the problem is corrected. 






