# Layer Cluster metrics

## Definitions

### Association Metric between `CaloParticles` and `LayerClusters`

We came up with the following metric between $\mathrm{CaloParticle}_i$
against $\mathrm{Layer~Cluster}_j$ on $\mathrm{Layer}_{\mathrm{N}}$:

$$
\mathrm{score}_{ij}^{N} = \frac{%
  \sum\limits_{k,l}^{\{\mathrm{DetId}_k\mathrm{~of~SC}_l^N \mathrm{\in CP}_i\}}
\left(
  \mathrm{fr}^{\mathrm{LC}_j^N\mathrm{,reco}}_{k} -
  \mathrm{fr}^{\mathrm{SC}_l^N\mathrm{,MC}}_k
\right)^2 \times \epsilon_{k}^{2}
}{
  \sum\limits_{h,l}^{\{\mathrm{DetId}_h\mathrm{~of~SC}_l^N \mathrm{\in
  CP}_i^N\}}\left[\mathrm{fr}^{\mathrm{SC}_l^N\mathrm{,MC}}_h\epsilon_h\right]^2
}
$$

where:

   * $SC_l^N$ represents the $l$-`SimCluster` on the $N$-`Layer`of the
     $i$-`CaloParticle` under investigation,
   * $\mathrm{fr}^{\mathrm{LC}_j^N\mathrm{,reco}}_{k}$ represents the fraction
     of the reconstructed $j$-`LayerCluster` on the $N$-`Layer` for the
     $k$-`DetId`,
   * $\mathrm{fr}^{\mathrm{SC}_l^N\mathrm{,MC}}_k$ represents the fraction
     of the generated $l$-`SimCluster` on the $N$-`Layer` for the
     $k$-`DetId` belonging to the $i$-`CaloParticle`,
   * $\epsilon$ is the energy associated to a particular `RecHit`/`DetId`.

### Association Metric between `LayerClusters` and `CaloParticles`

We came up with the following metric between $\mathrm{LC}_i^N$ against
$\mathrm{CP}_j$ on $\mathrm{Layer}_{\mathrm{N}}$:

$$
\mathrm{score}_{ij}^{N} = \frac{%
  \sum\limits_{k,l}^{\{\mathrm{DetId}_k \in \mathrm{LC}_i^N,
  \mathrm{SC}_l^N \mathrm{~of~CP}_j\}}
\left(
  \mathrm{fr}^{\mathrm{LC}_i^N\mathrm{,reco}}_{k} -
  \mathrm{fr}^{\mathrm{SC}_l^N\mathrm{,MC}}_k
\right)^2 \times \epsilon_{k}^{2}
}{
  \sum\limits_{h}^{\{\mathrm{DetId}_h\mathrm{~of~LC}_i^N\}}\left[\mathrm{fr}^{\mathrm{LC}_i^N\mathrm{,reco}}_h\epsilon_h\right]^2
}
$$

where:

   * $\mathrm{LC}_i^N$ represent the $i$-`LayerCluster` on the $N$-`Layer`.

### Efficiency

Ratio between `CaloParticles` that have been associated to a
reconstructed `LayerCluster` divided by all the generated
`CaloParticles`. Those plots are usually produced by **Layer** and
versus the **generated** (i.e. of the `CaloParticle`) $\eta$ and
$\phi$.

### Duplicate (Cluster Splitting)

Ratio between the `CaloParticles` that have been associated to *more
than one* `LayerCluster` divided all the generated `CaloParticles`.
Those plots are usually produced by **Layer** and versus the
**generated** (i.e. of the `CaloParticle`) $\eta$ and $\phi$.

### Fake Rate

One minus the ratio between the `LayerClusters` that have been
associated to a `CaloParticle` divided by all the reconstructed
`LayerClusters`. Those plots are usually produced by **Layer** and
versus the **reconstructed** (i.e. of the `LayerCluster`) $\eta$ and
$\phi$.

### Merge Rate

Ratio between the `LayerClusters` that have been associated to *more
than one* `CaloParticle` divided by all the reconstructed
`LayerClusters`. Those plots are usually produced by **Layer** and
versus the **reconstructed** (i.e. of the `LayerCluster`) $\eta$ and
$\phi$.

## Plots

### CP2LC

#### Score CP2LC

This plots shows the distribution of the score of the CP2LC
association. It is partitioned by **Layer**. There is one entry per
 `CaloParticle` on a specific Layer. This should help
guiding the eye to set a proper threshold to associate CP2LC.
`CaloParticles` that have a good match with a `LayerCluster` will tend
to have a score close to 0, while poorly matched ones will tend to
have a score closer to 1.

!!! info
    A high score means that several components (`DetIds`) of the
    `CaloParticle` are not reconstructed by the `LayerCluster` for
    which the score is computed. This is scaled with the energy
    squared, so that a missed energetic contribution is given a higher
    penalty.

![Example of Score of CP2LC](score_caloparticle_to_layercluster_Score_caloparticle2layercl_perlayer10.png "Example of distribution of CP2LC")

#### Shared Energy of CP and LC

This plot shows the distribution of the energy shared between a
`CaloParticle` and a `LayerCluster`.

!!! warning
    **The plot is filled w/o requiring any threshold on the association
    between the 2 objects**.

It is filled for all `CaloParticles` times all the `LayerClusters` that
contributed somehow to it. The ratio is between the energy deposited by the
`LayerCluster` on a specific layer **belonging to this particular
`CaloParticle`** and the energy associated to the same `CaloParticle`.

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{LC in CP}}}
                                   {\textrm{Energy}_{\textrm{CP}}}$

A ratio close to 1 implies that a single `LayerCluster` collected the majority of the
energy released by a `CaloParticle`. A ratio close to 0 implies that the
`LayerCluster` has reconstructed a small fraction of the Monte Carlo one.

???+ info
    By definition the ratio is limited to the [0,1] interval. The fraction
    represents the amount of energy of a single `CaloParticle` assigned to a
    **unique** `LayerCluster`. Values below 1 and considerably greater than 0,
    i.e. in the range [0.2,0.8] are indication of **cluster splitting**, since a
    single Monte Carlo contribution has been assigned to several
    `LayerClusters`, each with a fraction considerably smaller than 1. This
    arguments holds true for the case in which the shower is very well confined
    and generated contiguous energy deposit in the layer, e.g. **electromagnetic
    showers**. For other cases the interpretation could be less clear.

    The relative high population at very low values, i.e. in the range [0,0.1],
    represents _dust_ around the **core** of the shower that has been
    reconstructed as single `LayerClusters` which will inevitably hold an
    extremely low fraction of the overall `CaloParticle` energy. This could be
    more evident in the case of **hadronic showers**.


![Example of shared energy between CP and LC](sharedEnergy_caloparticle_to_layercluster_SharedEnergy_caloparticle2layercl_perlayer10.png
"Example of shared energy between CP and LC")

These plots are usually produced as cumulative distributions per
`Layer` and also as profiles versus $\eta$ and $\phi$ of the
generated `CaloParticle`.

!!! warning
    **The plots versus $\eta$ and $\phi$ are only filled if the
    association criteria for CP2LC is satisfied and, in case of
    multiple matches, the best LC (the one with the lowest score) is
    selected.**

#### Energy versus Score CP2LC

This is a 2D histogram that shows the correlation between the score of
the CP2LC (on the X-axis) and the fraction of energy shared between
the two objects (on the Y-axis). The definition of the shared energy
is identical to the case above, namely:

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{LC in CP}}}
                                   {\textrm{Energy}_{\textrm{CP}}}$

By definition the ratio is limited to the [0,1] interval.

!["Energy versus Score for CP2LC for 4 double-photon samples at different separations, 2.5, 5, 7.5, 10 cm"](Energy_vs_Score_Layer10.png "Energy versus Score for CP2LC for 4
double-photon samples at different separations at 2.5, 5, 7.5 and 10 cm, respectively, left-to-right, top-to-bottom.")

These plots are useful to guide the eye in selecting a good threshold
in order to associate a `CaloParticle` to a `LayerCluster`, while also
taking into account how much of the actual energy is indeed shared
between the 2 objects.

!!! warning
    **The plot is filled w/o requiring any threshold on the
    association between the 2 objects**.

???+ info
     A high concentration of points in the upper-left corner means that we have
     many `CaloParticles` with excellent score (0 on the X-Axis) that are
     associated to a reconstructed `LayerCluster` that gathered a considerable
     amount of the Monte Carlo energy (1 on the Y-Axis).

     An accumulation of points with relatively small scores (in the range
     [0.1,0.4]) and distributed along a wide range of Y-values could be an
     indication of **cluster splitting**, as explained in the previous section.

### LC2CP

#### Score LC2CP

This plots shows the distribution of the score of the LC2CP
association. It is partitioned by **Layer**. There is one entry per
reconstructed `LayerCluster` on a specific Layer. This should help
guiding the eye to set a proper threshold to associate LC2CP.
Extremely good `LayerClusters` will tend to have a score closer to 0,
while poorly assigned/reconstructed `LayerClusters` will have a score
  closer to 1.

![Example of Score of LC2CP](score_layercluster_to_caloparticle_Score_layercl2caloparticle_perlayer10.png "Example of distribution of LC2CP")

#### Shared Energy of LC and CP

This plot shows the distribution of the energy shared between a
`LayerCluster` and a `CaloParticle`.

!!! warning
    **The plot is filled w/o requiring any threshold on the
    association between the 2 objects**.

It is filled for all reconstructed `LayerClusters` times all the `CaloParticles`
that contributed somehow to each `LayerCluster`. The ratio is between the energy
deposited by the `CaloParticle` on a specific layer and reconstructed by a
`LayerCluster` and the overall energy associated to the same `LayerCluster`.

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{LC in CP}}}
                                   {\textrm{Energy}_{\textrm{LC}}}$

  A ratio close to 1 implies that the `LayerCluster` collected the majority of
  the energy released by a **unique** `CaloParticle`. A ratio close to 0 implies
  that the contribution of the `CaloParticle` to the `LayerCluster` is
  negligible.

???+ info
    By definition the ratio is limited to the [0,1] interval. The fraction
    represents the amount of energy of the `LayerCluster` assigned to a
    **unique** `CaloParticle`. Values below 1 and considerably greater than 0,
    i.e. in the range [0.2,0.8], are indication of **cluster merging**, since a
    single `LayerCluster` has contributions coming from several
    `CaloParticles`, each with a fraction considerably smaller than 1. This
    arguments holds true for the case in which the shower is very well confined
    and generated contiguous energy deposit in the layer, e.g. **electromagnetic
    showers**. For other cases the interpretation could be less clear.

![Example of shared energy between LC and CP](sharedEnergy_layercluster_to_caloparticle_SharedEnergy_layercluster2caloparticle_perlayer10.png
"Example of shared energy between LC and CP")

The plots above is just an illustrative example that, for the blue curve case,
shows a clear sign of **cluster merging**, where the peak at 0.5 means that the
reconstructed LC energy is equally distributed among different `CaloParticles`.
That sample is a double-$\gamma$ sample with 2.5cm distance that cannot be
separated into 2 contributions (at least on that `Layer`).

These plots are usually produced as cumulative distributions per
`Layer` and also as profiles versus $\eta$ and $\phi$ of the
reconstructed `LayerClusters`.

!!! warning
    **The plots versus $\eta$ and $\phi$ are only filled if the
    association criteria for LC2CP is satisfied and, in case of
    multiple matches, the best CP (the one with the lowest score) is
    selected.**

#### Energy versus Score LC2CP

This is a 2D histogram that shows the correlation between the score of
the LC2CP (on the X-axis) and the fraction of energy shared between
the two objects (on the Y-axis). The definition of the shared energy
is identical to the case above, namely:

$\epsilon_{\textrm{shared}} = \frac{\textrm{Energy}_{\textrm{LC in CP}}}
                                   {\textrm{Energy}_{\textrm{LC}}}$

!["Energy versus Score for LC2CP for 4 double-photon samples at different separations, 2.5, 5, 7.5, 10 cm"](Energy_vs_Score_Layer14.png "Energy versus Score for LC2CP for 4
double-photon samples at different separations at 2.5, 5, 7.5 and 10 cm, respectively, left-to-right, top-to-bottom.")

These plots are useful to guide the eye in selecting a good threshold
in order to associate a `LayerCluster` to a `CaloParticle`, while also
taking into account how much of the actual energy is indeed shared
between the 2 objects.

!!! note
    **The plot is filled w/o requiring any threshold on the
    association between the 2 objects**.

???+ info
     A high concentration of points in the upper-left corner means that we have
     many reconstructed `LayerClusters` with excellent score (0 on the X-Axis)
     that collected a considerable amount of the Monte Carlo energy (1 on the
     Y-Axis).

     An accumulation of points with relatively small scores (in the range
     [0.1,0.4]) and distributed along a wide range of Y-values could be an
     indication of **cluster merging**, as explained in the previous section.


??? tip
    If you take a look at the upper left plot of the score versus energy plot
    above for LC2PC, you can immediately spot a concentration of points with
    relatively good score and rather poor energy sharing. This plot refers to
    the double-$\gamma$ sample with a separation of 2.5cm. This is a clear sign
    of **cluster merging**. In fact, if a `LayerCluster` is the result of
    merging different contributions from 2 `CaloParticles`, its score is still
    relatively low, since all its components will found in at least 1
    `CaloParticle`, but it's energy fraction will be twice the `CaloParticle`,
    assuming the 2 contributions merged are roughly equivalent in energy. As a
    reminder, a very good score is much smaller than 0.1, in the $10^{-3}$ and
    below ballpark, at least for the easy cases of single or multi-particles.


## Technical Implementation

The association between `CaloParticles` and `LayerClusters` is performed by an 
EDProducer which provides two ([OneToManyWithQuality](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookAssociationMap)) 
AssociationMaps:
  * a `OneToManyWithQualityGeneric<CaloParticle collection, CaloCluster collection, CP2LC score>` (SimToReco)
  * a `OneToManyWithQualityGeneric<CaloCluster collection, CaloParticle collection, LC2CP score>` (RecoToSim)

The producer has been introduced with 
[PR #29844](https://github.com/cms-sw/cmssw/pull/29844), which also shows its 
first usage in the **HGCalValidator**.
A similar producer for the association between `CaloParticles` and 
`MultiClusters` is under preparation.

### Variables and their meaning

#### detIdInfoInCluster

```C++
struct detIdInfoInCluster {
  bool operator==(const detIdInfoInCluster& o) const {
    return clusterId == o.clusterId;};
  unsigned int clusterId;
  float fraction;
};
```
The definition of the `operator==` is **extremely important** because
this `struct` will be used inside `maps` and other containers and,
when searching for one particular occurence, only the `clusterId`
member will be used in the check, skipping the fraction part.

#### detIdInfoInMultiCluster
```C++
struct detIdInfoInMultiCluster
{
  bool operator==(const detIdInfoInMultiCluster& o) const {
    return multiclusterId == o.multiclusterId;};
  unsigned int multiclusterId;
  unsigned int clusterId;
  float fraction;
};
```

#### caloParticleOnLayer

```C++
struct caloParticleOnLayer
{
  unsigned int caloParticleId;
  float energy=0;
  vector<pair<DetId, float> > hits_and_fractions;
  unordered_map<int, pair<float,float>> layerClusterIdToEnergyAndScore;
};
```

#### Used Variables and Meaning

 Variable Name | Variable C++  Type | Usage | Explanation
-------------- | ------------------ | ----- | -----------
detIdToCaloParticleId_Map | `unordered_map<DetId, vector<detIdInfoInCluster> >` | detIdToCaloParticleId_Map[detId][dummy] | Given a RecHit(detId), addresses all **CaloParticles** that contributed to that RecHit storing **cpIdx** and its contributing fraction. All the different contributions from the same CaloParticle to a single recHit (coming from their internal SimClusters) are **merged** into a single entry, with the fractions properly summed.
detIdToLayerClusterId_Map | `unordered_map<DetId, vector<detIdInfoInCluster> >` | detIdToLayerClusterId_Map[detId][dummy] | Given a RecHit(detId), addresses all **LayerClusters** that contributed to that RecHit storing **lcIdx** and its contributing fraction.
cpsInLayerCluster | `vector<vector<pair<unsigned int, float> > >` | cpsInLayerCluster[LayerIdx][dummy]{cpIdx,score| For each **LayerCluster**, identified by **LayerIdx**, associate a vector containing all the **CaloParticles** via their **cpIdx** that contributed to that LayerClusters, together with score. The association is not unique and there could be several instances of the same **cpIdx**: this is due to the fact that a CaloParticle can have several SimClusterss that, each, contributed to the same LayerCluster.
cPOnLayer | `vector<vector<caloParticleOnLayer> >` | cPOnLayer[cpIdx][LayerIdx] | For each **CaloParticle**, addressed by its **cpIdx**, and for each layer, addressed by **LayerIdx**, stores the overall energy released by that CaloParticle on that specific layer, together with the details of the *hits_and_fractions* via a **caloParticleOnLayer** object. The member **layerClusterIdToEnergyAndScore** will store the energy coming from a particular **LayerCluster**, and its score. The indices are: **layerClusterIdToEnergyAndScore[lcIdx]{energy,score}**



