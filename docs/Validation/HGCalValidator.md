# HGCalValidator

## Introduction

Validation is a very challenging task, since it combines many different aspects of an experiment: 

* simulation (Geant)
* reconstruction (local and global)
* CMSSW Framework
* DQM/Validation.

If developed early, it can accelerate tremendously the progress on simulation and reconstruction software.
Tracker has developed through a period of many years a very successful package called __MultiTrackValidator__, which
can monitor objects from low to high level with clarity.
We profit from the sophisticated organization of the __MultiTrackValidator__ and created the __HGCalValidator__ package.

The HGCalValidator package follows the MultiTrackValidator design with a DQMGlobalEDAnalyzer and
a Histogramming class (HGVHistoProducerAlgo), which books histograms to be stored in a DQM file.
There are also python scripts for generating final set
of plots with an option to generate HTML pages.

The HGCalValidator package was first introduced in CMSSW with [PR #26099](https://github.com/cms-sw/cmssw/pull/26099),  while presentations in the HGCAL DPG described the content of the code ([here](https://indico.cern.ch/event/800319/contributions/3325660/attachments/1799333/2934293/HGCalValidation_20Feb19_HGCalDPG.pdf) and [there](https://indico.cern.ch/event/803066/contributions/3341958/attachments/1807341/2950233/20190306_HGCAL_DPG_ImagingAlgoModification_MR.pdf)).

The first object we started to monitor was 2d layer clusters (**"hgcalLayerClusters"**), which are the basic objects for the TICL reconstruction.
The HGCalValidator can loop through other objects also and easily add desired plots in relevant DQM folder,
due to the excellent organization of the skeleton code. We are currently looking into the following areas/objects in order to have a picture of the :

* Geometry
* SimHits
* SimClusters (Any track that has hits in HGCAL.)
* CaloParticles (A particle from the main vertex which has, directly or through its children, hits in HGCAL. A collection of SimClusters.)
* Digis
* RecHits 
* Calibration
* Layer clusters (Groups of RecHits in the same layer.)
* TICL tracksters (Linking of layer clusters to form the particle shower, all iterations.)

No higher level objects in this list since these will be validated by the POG/PAG. 
    
[Simulated hits](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/plugins/HGCalSimHitValidation.cc)
, [digitized hits](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/plugins/HGCalDigiValidation.cc)
and [reconstructed hits](https://github.com/cms-sw/cmssw/blob/master/Validation/HGCalValidation/plugins/HGCalRecHitValidation.cc)
use existing CMSSW validation code to create DQM plots and then the python scripts of the HGCalValidator package are used to
produce the final plots in HTML. 

## Validation in numbers 

The are two modes to work with: 

* Extended: We save and plot everything. 
* Normal: We remove a number of plots from the layer clusters and CaloParticles. We don’t save anything from SimClusters (under development).

In each validation campaign, when working with the Normal mode, 136795 files are created. Due to this enormous amount of files, the file limit of the eos space is easily reachable. That's why we zip them and the final zip files that we keep per campaign is around 5.2 GB. When working with the Extended mode for layer clusters add a 30% more both in size and number of files. When adding all CaloParticles and SimClusters the final zip file is a little above 10 GB. Approximately ~5 hours are needed to produce the results, while ~2 hours to unzip the file with the final results in the official HGCAL eos www area. Below you can see a pie chart with the current amount of files saved in the normal mode per object for each validation campaign. 

![alt text](ValidationInNumbers.png)


## Description of the tool

Below you can see the organization of the HGCalValidator package.

![alt text](HGCalValidator.png)

Specifically:

1.  HGCalValidator contains:    
    * Validation/HGCalValidation/plugins/HGCalValidator.cc
    * Validation/HGCalValidation/python/HGCalValidator\_cff.py
    * Validation/HGCalValidation/python/HGCalValidator\_cfi.py
    * Validation/HGCalValidation/interface/HGCalValidator.h
    * Validation/HGCalValidation/interface/CaloParticleSelector.h
    * Validation/HGCalValidation/python/CaloParticleSelectionForEfficiency\_cfi.py
    * Validation/HGCalValidation/python/CaloParticleSelectionsForEfficiency\_cff.py
    
    It is the basic DQMGlobalEDAnalyzer from where the necessary input/objects are loaded. There is also code to perform specific selection and studies on CaloParticle objects.

2.  HGVHistoProducerAlgo is the histogramming class, which books histograms to be stored in the DQM file. It contains:
    * Validation/HGCalValidation/python/HGVHistoProducerAlgoBlock_cfi.py
    * Validation/HGCalValidation/src/HGVHistoProducerAlgo.cc
    * Validation/HGCalValidation/interface/HGVHistoProducerAlgo.h.  
    
    This is where the core of the package algorithms lives. It reads everything it needs from HGCalValidator part and produces folders with output in the DQM file. The HGVHistoProducerAlgoBlock_cfi.py file contains all the info needed for the constructors of the histograms produced.

3. Last part are the python scripts for generating final set of plots with an option to generate HTML pages. It contains:
    * Validation/HGCalValidation/python/hgcalPlots.py
    * Validation/HGCalValidation/python/makeHGCalValidationPlots.py
   
   hgcalPlots.py is where all the plots reside, while makeHGCalValidationPlots.py is the basic scripts that we run.

???+ info
     For the plotting part, we take advantage of the tools already created by the MultiTrackValidator. However, one option was added (**--separate**) that when set to true it will produce a webpage with all plots to be able to visualize all images at a glance ([PR #26100](https://github.com/cms-sw/cmssw/pull/26100)).

??? tip
    You can change the plotting style to "ksiourmen" in Validation/RecoTrack/python/plotting/plotting.py (ROOT.gStyle.SetOptStat("ksiourmen")), in case you wish to extend the stat box, since we didn't want to mess up the already existing code of the MultiTrackValidator.

## Plots description

### hgcalLayerClusters

Below we give a description of a number of validation plots for the **"hgcalLayerClusters"**. The rest of the plots associated to the association metrics can be seen in the next section. All plots for a large amount of RelVals samples can be seen in this [link](http://apsallid.web.cern.ch/apsallid/RelVals/CMSSW_10_6_0/HGCValid_hgcalLayerClusters_Plots/). The output location in DQM file is: **HGCAL/HGCalValidator/hgcalLayerClusters**.

#### Number of layer clusters per thickness

These distributions shows the number of layer clusters per event in: a) 120um, b) 200um, c) 300um, d) scint with
one entry per event in each of the four histos. Each event to be treated as two events:
an event in +z endcap, plus another event in -z endcap.

**DQM histo name**: totclusternum_thick\_i (i=120,200,300,-1) with -1 for scint.   
**Number of histos**: 4

!!! warning
    These histos do not count clusters that contain cells with more that one kind of thickness.


#### Number of layer clusters with mixed thickness

We also keep track of the number of clusters per event with hits in different thicknesses. Again, each event to be treated as two events: an event in +z endcap, plus another event in -z endcap.

**DQM histo name**: mixedhitscluster\_i (i=zminus,zplus).  
**Number of histos**: 2

####  Number of layer clusters per layer

These distributions shows the number of layer clusters in each layer.
One entry per event in each histo. Once more, each event to be treated as two events: an event in +z endcap, plus another event in -z endcap.

**DQM histo name**: totclusternum\_layer\_i (i=0,..,103) ( [V10] -z: 0->49, +z: 50->99).  
**Number of histos**: 100 (V10)

####  Number of cells in layer cluster per layer and per thickness

These distributions shows the total number of cells in layer cluster per layer and per thickness.
Separate histos are created in each layer for a) 120um, b) 200um, c) 300um, d) scint. One entry per layer cluster in the appropriate histo. 

**DQM histo name**: cellsnum\_perthick\_perlayer\_i\_j , i=120,200,300,-1 with -1 for scint and j=0,..,99 ( [V10] -z: 0->49, +z: 50->99).   
**Number of histos**: 100 * 4 = 400

!!! warning
    Not all combinations exist; e.g. no 120um Si in layers with scint, but we keep them for cross checking.

???+ info
     In case of cluster with different mixed thickness cells, we check the thickness with the biggest amount of cells and assign to the relevant histo the total amount of cells (regardless of thickness).


#### Distance of cluster cells from seed

These distributions shows the distance of cluster cells in cm from a) seed cell b) same with entries weighted by cell energy.
Separate histos are created in each layer for a) 120um, b) 200um, c) 300um, d) scint. One entry per cell in a layer cluster in each of the four appropriate histos.

**DQM histo name**:   
a) distancetoseedcell\_perthickperlayer\_i\_j  
b) distancetoseedcell\_perthickperlayer\_eneweighted\_i\_j  
with i=120,200,300,-1 and -1 for scint j=0,..,99.  
**Number of histos**: 100 * 4 * 2 = 800

!!! warning
    Not all combinations exist; e.g. no 120um Si in layers with scint, but we keep them for cross checking.

???+ info
     In case of cluster with mixed cells the distance value fills the histo with thickness, the thickness of the cell under study.

#### Distance of cluster cells from max

These distributions shows the distance of cluster cells in cm from
a) max cell b) same with entries weighted by cell energy.
Separate histos are created in each layer for a) 120um, b) 200um, c) 300um, d) scint.
One entry per cell in a layer cluster in each of the four appropriate histos.

**DQM histo name**:   
a) distancetomaxcell\_perthickperlayer\_i\_j  
b) distancetomaxcell\_perthickperlayer\_eneweighted\_i\_j  
with i=120,200,300,-1 and -1 for scint j=0,..,99.  
**Number of histos**: 100 * 4 * 2 = 800

!!! warning
    Not all combinations exist; e.g. no 120um Si in layers with scint, but we keep them for cross checking.

???+ info
     In case of cluster with mixed cells the distance value fills the histo with thickness, the thickness of the cell under study.

####  Distance between seed cell and max cell 

These distributions shows the distance of HGCAL-style seed cell to max cell in cm. Separate histos are created in each layer for a) 120um, b) 200um, c) 300um, d) scint. One entry per layer cluster in each of the four appropriate histos.

**DQM histo name**: distancebetseedandmaxcell\_perthickperlayer\_i\_j
i=120,200,300,-1 with -1 for scint
j=0,..,99.  
**Number of histos**: 100 * 4 = 400

!!! warning
    Not all combinations exist; e.g. no 120um Si in layers with scint, but we keep them for cross checking.

???+ info
     In case of cluster with mixed cells the distance value fills the histo with thickness, the thickness of the cell under study.

#### Total energy clustered (%)

a) Fraction of the total energy (%) clustered by layer clusters over CaloParticles energy. One entry per event.
b) Fraction of the total energy (%) clustered by layer clusters per layer over CaloParticles energy.
One entry per event in the relevant layer histo.

**DQM histo name**:   
a) energyclustered\_i (i=zminus,zplus).  
b) energyclustered\_perlayer\_i (i=0,..,99).  
**Number of histos**: 2 + 100 = 102

!!! note
    There could be events with zero CaloParticle energy, no CaloParticle reached HGCAL. However, there is clustered energy, which does not contributes to these histos.

####  Longitudinal depth barycentre

The longitudinal depth barycentre. It is defined as: 

$$
\mathrm{t} = \frac{ \sum_{i=1}^{50}\left( \mathrm{E}_{i} \sum_{j=1}^{i} \mathrm{X}_{0,j}  \right) }{ \sum_{i=1}^{52} \mathrm{E}_{i}  }
$$

with $\mathrm{E}_{i}$ the total energy clustered in layer i and $\sum_{j=1}^{i} \mathrm{X}_{0,j}$ the total calorimetric radiation length up to layer i. One entry per event. 

!!! note
    For this histo we used the material budget package to get the accumulated material budget in front of each sensitive layer for the 2023D41 geometry and saved the result in Validation/HGCalValidation/data/D41.cumulative.xo . 

**DQM histo name**: longdepthbarycentre\_i (i=zminus,zplus).  
**Number of histos**: 2

####  Cells energy density

Energy density of cells in a) 120um, b) 200um, c) 300um, d) scint (one entry per rechit, in the appropriate histo)

**DQM histo name**: cellsenedens\_thick\_i with i=120,200,300,-1 and -1 for scint.   
**Number of histos**: 4

### ticlTracksters

We continue to `Tracksters` and below we give a description of the current validation plots produced by the HGCalValidator package. The output location in DQM file is: **HGCAL/HGCalValidator/ticlTracksters+iteration**, where the iteration is referred to the TICL iterations (**Merge**, **Trk**, **TrkEM**, **HAD**).

#### Number of tracksters 

These distributions shows the number of tracksters per event. Each event is treated as two events: an event in +z endcap, plus another event in -z endcap. There are 3 cases: 

1. First, we count the number of tracksters without any requirement, as they come out of the collection.   
   **DQM histo name**: tottracksternum

2. Then, we count them with the requirement of three contiguous layers per trackster.   
   **DQM histo name**: conttracksternum

3. Finally, we relax the requirement of three contiguous layers and count the tracksters that doesn't have 3 contiguous layers.   
   **DQM histo name**: nonconttracksternum

#### Layers and trackster

We also plot for each trackster:

1. First layer of trackster. **DQM histo name**: trackster_firstlayer
2. Last layer of trackster. **DQM histo name**: trackster_lastlayer
3. Total number of layers in trackster. **DQM histo name**: trackster_layersnum

#### Number of layer clusters in trackster

These distributions show:

1. The total number of layer clusters in trackster.   
   **DQM histo name**: clusternum\_in\_trackster
2. Number of layer clusters in trackster for each layer.   
   **DQM histo name**: clusternum\_in\_trackster\_perlayer_i_,(_i_=0,..,103) ( [V10 and above] -z: 00->49, +z: 50->99).
3. The profile of the number of 2d layer clusters in trackster vs layer number.   
   **DQM histo name**: clusternum\_in\_trackster\_vs\_layer
   
#### trackster physical quantities

We also monitor each trackster's : 

1. Transverse momentum. **DQM histo name**: trackster_pt 
2. Eta. **DQM histo name**: trackster_eta
3. Phi. **DQM histo name**: trackster_phi
4. Energy. **DQM histo name**: trackster_energy
5. X position of the trackster. **DQM histo name**: trackster_x
6. Y position of the trackster. **DQM histo name**: trackster_y
7. Z position of the trackster. **DQM histo name**: trackster_z

#### Multiplicity

A very useful plot is the one that shows the **cluster size (no. of hits of a layer cluster in a trackster) (Y-axis) vs. layer cluster multiplicity (no. of times layer clusters of that size are used for forming that trackster)**. So, there are **tracksters x LayerClusters** entries. 

There is also a colored z axis with a text option that gives the % of the current cell over the total number of layer clusters in all tracksters. So, in case of small size layer clusters with small multiplicity the lower left corner will be more populated, like in the case of hadronic showers. This is similar to the shared energy plots that are described in the next section, where at very low values, i.e. in the range [0,0.1], the _dust_ around the core of the shower reconstructs single LayerClusters. If we want to spot large multiplicity of small cluster we should move from that corner to the right low corner. This could indicate noise/PU layer clusters that has been reconstructed as a trackster. Very well confined showers that generated contiguous energy deposit could produce large sized layer cluster and populate the upper left corner. 

Similar plots are produced but with Y-axis layer number and layer cluster energy.

1. **DQM histo name**: multiplicityOfLCinMCL.  
2. **DQM histo name**: multiplicityOfLCinMCL\_vs\_layercluster\_zminus.  
3. **DQM histo name**: multiplicityOfLCinMCL\_vs\_layercluster\_zplus.  
4. **DQM histo name**: multiplicityOfLCinMCL\_vs\_layerclusterenergy

